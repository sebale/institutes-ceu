<?php

require_once("../../config.php");
require_once("locallib.php");
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->libdir.'/gradelib.php');

require_login();

$courseid = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
require_login($course);

$title = get_string('pluginname', 'local_gradebook');

$context = context_course::instance($courseid);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/gradebook/ti.php", array()));
$PAGE->requires->jquery();
$PAGE->navbar->add($title, new moodle_url('/local/gradebook/ti.php', array('id'=>$course->id)));
$PAGE->set_title($title);
$PAGE->set_pagelayout('course');
$PAGE->set_heading($title);

echo $OUTPUT->header();

echo html_writer::start_tag('div', array('class'=>'gradebook gradebook_ti'));
echo $OUTPUT->heading($title);

echo local_gradebook_ti_display($course);

echo html_writer::end_tag('div');

?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.section-header').click(function(){
            jQuery(this).parent().toggleClass('active');
            jQuery(this).parent().find('div.course-level2-sections').slideToggle();
        });
        jQuery('.segment-section').click(function(){
            var segmentid = jQuery(this).attr('data-segment-id');
            if (jQuery(this).find('.nav-segment-toggler').hasClass('open')){
                jQuery('.gradebook_ti').find('tr.segment-'+segmentid).hide();
            } else {
                jQuery('.gradebook_ti').find('tr.segment-'+segmentid).show();
            }
            jQuery(this).find('.nav-segment-toggler').toggleClass('open');
        });
    });
</script>

<?php
echo $OUTPUT->footer();
