<?php

/**
 * Institutes version file.
 *
 * @package    local_gradebook
 * @copyright  2017 The Institutes
 */

$capabilities = array(
    'local/gradebook:view' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
    'local/gradebook:manage' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
);
