<?php

/**
 * version file.
 *
 * @package    local_gradebook
 * @copyright  2017 The Institutes
 */

$plugin->version   = 2016060703;
$plugin->requires  = 2011120500;
$plugin->release   = '1.0';
$plugin->maturity  = MATURITY_STABLE;
$plugin->component = 'local_gradebook';

