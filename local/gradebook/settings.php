<?php

/**
 *
 * @package    local_gradebook
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die;

if (!$ADMIN->locate('local_gradebook') and $ADMIN->locate('localplugins')){
    $settings = new admin_settingpage('local_gradebook', get_string('pluginname', 'local_gradebook'));
    $ADMIN->add('localplugins', $settings);
}
