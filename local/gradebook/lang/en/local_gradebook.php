<?php

/**
 *
 * @package    local_gradebook
 * @copyright  2017 The Institutes
 */

$string['pluginname'] = 'Gradebook';
$string['settings'] = 'Settings';
$string['gradebook_settings'] = 'Gradebook Settings';
$string['gradebook:manage'] = 'Gradebook manage';
$string['gradebook:view'] = 'Gradebook view';
$string['nocourses'] = 'You are not enrolled in any courses';
$string['nothingtodisplay'] = 'Nothing to Display';
$string['quizname'] = 'Quiz Name';
$string['status'] = 'Status';
$string['attempts'] = 'Attempts';
$string['grade'] = 'Grade';
$string['complete'] = 'Complete';
$string['incomplete'] = 'Incomplete';
