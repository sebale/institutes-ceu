<?php

// institutes AB
function local_gradebook_display($course){
    global $CFG, $USER;

    $output = '';

    $course = course_get_format($course)->get_course();

    $modinfo = get_fast_modinfo($course);
    $sections = local_gradebook_get_course_sections($course);

    if (count($sections)){
        $course_quizzes = local_gradebook_get_course_quizzes($course);

        // level1 sections start
        $j = 1;
        $output .= html_writer::start_tag('ul', array('class'=>'course-level1-sections'));
        foreach ($sections[0] as $section1=>$thissection1){

            $output .= html_writer::start_tag('li', array('class'=>'course-section-box'));
            $toggler = html_writer::tag('i', '', array('class'=>'toggler ion-ios-arrow-right'));
            $counter = html_writer::tag('span', ((strlen($j) > 1) ? $j : '0'.$j), array('class'=>'section-counter'));
            $sectionname = $toggler.$counter.get_section_name($course, $thissection1);

            $output .= html_writer::tag('div', $sectionname, array('class'=>'section-header'));

            // level2 sections start
            if (isset($sections[$thissection1->id]) and count($sections[$thissection1->id])){
                $output .= html_writer::start_tag('div', array('class'=>'course-level2-sections'));

                    $output .= html_writer::start_tag('table', array('class'=>'course-level2-sections'));
                        $output .= html_writer::start_tag('thead');
                            $output .= html_writer::start_tag('tr');
                                $output .= html_writer::tag('th', get_string('quizname', 'local_gradebook'), array('class'=>'name'));
                                $output .= html_writer::tag('th', get_string('status', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('attempts', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('grade', 'local_gradebook'), array('class'=>'grade'));
                            $output .= html_writer::end_tag('tr');
                        $output .= html_writer::end_tag('thead');

                    foreach ($sections[$thissection1->id] as $section2=>$thissection2){
                        $output .= html_writer::start_tag('tbody');

                            $output .= html_writer::start_tag('tr');
                            $output .= html_writer::tag('td', get_section_name($course, $thissection2), array('class'=>'section-level2', 'colspan'=>4));
                            $output .= html_writer::end_tag('tr');

                            // level3 sections start
                            if (isset($sections[$thissection2->id]) and count($sections[$thissection2->id])){
                                foreach ($sections[$thissection2->id] as $section3=>$thissection3){
                                    $output .= html_writer::start_tag('tr');
                                    $output .= html_writer::tag('td', get_section_name($course, $thissection3), array('class'=>'section-level3', 'colspan'=>4));
                                    $output .= html_writer::end_tag('tr');

                                    // level4 sections start
                                    if (isset($sections[$thissection3->id]) and count($sections[$thissection3->id])){
                                        foreach ($sections[$thissection3->id] as $section4=>$thissection4){
                                            $output .= local_gradebook_get_quizzes_list($course, $thissection4, $course_quizzes);
                                        }
                                    }
                                    // level4 sections end
                                }
                            }
                            // level3 sections end

                        $output .= html_writer::end_tag('tbody');
                    }

                    $output .= html_writer::end_tag('table');
                $output .= html_writer::end_tag('div');
            }
            // level2 sections end

            $output .= html_writer::end_tag('li');
            $j++;
        }
        // level1 sections start
    } else {
        $output = html_writer::tag('div', get_string('nothingtodisplay', 'local_gradebook'), array('class'=>'alert alert-success'));
    }

    return $output;
}

function local_gradebook_get_quizzes_list($course, $section, $course_quizzes){
    global $DB, $CFG, $USER;
    $output = '';

    if (isset($course_quizzes[$section->id])){
        $i = 1;
        foreach($course_quizzes[$section->id] as $quiz){
            $output .= html_writer::start_tag('tr', array('class'=>'quiz-row'.((count($course_quizzes[$section->id]) == $i) ? ' last-row' : '')));
                $output .= html_writer::tag('td', html_writer::link(new moodle_url('/mod/quiz/view.php',              array('id' => $quiz->cmid)), $quiz->name), array('class'=>'name'));
                $output .= html_writer::tag('td', $quiz->status, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->attempts, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->grade, array('class'=>'grade'));
            $output .= html_writer::end_tag('tr');
            $i++;
        }
    }

    return $output;
}

function local_gradebook_get_course_quizzes($course){
    global $DB, $CFG, $USER;

    $items = array();
    $quizzes = $DB->get_records_sql("SELECT q.id, q.name, cm.section, cm.id as cmid, a.userattempts, cmc.completionstate, g.grade as quizgrade
                                            FROM {course_modules} cm
                                                LEFT JOIN {modules} m ON m.id = cm.module
                                                LEFT JOIN {quiz} q ON q.id = cm.instance
                                                LEFT JOIN (SELECT quiz, COUNT(id) as userattempts FROM {quiz_attempts} WHERE userid = :userid1 GROUP BY quiz) a ON a.quiz = q.id
                                                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid2
                                                LEFT JOIN {quiz_grades} g ON g.quiz = q.id AND g.userid = :userid3
                                        WHERE   cm.course = :course
                                                AND m.name = :modname
                                                AND q.reviewmarks > 0",
                                    array('userid1'=>$USER->id, 'userid2'=>$USER->id, 'userid3'=>$USER->id, 'course'=>$course->id, 'modname'=>'quiz'));
    if (count($quizzes)){
        foreach ($quizzes as $quiz){

            $quiz->attempts = ($quiz->userattempts) ? $quiz->userattempts : 0;
            $quiz->status = ($quiz->completionstate) ? get_string('complete', 'local_gradebook') : get_string('incomplete', 'local_gradebook');

            $mygrade = $quiz->quizgrade;
            $grading_info = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $USER->id);
            $grade_item = $grading_info->items[0];
            if (isset($grade_item->grades[$USER->id])) {
                $grade = $grade_item->grades[$USER->id];
                if ($grade->overridden) {
                    $mygrade = $grade->grade;
                }
            }
            $quiz->grade = local_gradebook_format_gradevalue($mygrade, $grade_item);

            $items[$quiz->section][$quiz->id] = $quiz;
        }
    }

    return $items;
}

/**
 * Returns a percentage representation of a grade value
 *
 * @param float $value The grade value
 * @param object $grade_item Grade item object
 * @param int $decimals The number of decimal places
 * @return string
 */
function local_gradebook_format_gradevalue($value, $grade_item) {
    if ($value === null){
        return '-';
    }

    $min = $grade_item->grademin;
    $max = $grade_item->grademax;
    if ($min == $max) {
        return '-';
    }
    $value = $value + 0;
    $percentage = (($value-$min)*100)/($max-$min);
    return format_float($percentage, 0).'%';
}



// institutes
function local_gradebook_get_course_sections($course) {
    global $PAGE, $DB;

    $sections = array();
    $allsections = $DB->get_records_sql("SELECT s.*, fs.parent, fs.level, fs.parentssequence, fs.sectiontype FROM {course_sections} s LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course WHERE s.course = $course->id AND s.section > 0 ORDER BY s.section");
    if (count($allsections)){
        foreach($allsections as $section){
            $sections[$section->parent][$section->id] = $section;
        }
    }

    return $sections;
}

function local_gradebook_ceu_display($course){
    global $CFG, $USER, $PAGE;

    $output = '';

    $course = course_get_format($course)->get_course();
    $format_renderer = $PAGE->get_renderer('format_institutes');

    $course_quizzes = local_gradebook_ceu_get_course_quizzes($course, $format_renderer);

    if (count($course_quizzes)){
        $output .= html_writer::start_tag('div', array('class'=>'course-sections'));

            $output .= html_writer::start_tag('table', array('class'=>'course-sections'));
                $output .= html_writer::start_tag('thead');
                    $output .= html_writer::start_tag('tr');
                        $output .= html_writer::tag('th', get_string('quizname', 'local_gradebook'), array('class'=>'name'));
                        $output .= html_writer::tag('th', get_string('status', 'local_gradebook'), array('class'=>'hidden_mobile'));
                        $output .= html_writer::tag('th', get_string('attempts', 'local_gradebook'), array('class'=>'hidden_mobile'));
                        $output .= html_writer::tag('th', get_string('grade', 'local_gradebook'), array('class'=>'grade'));
                    $output .= html_writer::end_tag('tr');
                $output .= html_writer::end_tag('thead');


                $output .= html_writer::start_tag('tbody');
                    $output .= local_gradebook_ceu_get_quizzes_list($course, $course_quizzes);
                $output .= html_writer::end_tag('tbody');

            $output .= html_writer::end_tag('table');
        $output .= html_writer::end_tag('div');
    } else {
        $output = html_writer::tag('div', get_string('nothingtodisplay', 'local_gradebook'), array('class'=>'alert alert-success'));
    }

    return $output;
}

function local_gradebook_ceu_get_quizzes_list($course, $course_quizzes){
    global $DB, $CFG, $USER, $PAGE;
    $output = '';

    $renderer = $PAGE->get_renderer('format_institutes');

    if (isset($course_quizzes)){
        $i = 1;
        foreach($course_quizzes as $quiz){
            $parentsection = $renderer->get_parent_section($course, $quiz->section);

            $output .= html_writer::start_tag('tr', array('class'=>'quiz-row'));
                $sectionname = html_writer::tag('strong', $quiz->sectionname);
                $output .= html_writer::tag('td', html_writer::link(new moodle_url('/mod/quiz/view.php',              array('id' => $quiz->cmid)), $sectionname.': '.$quiz->name), array('class'=>'name'));
                $output .= html_writer::tag('td', $quiz->status, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->attempts, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->grade, array('class'=>'grade'));
            $output .= html_writer::end_tag('tr');
            $i++;
        }
    }

    return $output;
}

function local_gradebook_ceu_get_course_quizzes($course, $format_renderer){
    global $DB, $CFG, $USER;

    $items = array();
    $quizzes = $DB->get_records_sql("SELECT q.id, q.name, cm.section, cm.id as cmid, a.userattempts, cmc.completionstate, g.grade as quizgrade, cs.name as sectionname, cs.section as sectionid
                                            FROM {course_modules} cm
                                                LEFT JOIN {modules} m ON m.id = cm.module
                                                LEFT JOIN {quiz} q ON q.id = cm.instance
                                                LEFT JOIN (SELECT quiz, COUNT(id) as userattempts FROM {quiz_attempts} WHERE userid = :userid1 GROUP BY quiz) a ON a.quiz = q.id
                                                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid2
                                                LEFT JOIN {quiz_grades} g ON g.quiz = q.id AND g.userid = :userid3
                                                LEFT JOIN {course_sections} cs ON cs.id = cm.section
                                        WHERE   cm.course = :course
                                                AND m.name = :modname
                                                AND q.reviewmarks > 0
                                            ORDER BY cs.section",
                                    array('userid1'=>$USER->id, 'userid2'=>$USER->id, 'userid3'=>$USER->id, 'course'=>$course->id, 'modname'=>'quiz'));
    if (count($quizzes)){

        $modinfo = get_fast_modinfo($course);

        foreach ($quizzes as $quiz){
            $quiz_section = $modinfo->get_section_info($quiz->sectionid);
            $quiz_section = $format_renderer->process_section_data($quiz_section);

            $showsection = $quiz_section->uservisible ||
                    ($quiz_section->visible && !$quiz_section->available &&
                    !empty($quiz_section->availableinfo));

            if (!$showsection or !$format_renderer->can_view_section($quiz_section)) {
                continue;
            }

            $quiz->attempts = ($quiz->userattempts) ? $quiz->userattempts : 0;
            $quiz->status = ($quiz->completionstate) ? get_string('complete', 'local_gradebook') : get_string('incomplete', 'local_gradebook');

            $mygrade = $quiz->quizgrade;
            $grading_info = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $USER->id);
            $grade_item = $grading_info->items[0];
            if (isset($grade_item->grades[$USER->id])) {
                $grade = $grade_item->grades[$USER->id];
                if ($grade->overridden) {
                    $mygrade = $grade->grade;
                }
            }
            $quiz->grade = local_gradebook_ceu_format_gradevalue($mygrade, $grade_item);

            $items[$quiz->id] = $quiz;
        }
    }

    return $items;
}

/**
 * Returns a percentage representation of a grade value
 *
 * @param float $value The grade value
 * @param object $grade_item Grade item object
 * @param int $decimals The number of decimal places
 * @return string
 */
function local_gradebook_ceu_format_gradevalue($value, $grade_item) {
    if ($value === null){
        return '-';
    }

    $min = $grade_item->grademin;
    $max = $grade_item->grademax;
    if ($min == $max) {
        return '-';
    }
    $value = $value + 0;
    $percentage = (($value-$min)*100)/($max-$min);
    return format_float($percentage, 0).'%';
}

function local_gradebook_ceu_get_course_sections($course) {
    global $PAGE, $DB;

    $sections = array();
    $allsections = $DB->get_records_sql("SELECT s.*, fs.parent, fs.level, fs.parentssequence FROM {course_sections} s LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course WHERE s.course = $course->id AND s.section > 0");
    if (count($allsections)){
        foreach($allsections as $section){
            $sections[$section->parent][$section->id] = $section;
        }
    }

    return $sections;
}


// ti gradebook
function local_gradebook_ti_display($course){
    global $CFG, $USER, $PAGE;

    $output = '';

    $course = course_get_format($course)->get_course();
    $format_renderer = $PAGE->get_renderer('format_institutes');
    $sections = local_gradebook_get_course_sections($course);
    $modinfo = get_fast_modinfo($course);

    $segmenttoggler = html_writer::start_tag('div', array('class' => 'nav-segment-toggler open'));
        $segmenttoggler .= html_writer::tag('span', '+', array('class'=>'show'));
        $segmenttoggler .= html_writer::tag('span', '-', array('class'=>'hide'));
    $segmenttoggler .= html_writer::end_tag('div');

    if (count($sections)){
        $course_quizzes = local_gradebook_ti_get_course_quizzes($course, $format_renderer);

        // level1 sections start
        $j = 1;
        $output .= html_writer::start_tag('ul', array('class'=>'course-level1-sections'));
        foreach ($sections[0] as $section1=>$thissection1){

            if (!can_see_course_section($thissection1->section, $modinfo, $format_renderer)){
                continue;
            }
            if (!isset($course_quizzes['sections_quizes'][$thissection1->id])){
                continue;
            }

            $output .= html_writer::start_tag('li', array('class'=>'course-section-box'));
            $toggler = html_writer::tag('i', '', array('class'=>'toggler ion-ios-arrow-right'));
            $sectionname = $toggler.get_section_name($course, $thissection1);

            $output .= html_writer::tag('div', $sectionname, array('class'=>'section-header'));

            // level2 sections start
            if (isset($sections[$thissection1->id]) and count($sections[$thissection1->id])){
                $output .= html_writer::start_tag('div', array('class'=>'course-level2-sections'));

                    $output .= html_writer::start_tag('table', array('class'=>'course-level2-sections'));
                        $output .= html_writer::start_tag('thead');
                            $output .= html_writer::start_tag('tr');
                                $output .= html_writer::tag('th', get_string('quizname', 'local_gradebook'), array('class'=>'name'));
                                $output .= html_writer::tag('th', get_string('status', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('attempts', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('grade', 'local_gradebook'), array('class'=>'grade'));
                            $output .= html_writer::end_tag('tr');
                        $output .= html_writer::end_tag('thead');

                    $output .= local_gradebook_ti_get_quizzes_list($course, $modinfo, $thissection1, $course_quizzes['items'], $format_renderer);

                    foreach ($sections[$thissection1->id] as $section2=>$thissection2){

                        if (!isset($course_quizzes['sections_quizes'][$thissection2->id])){
                            continue;
                        }

                        $output .= html_writer::start_tag('tbody');

                            $output .= html_writer::start_tag('tr');
                            $output .= html_writer::tag('td', (($thissection2->sectiontype == FORMAT_SECTIONS_SEGMENT) ? $segmenttoggler : '').get_section_name($course, $thissection2), array('class'=>'section-level2'.(($thissection2->sectiontype == FORMAT_SECTIONS_SEGMENT) ? ' segment-section' : ''), 'colspan'=>4, 'data-segment-id'=>(($thissection2->sectiontype == FORMAT_SECTIONS_SEGMENT) ? $thissection2->id : '')));
                            $output .= html_writer::end_tag('tr');

                            $output .= local_gradebook_ti_get_quizzes_list($course, $modinfo, $thissection2, $course_quizzes['items'], $format_renderer);

                            // level3 sections start
                            if (isset($sections[$thissection2->id]) and count($sections[$thissection2->id])){
                                foreach ($sections[$thissection2->id] as $section3=>$thissection3){

                                    if (!isset($course_quizzes['sections_quizes'][$thissection3->id])){
                                        continue;
                                    }

                                    if ($thissection2->sectiontype != FORMAT_SECTIONS_SEGMENT and $thissection2->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS){
                                        $output .= html_writer::start_tag('tr');
                                        $output .= html_writer::tag('td', (($thissection3->sectiontype == FORMAT_SECTIONS_SEGMENT) ? $segmenttoggler : '').get_section_name($course, $thissection3), array('class'=>'section-level3'.(($thissection3->sectiontype == FORMAT_SECTIONS_SEGMENT) ? ' segment-section' : ''), 'colspan'=>4, 'data-segment-id'=>(($thissection3->sectiontype == FORMAT_SECTIONS_SEGMENT) ? $thissection3->id : '')));
                                        $output .= html_writer::end_tag('tr');
                                    }

                                    $output .= local_gradebook_ti_get_quizzes_list($course, $modinfo, $thissection3, $course_quizzes['items'], $format_renderer);

                                    // level4 sections start
                                    if (isset($sections[$thissection3->id]) and count($sections[$thissection3->id])){
                                        foreach ($sections[$thissection3->id] as $section4=>$thissection4){
                                            if (!isset($course_quizzes['sections_quizes'][$thissection4->id])){
                                                continue;
                                            }

                                            $output .= local_gradebook_ti_get_quizzes_list($course, $modinfo, $thissection4, $course_quizzes['items'], $format_renderer);

                                            // level5 sections start
                                            if (isset($sections[$thissection4->id]) and count($sections[$thissection4->id])){
                                                foreach ($sections[$thissection4->id] as $section5=>$thissection5){
                                                    if (!isset($course_quizzes['sections_quizes'][$thissection5->id])){
                                                        continue;
                                                    }
                                                    $output .= local_gradebook_ti_get_quizzes_list($course, $modinfo, $thissection5, $course_quizzes['items'], $format_renderer);
                                                }
                                            }
                                        }
                                    }
                                    // level4 sections end
                                }
                            }
                            // level3 sections end

                        $output .= html_writer::end_tag('tbody');
                    }

                    $output .= html_writer::end_tag('table');
                $output .= html_writer::end_tag('div');
            }
            // level2 sections end

            $output .= html_writer::end_tag('li');
            $j++;
        }
        // level1 sections start
    } else {
        $output = html_writer::tag('div', get_string('nothingtodisplay', 'local_gradebook'), array('class'=>'alert alert-success'));
    }

    return $output;
}

function local_gradebook_ti_get_course_quizzes($course, $format_renderer){
    global $DB, $CFG, $USER;

    $items = array(); $sections_quizes = array();
    $quizzes = $DB->get_records_sql("SELECT q.id, q.name, cm.section, cm.id as cmid, a.userattempts, cmc.completionstate, g.grade as quizgrade, cs.name as sectionname, cs.section as sectionid
                                            FROM {course_modules} cm
                                                LEFT JOIN {modules} m ON m.id = cm.module
                                                LEFT JOIN {quiz} q ON q.id = cm.instance
                                                LEFT JOIN (SELECT quiz, COUNT(id) as userattempts FROM {quiz_attempts} WHERE userid = :userid1 GROUP BY quiz) a ON a.quiz = q.id
                                                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid2
                                                LEFT JOIN {quiz_grades} g ON g.quiz = q.id AND g.userid = :userid3
                                                LEFT JOIN {course_sections} cs ON cs.id = cm.section
                                        WHERE   cm.course = :course
                                                AND m.name = :modname
                                                AND q.reviewmarks > 0
                                            ORDER BY cs.section, q.name",
                                    array('userid1'=>$USER->id, 'userid2'=>$USER->id, 'userid3'=>$USER->id, 'course'=>$course->id, 'modname'=>'quiz'));
    if (count($quizzes)){

        $modinfo = get_fast_modinfo($course);

        foreach ($quizzes as $quiz){
            $quiz_section = $modinfo->get_section_info($quiz->sectionid);
            $quiz_section = $format_renderer->process_section_data($quiz_section);

            $showsection = $quiz_section->uservisible ||
                    ($quiz_section->visible && !$quiz_section->available &&
                    !empty($quiz_section->availableinfo));

            if (!$showsection or !$format_renderer->can_view_section($quiz_section)) {
                continue;
            }

            $quiz->attempts = ($quiz->userattempts) ? $quiz->userattempts : 0;
            $quiz->status = ($quiz->completionstate) ? get_string('complete', 'local_gradebook') : get_string('incomplete', 'local_gradebook');

            $mygrade = $quiz->quizgrade;
            $grading_info = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $USER->id);
            $grade_item = $grading_info->items[0];
            if (isset($grade_item->grades[$USER->id])) {
                $grade = $grade_item->grades[$USER->id];
                if ($grade->overridden) {
                    $mygrade = $grade->grade;
                }
            }
            $quiz->grade = local_gradebook_ceu_format_gradevalue($mygrade, $grade_item);

            $items[$quiz_section->id][$quiz->id] = $quiz;
            $sections_quizes[$quiz_section->id][$quiz->id] = $quiz->id;

            if (!empty($quiz_section->parentssequence)){
                $parentssequence = explode(',', $quiz_section->parentssequence);
                if (count($parentssequence)){
                    foreach ($parentssequence as $parent){
                        $sections_quizes[$parent][$quiz->id] = 1;
                    }
                }
            }
        }
    }

    return array('items'=>$items, 'sections_quizes'=>$sections_quizes);
}


function local_gradebook_ti_get_quizzes_list($course, $modinfo, $section, $course_quizzes, $format_renderer){
    global $DB, $CFG, $USER;
    $output = '';

    if (isset($course_quizzes[$section->id])){
        $i = 1;
        foreach($course_quizzes[$section->id] as $quiz){
            $segmentparent = $format_renderer->get_section_parent_by_type($course, $modinfo, $section, 5);
            $parentsection = $format_renderer->_sections[$section->parent];

            $segmentid = 0;
            if (isset($segmentparent->id)){
                $segmentid = $segmentparent->id;
            } elseif ($section->sectiontype == FORMAT_SECTIONS_SEGMENT){
                $segmentid = $section->id;
            }

            $output .= html_writer::start_tag('tr', array('class'=>'quiz-row'.((count($course_quizzes[$section->id]) == $i) ? ' last-row' : '').(($segmentid) ? ' segment-child segment-'.$segmentid : '')));

                $output .= html_writer::tag('td', html_writer::link(new moodle_url('/mod/quiz/view.php',              array('id' => $quiz->cmid)), html_writer::tag('strong', $quiz->name)), array('class'=>'name'));

                $output .= html_writer::tag('td', $quiz->status, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->attempts, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->grade, array('class'=>'grade'));
            $output .= html_writer::end_tag('tr');
            $i++;
        }
    }

    return $output;
}

function can_see_course_section($sectionid, $modinfo, $format_renderer){
    $section = $modinfo->get_section_info($sectionid);
    $section = $format_renderer->process_section_data($section);

    $showsection = $section->uservisible ||
            ($section->visible && !$section->available &&
            !empty($section->availableinfo));

    if (!$showsection or !$format_renderer->can_view_section($section)) {
        return false;
    }
    if ($section->sectiontype == FORMAT_SECTIONS_GENERAL or $section->sectiontype == FORMAT_SECTIONS_RESOURCES){
        return false;
    }

    return true;
}
