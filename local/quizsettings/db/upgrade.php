<?php

/**
 *
 * @package    local
 * @subpackage quizsettings
 * @copyright  2017 The Institutes.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function xmldb_local_quizsettings_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2016111402) {

        $table = new xmldb_table('quiz_settings');
        $field = new xmldb_field('displaysubmitpopup', XMLDB_TYPE_INTEGER, '1', null, null, null, '0', 'displayquestionworth');

        // Conditionally launch add field latest.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
    }

    return true;
}
