<?php

$capabilities = array(
    'local/quizsettings:editquizsettings' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
         'captype' => 'write',
        'contextlevel' => CONTEXT_MODULE,
        'archetypes' => array(
        	'editingteacher' => CAP_ALLOW,
        	'manager' => CAP_ALLOW
        ),

    ),
);