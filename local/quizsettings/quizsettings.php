<?php

require_once(dirname(__FILE__) . '../../../config.php');
include("quizsettings_form.php");

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $PAGE, $DB, $COURSE;

    $cmid = optional_param('cmid', '', PARAM_INT); // CourseModule ID.
    $quizid = optional_param('quizid', '', PARAM_INT); // Quiz ID
    $PAGE->set_pagelayout('admin');
    $PAGE->set_url('/local/quizsettings/quizsettings.php', array('cmid' => $cmid));

    // Basic access control checks.
    if ($cmid) {
        $courseModule = $DB->get_record('course_modules', array('id' => $cmid), '*', MUST_EXIST);
        $quizid = $courseModule->instance;
        $courseid = $courseModule->course; // Course ID
        $course = get_course($courseid);
        require_login($course);
        $context = context_module::instance($quizid);
    } else {
        $context = CONTEXT_SYSTEM::instance();
        require_login();
        $PAGE->set_context($context);
    }
    // Check capability
    if(has_capability('local/quizsettings:editquizsettings', $context)) {
        if (!$DB->record_exists('quiz_settings', array('quizid' => $quizid))) {
            $quizSettings = new \stdClass();
            $quizSettings->quizid = $quizid;
            $quizSettings->restrictaccess = 0;
            $quizSettings->enabledynamicexamsizing = 0;
            $quizSettings->displayquestionworth = 0;
            $quizSettings->displaysubmitpopup = 0;
            $DB->insert_record('quiz_settings', $quizSettings);
        }

        $quizSettings = $DB->get_record('quiz_settings', array('quizid'=>$quizid), '*', MUST_EXIST);

        $returnurl = new moodle_url($CFG->wwwroot . '/mod/quiz/view.php', array('q'=>$quizid ));

        // Instantiate Form
        $mform = new quizsettings_form(null, array('quizid' => $quizid, 'restrictaccess' => $quizSettings->restrictaccess, 'enabledynamicexamsizing' =>
            $quizSettings->enabledynamicexamsizing, 'displayquestionworth' => $quizSettings->displayquestionworth, 'displaysubmitpopup' => $quizSettings->displaysubmitpopup ));

        if ($mform->is_cancelled()) {
            redirect($returnurl);

        // If data submitted, then process and store.
        } else if ($data = $mform->get_data()) {
            $quizSettings = $DB->get_record('quiz_settings', array('quizid' => $quizid), '*', MUST_EXIST);
            $quizSettings->restrictaccess = empty($data->restrictaccess) ? 0 : 1;
            $quizSettings->enabledynamicexamsizing = empty($data->enabledynamicexamsizing) ? 0 : 1;
            $quizSettings->displayquestionworth = empty($data->displayquestionworth) ? 0 : 1;
            $quizSettings->displaysubmitpopup = empty($data->displaysubmitpopup) ? 0 : 1;
            $DB->update_record('quiz_settings', $quizSettings);
            redirect($returnurl);
        }

        //**********************
        //*** DISPLAY PAGE ***

        $pagetitle = get_string('pluginname', 'local_quizsettings');
        $PAGE->set_heading($course->shortname . ': ' . $pagetitle);
        $PAGE->set_title($pagetitle);
        echo $OUTPUT->header();
        echo $OUTPUT->heading($course->fullname);
        $mform->display();
        echo $OUTPUT->footer();

    } else { // If does not have capability redirect to quiz view
        $returnurl = new moodle_url($CFG->wwwroot . '/mod/quiz/view.php', array('q'=>$quizid));
        redirect($returnurl);
    }
}

display_page();
