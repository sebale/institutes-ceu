<?php

$string["pluginname"] = "Manage Quiz Settings";
$string["quizsettingshelp"] = "Manage Quiz Settings";
$string["quizsettingshelp_help"] = "Check or uncheck specific settings for each quiz. Remember to click Save Changes.";
$string["quizsettings:editquizsettings"] = "Edit Quiz Settings";
$string["restrictaccessflag"] = "Restrict Access to the Quiz";
$string["enabledynamicexamsizingflag"] = "Enable Dynamic Exam Sizing";
$string["displayquestionworthflag"] = "Display Question Worth";
$string["displaysubmitpopup"] = "Display Submit Popup";
