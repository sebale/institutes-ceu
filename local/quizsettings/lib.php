<?php

require_once(dirname(__FILE__) . '../../../config.php');
/**
 * Update the navigation block with newplugin options
 * @global moodle_page $PAGE
 * @param settings_navigation $settingnav, context $context
 */

function local_quizsettings_extend_settings_navigation($settingsnav, $context) {

    global $PAGE;
  
    //Check whether the course module is quiz. Module id for quiz is 16 
    if (isset($PAGE->cm->module) && $PAGE->cm->module == 16){
        // Check capability
        if (has_capability('local/quizsettings:editquizsettings', $context)) {
            if ($settingnode = $settingsnav->find('modulesettings', navigation_node::TYPE_SETTING)) {
                $colnm = get_string('pluginname', 'local_quizsettings');
                $url = new moodle_url('/local/quizsettings/quizsettings.php', array('cmid' => $PAGE->cm->id));
                $colnode = navigation_node::create(
                    $colnm,
                    $url,
                    navigation_node::NODETYPE_LEAF,
                    'quizsettings',
                    'quizsettings',
                    new pix_icon('t/addcontact', $colnm)
                );
                $settingnode->add_node($colnode);
            }
        }
    }     
};
