<?php

/**
 * This is a plugin form for changing the settings for each quiz associated with a course
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    local/quizsettings
 * @copyright  The Institutes.org
 */
defined('MOODLE_INTERNAL') || die();

require_once("$CFG->dirroot/lib/formslib.php");

/**
 * Module instance settings form
 */
class quizsettings_form extends moodleform {

    function definition() {
        $mform = $this->_form;

        $id  = $this->_customdata['quizid'];
        $restrictaccess  = $this->_customdata['restrictaccess'];
        $enabledynamicexamsizing  = $this->_customdata['enabledynamicexamsizing'];
        $displayquestionworth  = $this->_customdata['displayquestionworth'];
        $displaysubmitpopup  = $this->_customdata['displaysubmitpopup'];

        $mform->addElement('header', 'general', get_string('pluginname', 'local_quizsettings'));
        $mform->addHelpButton('general','quizsettingshelp', 'local_quizsettings');

        $mform->addElement('checkbox', 'restrictaccess', get_string('restrictaccessflag', 'local_quizsettings'));
        $mform->addElement('checkbox', 'enabledynamicexamsizing', get_string('enabledynamicexamsizingflag', 'local_quizsettings'));
        $mform->addElement('checkbox', 'displayquestionworth', get_string('displayquestionworthflag', 'local_quizsettings'));
        $mform->addElement('checkbox', 'displaysubmitpopup', get_string('displaysubmitpopup', 'local_quizsettings'));

        $mform->setDefault('restrictaccess', $restrictaccess);
        $mform->setDefault('enabledynamicexamsizing', $enabledynamicexamsizing);
        $mform->setDefault('displayquestionworth', $displayquestionworth);
        $mform->setDefault('displaysubmitpopup', $displaysubmitpopup);

        $mform->addElement('hidden', 'quizid', $id);
        $mform->setType('quizid', PARAM_INT);

        // add standard buttons, common to all modules
        $this->add_action_buttons();

    }

}
