<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Institutes lang file
 *
 * @package    local_bookmarks
 * @copyright  2017 The Institutes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Bookmarks';
$string['gradebook_ceu:view'] = 'Bookmarks View';
$string['savedbookmarks'] = 'Saved bookmarks';
$string['addbookmarks'] = 'Add bookmark';
$string['name'] = 'Name:';
$string['savebookmark'] = 'Save bookmark';
$string['havenotbookmarks'] = 'You have not saved bookmarks';
$string['editname'] = 'Edit name';
$string['bookmarks:view'] = 'View bookmarks';

