<?php

function local_bookmarks_display(){
    global $DB, $CFG, $USER, $PAGE, $OUTPUT;
    
    $output = '';
    $bookmarks = $DB->get_records('local_bookmarks', array('userid'=>$USER->id), 'timecreated DESC');
    
    $output .= html_writer::start_tag('div', array('class' => 'bookmarks-box'));
    
    $output .= html_writer::start_tag('div', array('class' => 'bookmarks-form'));
    $output .= html_writer::tag('div', get_string('addbookmarks', 'local_bookmarks').'<i class="close fa fa-close" onclick="bookmarksClose();"></i>', array('class' => 'bookmarks-form-title', 'title'=>'Close'));
    $output .= html_writer::start_tag('form', array('action' => '', 'method'=>'POST', 'class'=>'clearfix'));
    $output .= html_writer::tag('label', get_string('name', 'local_bookmarks'));
    $output .= html_writer::empty_tag('input', array('name'=>'bookmark_name', 'type'=>'text', 'value'=>$OUTPUT->page_title(), 'id'=>'bookmark_name'));
    $output .= html_writer::empty_tag('input', array('name'=>'bookmark_id', 'type'=>'hidden', 'value'=>'0', 'id'=>'bookmark_id'));
    $output .= html_writer::empty_tag('input', array('name'=>'bookmark_userid', 'type'=>'hidden', 'value'=>$USER->id, 'id'=>'bookmark_userid'));
    $output .= html_writer::link('javascript:void(0);', get_string('savebookmark', 'local_bookmarks'), array('onclick'=>'bookmarkSave();'));
    $output .= html_writer::end_tag('form');
    
    $output .= html_writer::end_tag('div');
    
    $output .= html_writer::tag('div', get_string('savedbookmarks', 'local_bookmarks'), array('class' => 'saved-bookmarks-title'.((count($bookmarks)) ? '' : ' hidden')));
    $output .= html_writer::start_tag('ul', array('class' => 'bookmarks-list'));
    
    if (count($bookmarks)){
        foreach ($bookmarks as $record) {
            $output .= html_writer::start_tag('li', array('class' => 'bookmark-item'.(($CFG->wwwroot.$record->url == $PAGE->url) ? ' current' : ''), 'data-id' => $record->id));
                $output .= html_writer::link($CFG->wwwroot.$record->url, $record->name, array('class'=>'bookmark-link'));
                $output .= html_writer::start_tag('div', array('class' => 'bookmark-actions'));
                    $output .= html_writer::link('javascript:void(0);', get_string('editname', 'local_bookmarks'), array('onclick'=>'bookmarksEdit('.$record->id.');'));
                    $output .= html_writer::tag('i', '', array('class' => 'fa fa-circle'));
                    $output .= html_writer::link('javascript:void(0);', get_string('delete'), array('onclick'=>'bookmarksDelete('.$record->id.');'));
                $output .= html_writer::end_tag('div');
            $output .= html_writer::end_tag('li');
        }
    }
    $output .= html_writer::end_tag('ul');
    
    $output .= html_writer::end_tag('div');
    
    return $output;
}


