<?php

/**
 * Version details
 *
 * @package    block
 * @subpackage coursesettings
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2016112900;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013110500;        // Requires this Moodle version
$plugin->component = 'block_sb_coursesettings'; // Full name of the plugin (used for diagnostics)
