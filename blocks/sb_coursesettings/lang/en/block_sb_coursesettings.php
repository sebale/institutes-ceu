<?php

/**
 * Strings for component 'block_sb_coursenav', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    local_sb_announcements
 * @copyright  2017 The Institutes
 */

$string['sb_coursesettings:addinstance'] = 'Add a new Course Format Settings block';
$string['sb_coursesettings:myaddinstance'] = 'Add a new Course Format Settings block to My home';
$string['pluginname'] = 'Course Format Settings';
$string['blocktitle'] = 'Course Settings';
$string['resourcessettings'] = 'Resources Settings';
$string['menusettings'] = 'Menu Settings';
$string['topicview'] = 'Switch to Topic View';
$string['institutesview'] = 'Back to Institutes View';
$string['customresources'] = 'Custom Resources';
$string['courseresources'] = 'Course Resources';
