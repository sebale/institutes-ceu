<?php

/**
 * Handles displaying the sb coursesettings.
 *
 * @package    block_sb_coursesettings
 * @copyright  2017 The Institutes
 */
class block_sb_coursesettings extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('blocktitle', 'block_sb_coursesettings');
    }
    function hide_header() {
        return false;
    }
    function html_attributes() {
        $attributes = parent::html_attributes();
        $attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        $course = $this->page->course;
        $cm     = $this->page->cm;
        $issite = ($course->id == SITEID);
        $context = context_course::instance($course->id, MUST_EXIST);

        if ($issite) return '';

        $this->content->text .= html_writer::start_tag('ul');


            if (has_capability('format/institutes_ab:manageresources', $context) and $course->format == 'institutes_ab'){
                $this->content->text .= html_writer::start_tag('li');
                    $this->content->text .= html_writer::link(new moodle_url('/course/format/'.$course->format.'/resourcessettings.php', array('id'=>$course->id)), get_string('resourcessettings', 'block_sb_coursesettings'));
                $this->content->text .= html_writer::end_tag('li');
            }

            $this->content->text .= html_writer::start_tag('li');
                $this->content->text .= html_writer::link(new moodle_url('/course/format/'.$course->format.'/menusettings.php', array('id'=>$course->id)), get_string('menusettings', 'block_sb_coursesettings'));
            $this->content->text .= html_writer::end_tag('li');

            if ($this->page->user_is_editing() and has_capability('moodle/course:update', $context) and stristr($this->page->url, '/course/view.php') and $course->format == 'institutes_ab') {
                $courseview = get_user_preferences('courseview_'.$course->id, 'institutes');
                $this->content->text .= html_writer::start_tag('li');
                    if ($courseview == 'institutes'){
                        $this->content->text .= html_writer::link(new moodle_url('/course/format/'.$course->format.'/toggleview.php', array('id'=>$course->id, 'view'=>'topic')), get_string('topicview', 'block_sb_coursesettings'));
                    } else {
                        $this->content->text .= html_writer::link(new moodle_url('/course/format/'.$course->format.'/toggleview.php', array('id'=>$course->id, 'view'=>'institutes')), get_string('institutesview', 'block_sb_coursesettings'));
                    }
                $this->content->text .= html_writer::end_tag('li');
            }

            if ($course->format == 'institutes'){
                if (has_capability('format/institutes:managenotes', $context)){
                    $this->content->text .= html_writer::start_tag('li');
                        $this->content->text .= html_writer::link(new moodle_url('/course/format/'.$course->format.'/notes/index.php', array('id'=>$course->id)), get_string('coursenotes', 'format_institutes'));
                    $this->content->text .= html_writer::end_tag('li');
                }
            }

        $this->content->text .= html_writer::end_tag('ul');

        return $this->content;
    }

}
