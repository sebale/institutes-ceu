<?php

/**
 *
 * @package    block_sb_coursenav
 * @copyright  2017 The Institutes
 */
class block_sb_coursenav extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_coursenav');
    }
    function hide_header() {
        return true;
    }
    function html_attributes() {
        $attributes = parent::html_attributes();
        $attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if ($this->page->course->id == 1) return $this->content;

        if ($this->page->course->format == 'institutes'){
            $this->content->text = $this->print_institutes_ti_navigation();
        } else {
            $this->content->text = $this->print_institutes_navigation();
        }

        return $this->content;
    }

    public function print_institutes_navigation(){
        global $CFG, $DB, $USER, $OUTPUT;

        $output = '';

        $course = $this->page->course;
        $cm     = $this->page->cm;
        $issite = ($course->id == SITEID);
        $context = context_course::instance($course->id, MUST_EXIST);

        $is_enrolled = (is_enrolled($context, $USER));

        require_once($CFG->dirroot.'/course/lib.php');
        $course = course_get_format($course)->get_course();
        course_create_sections_if_missing($course, range(0, $course->numsections));

        $modfullnames = array(); $archetypes = array();

        $displaysection = optional_param('section', 0, PARAM_INT);
        $modinfo = get_fast_modinfo($course);
        $csections = $this->get_course_sections($course);
        if (isset($cm->section)){
            $cm_section = $DB->get_record('course_sections', array('id'=>$cm->section));
        }

        $sections = array('root'=>array());
        $currentsectionid = (isset($cm_section->section)) ? $cm_section->section : $displaysection;
        $open_sections = array();
        if ($currentsectionid > 0){
            $currentsection = $modinfo->get_section_info($currentsectionid);
            $currentsection->parent = (isset($csections[$currentsection->id]->parent)) ? $csections[$currentsection->id]->parent : 0;
            if (isset($csections[$currentsection->id]->parentssequence) and !empty($csections[$currentsection->id]->parentssequence)){
                $open_sections = explode(',',$csections[$currentsection->id]->parentssequence);
            }
        }

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) continue;
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) continue;

            $thissection->parent = (isset($csections[$thissection->id]->parent)) ? $csections[$thissection->id]->parent : 0;
            if (intval($thissection->parent) > 0){
                $sections[$thissection->parent][$thissection->section] = $thissection;
            } else {
                $sections['root'][$thissection->section] = $thissection;
            }
        }

        $output .= html_writer::start_tag('div', array('class'=>'course-navigation-block'));

        $output .= html_writer::start_tag('div', array('class'=>'course-content-box'));

        // root sections start
        $output .= html_writer::start_tag('ul', array('class'=>'course-root-sections'));

        if (count($sections['root'])){
            $format_renderer = $this->page->get_renderer('format_institutes_ab');
            $j = 1;
            foreach ($sections['root'] as $section=>$thissection){
                $thissection->progress = $format_renderer->get_section_completion($course, $section);

                $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection->progress['status'].((($currentsectionid > 0 and ($currentsectionid == $section or in_array($thissection->id, $open_sections))) ) ? ' open' : '')));

                $counter = html_writer::tag('span', ((strlen($j) > 1) ? $j : '0'.$j), array('class'=>'section-counter'));
                if (isset($sections[$thissection->id]) and count($sections[$thissection->id])){
                    $output .= html_writer::tag('span', '', array('class'=>'toggler'));
                }

                $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section.'" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $section) ? ' current' : '').'">';
                if ($thissection->progress['status'] == 'completed'){
                    $output .= html_writer::tag('i', '', array('class'=>'status ion-checkmark-round'));
                }
                $output .= $counter.get_section_name($course, $thissection);
                $output .= '</a>';

                // level1 sections start
                if (isset($sections[$thissection->id]) and count($sections[$thissection->id])){
                    $output .= html_writer::start_tag('ul', array('class'=>'course-level1-sections'));
                        foreach ($sections[$thissection->id] as $section1=>$thissection1){
                            $thissection1->progress = $format_renderer->get_section_completion($course, $section1);

                            $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection1->progress['status']));
                            if ($this->page->user_is_editing()){
                                $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section1.'" title="'.get_section_name($course, $thissection1).'" alt="'.get_section_name($course, $thissection1).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $section1) ? ' current' : '').'">'.get_section_name($course, $thissection1).'</a>';
                            } else {
                                $output .= '<a href="javascript:void(0);" title="'.get_section_name($course, $thissection1).'" alt="'.get_section_name($course, $thissection1).'" class="section-header without-link'.(($currentsectionid > 0 and $currentsectionid == $section1) ? ' current' : '').'">'.get_section_name($course, $thissection1).'</a>';
                            }

                            // level2 sections start
                            if (isset($sections[$thissection1->id]) and count($sections[$thissection1->id])){
                                $output .= html_writer::start_tag('ul', array('class'=>'course-level2-sections'));
                                    foreach ($sections[$thissection1->id] as $section2=>$thissection2){
                                        $thissection2->progress = $format_renderer->get_section_completion($course, $section2);

                                        $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection2->progress['status'].(((isset($cm_section->section) and $cm_section->section == $section2) || ($displaysection > 0 and $displaysection == $section2)) ? ' active' : '')));
                                            $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section2.'" title="'.get_section_name($course, $thissection2).'" alt="'.get_section_name($course, $thissection2).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $section2 || $currentsection->parent == $thissection2->id) ? ' current' : '').'">';
                                            if ($thissection2->progress['status'] == 'completed'){
                                                $output .= html_writer::tag('i', '', array('class'=>'status ion-checkmark-round'));
                                            }
                                            $output .= get_section_name($course, $thissection2);
                                            $output .= '</a>';
                                        $output .= html_writer::end_tag('li');
                                    }
                                $output .= html_writer::end_tag('ul');
                            }
                            // level2 sections end

                            $output .= html_writer::end_tag('li');
                        }
                    $output .= html_writer::end_tag('ul');
                }
                // level1 sections start

                $output .= html_writer::end_tag('li');
                $j++;
            }
        }

        $output .= html_writer::end_tag('ul');
        // root sections end

        $output .= html_writer::end_tag('div');

        // end block wrapper
        $output .= html_writer::end_tag('div');

        $output .= '<script>
                                jQuery(".course-section-box .toggler").click(function(e){
                                    jQuery(this).parent().toggleClass("open");
                                });
                                </script>';
        return $output;
    }

    public function print_institutes_ti_navigation(){
        global $CFG, $DB, $USER, $OUTPUT;

        $output = '';

        $course = $this->page->course;
        $cm     = $this->page->cm;
        $issite = ($course->id == SITEID);
        $context = context_course::instance($course->id, MUST_EXIST);

        $is_enrolled = (is_enrolled($context, $USER));

        require_once($CFG->dirroot.'/course/lib.php');
        $course = course_get_format($course)->get_course();
        course_create_sections_if_missing($course, range(0, $course->numsections));
        $format_renderer = $this->page->get_renderer('format_institutes');

        $modfullnames = array(); $archetypes = array();

        $displaysection = optional_param('section', 0, PARAM_INT);
        $modinfo = get_fast_modinfo($course);
        $csections = $this->get_course_sections($course);
        if (isset($cm->section)){
            $cm_section = $DB->get_record('course_sections', array('id'=>$cm->section));
        }

        $sections = array('root'=>array());
        $currentsectionid = (isset($cm_section->section)) ? $cm_section->section : $displaysection;
        $open_sections = array();
        if ($currentsectionid > 0){
            $currentsection = $modinfo->get_section_info($currentsectionid);
            $currentsection->parent = (isset($csections[$currentsection->id]->parent)) ? $csections[$currentsection->id]->parent : 0;
            if (isset($csections[$currentsection->id]->parentssequence) and !empty($csections[$currentsection->id]->parentssequence)){
                $open_sections = explode(',',$csections[$currentsection->id]->parentssequence);
            }
        }

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) continue;
            $thissection = $format_renderer->process_section_data($thissection);

            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) continue;

            if (!$format_renderer->can_view_section($thissection)){
                continue;
            }

            $thissection->parent = (isset($csections[$thissection->id]->parent)) ? $csections[$thissection->id]->parent : 0;
            if (intval($thissection->parent) > 0){
                $sections[$thissection->parent][$thissection->section] = $thissection;
            } else {
                if ($thissection->sectiontype == FORMAT_SECTIONS_GENERAL or $thissection->sectiontype == FORMAT_SECTIONS_RESOURCES) continue;
                $sections['root'][$thissection->section] = $thissection;
            }
        }

        $output .= html_writer::start_tag('div', array('class'=>'course-navigation-block ti-course-navigation'));

        $output .= html_writer::start_tag('div', array('class'=>'course-content-box'));

        // root sections start
        $output .= html_writer::start_tag('ul', array('class'=>'course-root-sections'));

        if (count($sections['root'])){
            $j = 1;
            foreach ($sections['root'] as $section=>$thissection){

                $thissection->progress = $format_renderer->get_section_completion($course, $section);

                $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection->progress['status'].((($currentsectionid > 0 and ($currentsectionid == $section or in_array($thissection->id, $open_sections))) ) ? ' open' : '')));

                if (isset($sections[$thissection->id]) and count($sections[$thissection->id])){
                    $output .= html_writer::tag('span', '', array('class'=>'toggler'));
                }

                $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section.'" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $section) ? ' current' : '').'">';
                if ($thissection->progress['status'] == 'completed'){
                    $output .= html_writer::tag('i', '', array('class'=>'status ion-checkmark-round'));
                }
                $output .= get_section_name($course, $thissection);
                $output .= '</a>';

                // level1 sections start
                if (isset($sections[$thissection->id]) and count($sections[$thissection->id])){
                    $output .= html_writer::start_tag('ul', array('class'=>'course-level1-sections'));
                        foreach ($sections[$thissection->id] as $section1=>$thissection1){
                            $thissection1->progress = $format_renderer->get_section_completion($course, $section1);

                            $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection1->progress['status'].(($thissection1->sectiontype == FORMAT_SECTIONS_SEGMENT) ? ' segment-section-box' : '').((($currentsectionid > 0 and ($currentsectionid == $section1 or in_array($thissection1->id, $open_sections))) ) ? ' open' : '')));

                            $output .= $this->print_menu_item($course, $thissection1, $section, $thissection, $currentsectionid, $format_renderer);


                            // level2 sections start
                            if (isset($sections[$thissection1->id]) and count($sections[$thissection1->id])){
                                $output .= html_writer::start_tag('ul', array('class'=>'course-level2-sections'));
                                    foreach ($sections[$thissection1->id] as $section2=>$thissection2){
                                        $thissection2->progress = $format_renderer->get_section_completion($course, $section2);

                                        if (($thissection2->sectiontype == FORMAT_SECTIONS_SEGMENT and !isset($sections[$thissection2->id])) or ( isset($sections[$thissection2->id]) and !count($sections[$thissection2->id]))){
                                            continue;
                                        }
                                        if ($thissection1->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS){
                                            continue;
                                        }

                                        $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection2->progress['status'].(($thissection2->sectiontype == FORMAT_SECTIONS_SEGMENT) ? ' segment-section-box' : '').((($currentsectionid > 0 and ($currentsectionid == $section2 or in_array($thissection2->id, $open_sections))) ) ? ' open' : '')));

                                        $output .= $this->print_menu_item($course, $thissection2, $section, $thissection1, $currentsectionid, $format_renderer);

                                        // level3 sections start
                                        if (isset($sections[$thissection2->id]) and count($sections[$thissection2->id]) and $thissection1->sectiontype != FORMAT_SECTIONS_SEGMENT and $thissection2->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS){
                                            $output .= html_writer::start_tag('ul', array('class'=>'course-level3-sections'));
                                                foreach ($sections[$thissection2->id] as $section3=>$thissection3){
                                                    $thissection3->progress = $format_renderer->get_section_completion($course, $section3);

                                                    $output .= html_writer::start_tag('li', array('class'=>'course-section-box '.$thissection3->progress['status'].(((isset($cm_section->section) and $cm_section->section == $section3) || ($displaysection > 0 and $displaysection == $section3)) ? ' active' : '')));

                                                        $output .= $this->print_menu_item($course, $thissection3, $section, $thissection2, $currentsectionid, $format_renderer);

                                                    $output .= html_writer::end_tag('li');
                                                }
                                            $output .= html_writer::end_tag('ul');
                                        }

                                        $output .= html_writer::end_tag('li');
                                    }
                                $output .= html_writer::end_tag('ul');
                            }
                            // level2 sections end

                            $output .= html_writer::end_tag('li');
                        }
                    $output .= html_writer::end_tag('ul');
                }
                // level1 sections start

                $output .= html_writer::end_tag('li');
                $j++;
            }
        }

        $output .= html_writer::end_tag('ul');
        // root sections end

        $output .= html_writer::end_tag('div');

        // end block wrapper
        $output .= html_writer::end_tag('div');

        $output .= '<script>
                        jQuery(".course-section-box .toggler").click(function(e){
                            jQuery(this).parent().toggleClass("open");
                        });
                        jQuery(".segment-section").click(function(e){
                            jQuery(this).parent().toggleClass("open");
                        });
                    </script>';
        return $output;
    }

    public function get_course_sections($course) {
        global $PAGE, $DB;

        $sections = array();
        $allsections = $DB->get_records_sql("SELECT s.*, fs.parent, fs.level, fs.parentssequence FROM {course_sections} s LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course WHERE s.course = $course->id");
        if (count($allsections)){
            foreach($allsections as $section){
                $sections[$section->id] = $section;
            }
        }

        return $this->_sections = $sections;
    }

    public function print_menu_item($course, $thissection, $rootsection = 0, $parentsection, $currentsectionid = 0, $format_renderer) {
        global $CFG;

        $output = '';
        $completion = '';
        if ($thissection->progress['status'] == 'completed'){
            $completion = html_writer::tag('i', '', array('class'=>'status ion-checkmark-round'));
        }

        if ($thissection->sectiontype == FORMAT_SECTIONS_SEGMENT){
            $segmenttoggler = html_writer::start_tag('div', array('class' => 'nav-segment-toggler'));
                $segmenttoggler .= html_writer::tag('span', '+', array('class'=>'show'));
                $segmenttoggler .= html_writer::tag('span', '-', array('class'=>'hide'));
            $segmenttoggler .= html_writer::end_tag('div');

            $output .= '<a href="javascript:void(0);" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header segment-section">'.$segmenttoggler.get_section_name($course, $thissection).'</a>';
        } elseif ($parentsection->sectiontype == FORMAT_SECTIONS_SEGMENT or $thissection->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS) {
            $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$thissection->section.'" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $thissection->section) ? ' current' : '').'">'.$completion.get_section_name($course, $thissection).'</a>';
        } else {
            $output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$rootsection.'#section-'.$thissection->section.'" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header'.(($currentsectionid > 0 and $currentsectionid == $thissection->section) ? ' current' : '').'">'.$completion.get_section_name($course, $thissection).'</a>';
        }

        return $output;
    }
}
