<?php

/**
 * Strings for component 'block_sb_coursenav'
 *
 * @package    local_sb_announcements
 * @copyright  2017 The Institutes
 */

$string['sb_coursenav:addinstance'] = 'Add a new Course Navigation block';
$string['sb_coursenav:myaddinstance'] = 'Add a new Course Navigation block to My home';
$string['pluginname'] = 'Course Navigation';

