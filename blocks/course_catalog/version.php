<?php

/**
 * Version details
 *
 * @package    block_course_catalog
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015061801;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014050800;        // Requires this Moodle version
$plugin->component = 'block_course_catalog'; // Full name of the plugin (used for diagnostics)
