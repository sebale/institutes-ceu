<?php

/**
 * The maintenance layout.
 *
 * @package   theme_institutes_ti
 * @copyright  2017 The Institutes
 */

// Get the HTML for the settings bits.
$html = theme_institutes_ti_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link rel="apple-touch-icon" sizes="48x48" href="<?php echo $CFG->wwwroot; ?>/theme/institutes_ti/pix/favicon_48x48.png">
    <link rel="icon" type="image/png" href="<?php echo $CFG->wwwroot; ?>/theme/institutes_ti/pix/favicon_32x32.png" sizes="32x32">
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<div id="page" class="container-fluid">
    <header id="page-header" class="clearfix">
        <?php echo $html->heading; ?>
    </header>
    <div id="page-content" class="row-fluid">
        <section id="region-main" class="span12">
            <?php echo $OUTPUT->main_content(); ?>
        </section>
    </div>
    <footer id="page-footer">
        <?php
        echo $OUTPUT->standard_footer_html();
        ?>
    </footer>
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
</div>
</body>
</html>
