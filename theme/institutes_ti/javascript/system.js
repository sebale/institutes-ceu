function toggleLeftSidebar(){

    if (jQuery('.side-post').hasClass('open')){
         toggleRightSidebar();
    }

    jQuery('.left-sidebar').toggleClass('open');
    jQuery('#page.container-fluid').toggleClass('openmenu');

    if($('.left-sidebar.active').length) {
        $('#page-content.row-fluid.active-sidebar').toggleClass('open');
    }
    M.util.set_user_preference('fix-sidebar', jQuery('.left-sidebar').hasClass('open')?1:0);
}

function toggleRightSidebar() {
    if (jQuery('#page.container-fluid').hasClass('openmenu')){
        toggleLeftSidebar();
    }
    if (jQuery('.side-post').hasClass('open')){
        jQuery('#region-main-box').addClass('span12 pull-right').removeClass('span9');
        jQuery('#page').addClass('full-width');
        jQuery('.side-post').removeClass('open');
    } else {
        jQuery('#region-main-box').addClass('span9').removeClass('span12 pull-right');
        jQuery('#page').removeClass('full-width');

        jQuery('.side-post').toggleClass('open');
        // jQuery('.main-navigation li.activecourse').toggleClass('active');
    }
    jQuery('.main-navigation li.activecourse').toggleClass('active');
}



function toggleCourseMenu() {
    if (!jQuery('.left-sidebar').hasClass('open')){
        toggleLeftSidebar();
    }

    jQuery('.main-navigation li.activecourse').toggleClass('active');
}

function instructionsPopupClose(){
    jQuery('.instructions-popup-box').removeClass('open');
}

function instructionsPopupOpen(id){
    instructionsPopupClose();
    jQuery('#instruction_'+id).addClass('open');
}

function resourcePopupClose(){
    jQuery('.instructions-popup-box').removeClass('open');
}

function resourcePopupOpen(id){
    resourcePopupClose();
    jQuery('#resource_'+id).addClass('open');
}

console.log('add it')

// setTimeout(function(){
//     (function($){
//         console.log('init')
//         if( $('body').width() < 1000 ){
//             console.log('go')
//             $('.segment-sections-box li.segment-sections-item > div:nth-child(1)').attr('style', 'width:100% !important;');
//         }
//     })(jQuery);
// }, 3000);

