<?php

/**
 * @package    theme_institutes_ti
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2017022100;
$plugin->requires  = 2016051900;
$plugin->component = 'theme_institutes_ti';
$plugin->dependencies = array(
    'theme_bootstrapbase'   => 2016051900,
    'theme_institutes'      => 2016052303,
    'format_institutes' => 20170222000,
    'local_bookmarks'       => 2017030101,
);
