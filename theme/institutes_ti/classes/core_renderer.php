<?php
/**
 * institutes_ti core renderers.
 *
 * @package    theme_institutes_ti
 * @copyright  2017 The Institutes
 */

require_once($CFG->dirroot . '/theme/bootstrapbase/renderers.php');
require_once($CFG->dirroot . '/theme/institutes/classes/core_renderer.php');

class theme_institutes_ti_core_renderer extends theme_institutes_core_renderer {

    /**
     * Return the navbar content so that it can be echoed out by the layout
     *
     * @return string XHTML navbar
     */
    public function navbar() {
        global $DB, $PAGE, $CFG;

        $items = $this->page->navbar->get_items();
        $itemcount = count($items);
        if ($itemcount === 0) {
            return '';
        }

        $course = $this->page->course;
        if ($course->id == 1) return '';

        require_once($CFG->dirroot.'/course/format/institutes/lib.php');
        $course_sections = format_institutes_get_course_sections($course);

        $displaysection = optional_param('section', 0, PARAM_INT);
        if ($displaysection > 0){
            $section = $DB->get_record('course_sections', array('section'=>$displaysection, 'course'=>$course->id));
            $displaysection = (isset($section->id)) ? $section->id : 0;
        }
        $cm = (isset($this->page->cm->id)) ? $this->page->cm : null;

        $currentsectionid = (isset($cm->section)) ? $cm->section : $displaysection;
        if ($currentsectionid == 0 and !stristr($PAGE->url, '/local/gradebook') and !stristr($PAGE->url, '/course/format/institutes/resources.php') and !stristr($PAGE->url, '/course/format/institutes/contactus.php')) return '';

        $page_url = '/course/view.php';

        if ($currentsectionid > 0){
            $currentsection = $course_sections[$currentsectionid];
            $parentsection = format_institutes_get_sections_parent($course, $currentsection, 0, true);
            if (isset($parentsection->sectiontype) and $parentsection->sectiontype == FORMAT_SECTIONS_RESOURCES and !$PAGE->user_is_editing()){
                $page_url = '/course/format/institutes/resources.php';
            }
        }

        $htmlblocks = array();
        // Iterate the navarray and display each node
        $separator = ' > ';
        for ($i=0;$i < $itemcount;$i++) {
            $item = $items[$i];

            if ($i == 0) continue;

            if ($item->type != global_navigation::TYPE_COURSE and $item->type != global_navigation::TYPE_ACTIVITY and $item->type != global_navigation::TYPE_RESOURCE and $item->type != global_navigation::TYPE_CUSTOM) continue;
            $item->hideicon = true;

            if ($item->type == global_navigation::TYPE_COURSE){
                $item->text = $item->title = $item->shorttext = get_string('home', 'theme_institutes');
            }

            $options = ($item->type == global_navigation::TYPE_ACTIVITY or $item->type == global_navigation::TYPE_RESOURCE or $item->type == global_navigation::TYPE_CUSTOM) ? array('class'=>'active') : array();

            if ($i===0 or count($htmlblocks) == 0) {
                $content = html_writer::tag('li', $this->render($item), $options);
            } else {
                $content = html_writer::tag('li', $separator.$this->render($item), $options);
            }

            if ($item->type == global_navigation::TYPE_COURSE){

                if (isset($currentsection->section) and $currentsection->section > 0){

                    $parentid = 0;
                    if (isset($parentsection->id) and $parentsection->id != $currentsection->id){
                        $item->title = get_section_name($course, $parentsection);
                        $item->text = $item->shorttext = $item->title;
                        $item->type == global_navigation::TYPE_SECTION;
                        $item->action = new moodle_url($page_url, array('id'=>$course->id, 'section'=>$parentsection->section));
                        $content .= html_writer::tag('li', $separator.$this->render($item));
                    }

                    $item->title = get_section_name($course, $currentsection);
                    $item->text = $item->shorttext = $item->title;

                    $item->type == global_navigation::TYPE_SECTION;

                    if ($currentsection->sectiontype == 0 or $currentsection->sectiontype == 8 or $currentsection->sectiontype == 9){
                        $item->action = new moodle_url($page_url, array('id'=>$course->id, 'section'=>$currentsection->section));
                    } else {
                        $item->action = '#';
                    }


                    $options = (isset($cm->section)) ? array() : array('class'=>'active');
                    $content .= html_writer::tag('li', $separator.$this->render($item), $options);
                }
            }

            $htmlblocks[] = $content;
        }

        //accessibility: heading for navbar list  (MDL-20446)
        $navbarcontent = html_writer::tag('span', get_string('pagepath'),
                array('class' => 'accesshide', 'id' => 'navbar-label'));
        $navbarcontent .= html_writer::tag('nav',
                html_writer::tag('ul', join('', $htmlblocks), array('class'=>'breadcrumb')),
                array('aria-labelledby' => 'navbar-label'));
        // XHTML
        return $navbarcontent;
    }

}
