<?php
/**
 * institutes_ti core_course_renderer.
 *
 * @package    theme_institutes_ti
 * @copyright  2017 The Institutes
 */

require_once($CFG->dirroot . "/theme/institutes/classes/core_course_renderer.php");

class theme_institutes_ti_core_course_renderer extends theme_institutes_core_course_renderer{

    public $_sections = array();
    public $_sections_initialized = false;
    public $formatrenderer = null;


    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        $course = $page->course;
        $course = course_get_format($course)->get_course();

        if ($course->format == 'insitutes'){
            global $CFG;
            require_once($CFG->dirroot.'/course/format/'.$course->format.'/lib.php');

            // process course format sections
            $this->_sections = format_institutes_get_course_sections($course);

            // process instututes requirements
            $this->_canviewhiddensections = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id));
            $this->_states = format_institutes_get_course_states($course);
            $this->_requirements = format_institutes_get_course_requirements($course, $this->_states);
        }
    }

    /**
     * Renders HTML to display one course module in a course section
     *
     * This includes link, content, availability, completion info and additional information
     * that module type wants to display (i.e. number of unread forum posts)
     *
     * This function calls:
     * {@link core_course_renderer::course_section_cm_name()}
     * {@link core_course_renderer::course_section_cm_text()}
     * {@link core_course_renderer::course_section_cm_availability()}
     * {@link core_course_renderer::course_section_cm_completion()}
     * {@link course_get_cm_edit_actions()}
     * {@link core_course_renderer::course_section_cm_edit_actions()}
     *
     * @param stdClass $course
     * @param completion_info $completioninfo
     * @param cm_info $mod
     * @param int|null $sectionreturn
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        global $CFG;

        $output = '';
        // We return empty string (because course module will not be displayed at all)
        // if:
        // 1) The activity is not visible to users
        // and
        // 2) The 'availableinfo' is empty, i.e. the activity was
        //     hidden in a way that leaves no info, such as using the
        //     eye icon.
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            return $output;
        }

        if ($completioninfo === null) {
            $completioninfo = new completion_info($course);
        }

        $indentclasses = 'mod-indent';
        if (!empty($mod->indent)) {
            $indentclasses .= ' mod-indent-'.$mod->indent;
            if ($mod->indent > 15) {
                $indentclasses .= ' mod-indent-huge';
            }
        }

        if ($this->page->user_is_editing()) {

            $output .= html_writer::start_tag('div');

            if ($this->page->user_is_editing()) {
                $output .= course_get_cm_move($mod, $sectionreturn);
            }

            $output .= html_writer::start_tag('div', array('class' => 'mod-indent-outer'));

            // This div is used to indent the content.
            $output .= html_writer::div('', $indentclasses);

            // Start a wrapper for the actual content to keep the indentation consistent
            $output .= html_writer::start_tag('div');

            // Display the link to the module (or do nothing if module has no url)
            $cmname = $this->course_section_cm_name($mod, $displayoptions);

            if (!empty($cmname)) {
                // Start the div for the activity title, excluding the edit icons.
                $output .= html_writer::start_tag('div', array('class' => 'activityinstance'));
                $output .= $cmname;

                // Module can put text after the link (e.g. forum unread)
                $output .= $mod->afterlink;

                // Closing the tag which contains everything but edit icons. Content part of the module should not be part of this.
                $output .= html_writer::end_tag('div'); // .activityinstance
            }

            // If there is content but NO link (eg label), then display the
            // content here (BEFORE any icons). In this case cons must be
            // displayed after the content so that it makes more sense visually
            // and for accessibility reasons, e.g. if you have a one-line label
            // it should work similarly (at least in terms of ordering) to an
            // activity.
            $contentpart = $this->course_section_cm_text($mod, $displayoptions);
            $url = $mod->url;
            if (empty($url)) {
                $output .= $contentpart;
            }

            $modicons = '';
            if ($this->page->user_is_editing()) {
                $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                $modicons .= ' '. $this->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                $modicons .= $mod->afterediticons;
            }

            $modicons .= $this->course_section_cm_completion($course, $completioninfo, $mod, $displayoptions);

            if (!empty($modicons)) {
                $output .= html_writer::span($modicons, 'actions');
            }

            // If there is content AND a link, then display the content here
            // (AFTER any icons). Otherwise it was displayed before
            if (!empty($url)) {
                $output .= $contentpart;
            }

            // show availability info (if module is not available)
            $output .= $this->course_section_cm_availability($mod, $displayoptions);

            $output .= html_writer::end_tag('div'); // $indentclasses

            // End of indentation div.
            $output .= html_writer::end_tag('div');

            $output .= html_writer::end_tag('div');

        } else {

            $output .= html_writer::start_tag('div', array('class'=>'activity-box clearfix'));

            $output .= html_writer::start_tag('div', array('class' => 'mod-indent-outer activityname-box'));

            // This div is used to indent the content.
            $output .= html_writer::div('', $indentclasses);

            // Start a wrapper for the actual content to keep the indentation consistent
            $output .= html_writer::start_tag('div');

            // Display the link to the module (or do nothing if module has no url)
            $cmname = $this->course_section_cm_name($mod, $displayoptions);

            if (!empty($cmname)) {
                // Start the div for the activity title, excluding the edit icons.
                $output .= html_writer::start_tag('div', array('class' => 'activityinstance'));
                $output .= $cmname;

                // Module can put text after the link (e.g. forum unread)
                $output .= $mod->afterlink;

                // Closing the tag which contains everything but edit icons. Content part of the module should not be part of this.
                $output .= html_writer::end_tag('div'); // .activityinstance
            }

            // If there is content but NO link (eg label), then display the
            // content here (BEFORE any icons). In this case cons must be
            // displayed after the content so that it makes more sense visually
            // and for accessibility reasons, e.g. if you have a one-line label
            // it should work similarly (at least in terms of ordering) to an
            // activity.
            $contentpart = $this->course_section_cm_text($mod, $displayoptions);

            if (!empty($contentpart)){
                if (empty($url)) {
                    $output .= html_writer::tag('div', $contentpart, array('class'=>'mod-text'));
                } else {
                    $output .= html_writer::tag('div', $contentpart, array('class'=>'mod-description'));
                }
            }

            // show availability info (if module is not available)
            $output .= $this->course_section_cm_availability($mod, $displayoptions);

            $output .= html_writer::end_tag('div'); // $indentclasses

            // End of indentation div.
            $output .= html_writer::end_tag('div');

            $output .= html_writer::start_tag('div', array('class'=>'activity-buttons'));

            if (!$this->_sections_initialized){
                $csections = $this->get_course_sections($course);
            }
            $coursetranscriptflag = (isset($this->_sections[$mod->section]->coursetranscriptflag)) ? $this->_sections[$mod->section]->coursetranscriptflag : 0;

                if ($mod->modname != 'label') {
                    $viewbtn = get_string('view', 'format_institutes').' ' . get_string('pluginname', 'mod_' . $mod->modname);

                    switch ($mod->modname) {
                        case 'quiz':
                            $viewbtn = html_writer::tag('span', get_string('take', 'format_institutes').' ' . get_string('pluginname', 'mod_' . $mod->modname), array('class'=>'take-quiz'));
                            $viewbtn .= html_writer::tag('span', get_string('takeexam', 'format_institutes'), array('class'=>'take-exam'));
                            break;
                        case 'scorm':
                            $viewbtn = get_string('viewmodule', 'format_institutes');
                            break;
                        case 'url':
                            $viewbtn = get_string('viewwebsite', 'format_institutes');
                            break;
                        case 'lti':
                            $viewbtn = get_string('viewlink', 'format_institutes');
                            break;
                        case 'video':
                            $viewbtn = get_string('watch', 'format_institutes');
                            break;
                        case 'resource':
                            if ($coursetranscriptflag){
                                $viewbtn = get_string('viewtranscript', 'format_institutes');
                            }
                            break;
                    }

                    if ($mod->modname == 'resource'){
                        $context = context_module::instance($mod->id);
                        $fs = get_file_storage();
                        $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
                        if (count($files) > 0) {
                            $file = reset($files);
                            unset($files);

                            if (stristr($file->get_mimetype(), 'video')){
                                $viewbtn = get_string('watch', 'format_institutes');
                            }
                        }
                    }
                    if (isset($this->_sections[$mod->section]->sectiontype) and $this->_sections[$mod->section]->sectiontype == 7){
                        $viewbtn = $mod->get_formatted_name();
                    }

                    $link = '';
                    if ($mod->uservisible and $mod->url) {

                        $options = array(
                            'id' => $course->id,
                            'module' => $mod->id,
                            'url' => urlencode( $mod->url ),
                        );

                        $errortext = getModuleAccessibilityError($course->id, $mod->id);

                        if($errortext !== '') {
                            $moduleLink = new moodle_url($CFG->wwwroot . '/local/studylevel/studylevelerror.php', $options);

                            $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);
                            $link = html_writer::link($moduleLink, $viewbtn, array('class' => 'btn modid'.$mod->id, 'onclick' => $onclick));

                            // Create hidden div that will be shown by javascript function
                            $output .= html_writer::start_tag('div', ['class'=>'hidden', 'style'=>'display:none;']);
                            $output .= html_writer::tag('div', $errortext, array('id'=> 'course_study_level_error_popup_'.$mod->id));
                            $output .= html_writer::end_tag('div');

                            $this->page->requires->js_call_amd(
                                            'theme_custom/studylevelerror',
                                            'init',
                                            array(
                                                '.btn.modid' . $mod->id,
                                                'Study Level Verification',
                                                '#course_study_level_error_popup_'.$mod->id,
                                                $mod->id
                                            ));
                        } else {
                            $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);
                            $link = html_writer::link($mod->url, $viewbtn, array('class' => 'btn', 'onclick' => $onclick));
                        }

                    } else {
                        $link = html_writer::link('javascript:void(0)', $viewbtn, array('class' => 'btn btn-disabled'));
                    }
                    $output .= $link;
                }

            $output .= html_writer::end_tag('div');

            $output .= html_writer::start_tag('div', array('class'=>'completion-buttons'));
            $output .= $this->course_section_cm_completion_button($course, $completioninfo, $mod, $displayoptions);
            $output .= html_writer::end_tag('div');

            $output .= html_writer::end_tag('div');
        }
        return $output;
    }

    /**
     * Renders HTML to display a list of course modules in a course section
     * Also displays "move here" controls in Javascript-disabled mode
     *
     * This function calls {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass $course course object
     * @param int|stdClass|section_info $section relative section number or section object
     * @param int $sectionreturn section number to return to
     * @param int $displayoptions
     * @return void
     */
    public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array(), $formatsections = array()) {
        global $USER, $CFG;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // check if we are currently in the process of moving a module with JavaScript disabled
        $ismoving = $this->page->user_is_editing() && ismoving($course->id);
        if ($ismoving) {
            $movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array('class' => 'movetarget'));
            $strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
        }

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = array();
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                if ($mod->modname == 'label' and !$this->page->user_is_editing() and !isset($displayoptions['displaylabel'])) continue;

                if ($mod->modname == 'resource' and !$this->page->user_is_editing()){
                    $context = context_module::instance($mod->id);
                    $fs = get_file_storage();
                    $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
                    if (count($files) > 0) {
                        $file = reset($files);
                        unset($files);

                        if (stristr($file->get_mimetype(), 'audio') and (!isset($displayoptions['displayaudio']) or (isset($displayoptions['displayaudio']) and !$displayoptions['displayaudio']))){
                            continue;
                        }

                        // check if current section is transcript section and show/hide secured trascript file
                        if ($section->coursetranscriptflag){
                            $issecurepdf = (strpos($file->get_filename(), '_s.pdf') !== false);
                            if ($issecurepdf and (isset($this->_requirements['issecurepdf']) and !$this->_requirements['issecurepdf'])){
                                continue;
                            }
                        }
                    }
                }

                if ($ismoving and $mod->id == $USER->activitycopy) {
                    // do not display moving mod
                    continue;
                }

                if ($modulehtml = $this->course_section_cm_list_item($course,
                        $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                    $moduleshtml[$modnumber] = $modulehtml;
                }
            }
        }

        $sectionoutput = '';
        if (!empty($moduleshtml) || $ismoving) {
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                if ($ismoving) {
                    $movingurl = new moodle_url('/course/mod.php', array('moveto' => $modnumber, 'sesskey' => sesskey()));
                    $sectionoutput .= html_writer::tag('li',
                            html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
                            array('class' => 'movehere'));
                }

                $sectionoutput .= $modulehtml;
            }

            if ($ismoving) {
                $movingurl = new moodle_url('/course/mod.php', array('movetosection' => $section->id, 'sesskey' => sesskey()));
                $sectionoutput .= html_writer::tag('li',
                        html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
                        array('class' => 'movehere'));
            }
        }

        // Always output the section module list.
        $output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section img-text'));

        return $output;
    }

    /**
     * Renders HTML to display a list of course modules in a course section
     * Also displays "move here" controls in Javascript-disabled mode
     *
     * This function calls {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass $course course object
     * @param int|stdClass|section_info $section relative section number or section object
     * @param int $sectionreturn section number to return to
     * @param int $displayoptions
     * @return void
     */
    public function course_section_cm_activitieslist($course, $section, $sectionreturn = null, $displayoptions = array(), $formatsections = array()) {
        global $USER, $CFG;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = array();
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                $completionclass = $this->course_section_cm_completion_status($course, $completioninfo, $mod, $displayoptions);
                $extraclasses = $mod->extraclasses.' '.$completionclass;
                $mod->set_extra_classes($extraclasses);

                if ($modulehtml = $this->course_section_cm_list_item($course,
                        $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                    $moduleshtml[$modnumber] = $modulehtml;
                }
            }
        }

        $sectionoutput = '';
        if (!empty($moduleshtml)) {
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                $sectionoutput .= $modulehtml;
            }
        }

        // Always output the section module list.
        $output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section img-text clearfix'));

        return $output;
    }

}
