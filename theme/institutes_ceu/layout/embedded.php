<?php

/**
 * The embedded layout.
 *
 * @package   theme_institutes_ceu
 * @copyright  2017 The Institutes
 */

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <link rel="apple-touch-icon" sizes="48x48" href="<?php echo $CFG->wwwroot; ?>/theme/institutes_ceu/pix/favicon_48x48.png">
    <link rel="icon" type="image/png" href="<?php echo $CFG->wwwroot; ?>/theme/institutes_ceu/pix/favicon_32x32.png" sizes="32x32">
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
<?php echo $OUTPUT->standard_top_of_body_html() ?>
<div id="page">
    <div id="page-content" class="clearfix">
        <?php echo $OUTPUT->main_content(); ?>
    </div>
</div>
<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
