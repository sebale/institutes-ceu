function toggleLeftSidebar(){

    jQuery('.left-sidebar').toggleClass('open');
    jQuery('#page.container-fluid').toggleClass('full-width');

    if($('.left-sidebar.active').length) {
        $('#page-content.row-fluid.active-sidebar').toggleClass('open');
    }
    M.util.set_user_preference('fix-sidebar', jQuery('.left-sidebar').hasClass('open')?1:0);
}



function toggleCourseMenu() {
    if (!jQuery('.left-sidebar').hasClass('open')){
        toggleLeftSidebar();
    }

    jQuery('.main-navigation li.activecourse').toggleClass('active');
}

function instructionsPopupClose(){
    jQuery('.instructions-popup-box').removeClass('open');
}

function instructionsPopupOpen(id){
    instructionsPopupClose();
    jQuery('#instruction_'+id).addClass('open');
}

function resourcePopupClose(){
    jQuery('.instructions-popup-box').removeClass('open');
}

function resourcePopupOpen(id){
    resourcePopupClose();
    jQuery('#resource_'+id).addClass('open');
}

