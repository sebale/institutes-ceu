<?php

/**
 * @package   theme_institutes_ceu
 * @copyright  2017 The Institutes
 */

require_once("$CFG->dirroot/theme/institutes_ceu/lib.php");

defined('MOODLE_INTERNAL') || die;



if ($ADMIN->fulltree) {

    // Logo file setting.
    $name = 'theme_institutes_ceu/logo';
    $title = get_string('logo','theme_institutes_ceu');
    $description = get_string('logodesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Small logo file setting.
    $name = 'theme_institutes_ceu/smalllogo';
    $title = get_string('smalllogo', 'theme_institutes_ceu');
    $description = get_string('smalllogodesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'smalllogo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Show site name along with small logo.
    $name = 'theme_institutes_ceu/sitename';
    $title = get_string('sitename', 'theme_institutes_ceu');
    $description = get_string('sitenamedesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Site name text
    global $SITE;
    $name = 'theme_institutes_ceu/sitenametext';
    $title = get_string('sitenametext', 'theme_institutes_ceu');
    $description = get_string('sitenametextdesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configtext($name, $title, $description, $SITE->shortname);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Show contact button
    $name = 'theme_institutes_ceu/contactusenable';
    $title = get_string('contactusenable', 'theme_institutes_ceu');
    $description = get_string('contactusenabledesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Show contact button
    $name = 'theme_institutes_ceu/contactustitle';
    $title = get_string('contactustitle', 'theme_institutes_ceu');
    $default = get_string('contactus', 'theme_institutes_ceu');
    $description = get_string('contactustitledesc', 'theme_institutes_ceu');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Contact information
    $name = 'theme_institutes_ceu/contacttext';
    $title = get_string('contacttext', 'theme_institutes_ceu');
    $description = get_string('contacttextdesc', 'theme_institutes_ceu');
    $setting = new admin_setting_confightmleditor($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Custom CSS file.
    $name = 'theme_institutes_ceu/customcss';
    $title = get_string('customcss', 'theme_institutes_ceu');
    $description = get_string('customcssdesc', 'theme_institutes_ceu');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_institutes_ceu/footnote';
    $title = get_string('footnote', 'theme_institutes_ceu');
    $description = get_string('footnotedesc', 'theme_institutes_ceu');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Glossary
    $name = 'theme_institutes_ceu/glossary_course_id';
    $title = get_string('glossary_course_title', 'theme_institutes_ceu');
    $description = get_string('glossary_course_description', 'theme_institutes_ceu');
    $default = null;
    $setting = new admin_setting_configselect($name, $title, $description, $default, theme_institutes_ceu_get_courses_list());
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // FAQ
    $name = 'theme_institutes_ceu/faq_course_id';
    $title = get_string('faq_course_title', 'theme_institutes_ceu');
    $description = get_string('faq_course_description', 'theme_institutes_ceu');
    $default = null;
    $setting = new admin_setting_configselect($name, $title, $description, $default, theme_institutes_ceu_get_courses_list());
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
}
