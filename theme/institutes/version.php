<?php

/**
 * @package    theme_institutes
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2017020200;
$plugin->requires  = 2016051900;
$plugin->component = 'theme_institutes';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2016051900,
);
