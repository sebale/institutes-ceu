<?php

/**
 * @package   theme_institutes
 * @copyright  2017 The Institutes
 */

require_once("$CFG->dirroot/theme/institutes/lib.php");

defined('MOODLE_INTERNAL') || die;



if ($ADMIN->fulltree) {

    // Logo file setting.
    $name = 'theme_institutes/logo';
    $title = get_string('logo','theme_institutes');
    $description = get_string('logodesc', 'theme_institutes');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Small logo file setting.
    $name = 'theme_institutes/smalllogo';
    $title = get_string('smalllogo', 'theme_institutes');
    $description = get_string('smalllogodesc', 'theme_institutes');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'smalllogo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Show site name along with small logo.
    $name = 'theme_institutes/sitename';
    $title = get_string('sitename', 'theme_institutes');
    $description = get_string('sitenamedesc', 'theme_institutes');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 1);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Custom CSS file.
    $name = 'theme_institutes/customcss';
    $title = get_string('customcss', 'theme_institutes');
    $description = get_string('customcssdesc', 'theme_institutes');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_institutes/footnote';
    $title = get_string('footnote', 'theme_institutes');
    $description = get_string('footnotedesc', 'theme_institutes');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Glossary
    $name = 'theme_institutes/glossary_course_id';
    $title = get_string('glossary_course_title', 'theme_institutes');
    $description = get_string('glossary_course_description', 'theme_institutes');
    $default = null;
    $setting = new admin_setting_configselect($name, $title, $description, $default, get_courses_list());
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // FAQ
    $name = 'theme_institutes/faq_course_id';
    $title = get_string('faq_course_title', 'theme_institutes');
    $description = get_string('faq_course_description', 'theme_institutes');
    $default = null;
    $setting = new admin_setting_configselect($name, $title, $description, $default, get_courses_list());
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
}
