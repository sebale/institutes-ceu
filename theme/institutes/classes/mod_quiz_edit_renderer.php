<?php
/**
 * Custom renderer for the Quiz's edit page.
 *
 * @package    theme_custom
 * @copyright  2017 The Institutes
 */

class theme_institutes_mod_quiz_edit_renderer extends mod_quiz\output\edit_renderer {

    /**
     * @var bool True if we are rendering the edit page for a dynamic exam
     */
    protected $dynamicexam;

    /**
     * Render the edit page
     *
     * @param \quiz $quizobj object containing all the quiz settings information.
     * @param structure $structure object containing the structure of the quiz.
     * @param \question_edit_contexts $contexts the relevant question bank contexts.
     * @param \moodle_url $pageurl the canonical URL of this page.
     * @param array $pagevars the variables from {@link question_edit_setup()}.
     * @return string HTML to output.
     */
    public function edit_page(\quiz $quizobj, \mod_quiz\structure $structure,
            \question_edit_contexts $contexts, \moodle_url $pageurl, array $pagevars) {
        global $DB;

        // TIMOOD-57 Determine if the quiz is a dynamic exam, save to this class for later use
        $quizid = $quizobj->get_quizid();
        $quizsettings = $DB->get_record('quiz_settings', ['quizid' => $quizid]);
        $this->dynamicexam = isset($quizsettings->enabledynamicexamsizing) ? (bool) $quizsettings->enabledynamicexamsizing : false;

        return parent::edit_page($quizobj, $structure, $contexts, $pageurl, $pagevars);
    }

    public function marked_out_of_field(mod_quiz\structure $structure, $slot) {

        if (!$structure->is_real_question($slot)) {
            $output = html_writer::span('',
                    'instancemaxmark decimalplaces_' . $structure->get_decimal_places_for_question_marks());

            $output .= html_writer::span(
                    $this->pix_icon('spacer', '', 'moodle', array('class' => 'editicon visibleifjs', 'title' => '')),
                    'editing_maxmark');
            return html_writer::span($output, 'instancemaxmarkcontainer infoitem');
        }

        $output = html_writer::span($structure->formatted_question_grade($slot),
                'instancemaxmark decimalplaces_' . $structure->get_decimal_places_for_question_marks(),
                array('title' => get_string('maxmark', 'quiz')));

        // TIMOOD-57 Do not allow the admin to edit the wort of the question if there are attempts and it's a dynamic exam.
        if( !$this->dynamicexam || $structure->can_be_edited() ) {
            $output .= html_writer::span(
                html_writer::link(
                    new \moodle_url('#'),
                    $this->pix_icon('t/editstring', '', 'moodle', array('class' => 'editicon visibleifjs', 'title' => '')),
                    array(
                        'class' => 'editing_maxmark',
                        'data-action' => 'editmaxmark',
                        'title' => get_string('editmaxmark', 'quiz'),
                    )
                )
            );
        }

        return html_writer::span($output, 'instancemaxmarkcontainer');
    }

}
