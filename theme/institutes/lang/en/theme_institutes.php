<?php

/**
 * Strings for component 'theme_institutes', language 'en'
 *
 * @package   theme_institutes
 * @copyright  2017 The Institutes
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Institutes</h2>
<p><img class=img-polaroid src="institutes/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>institutes is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="institutes/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['configtitle'] = 'Institutes';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'The logo is only displayed in the header of the front page and login page.<br /> If the height of your logo is more than 75px add div.logo {height: 100px;} to the Custom CSS box below, amending accordingly if the height is other than 100px.';

$string['pluginname'] = 'Institutes';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['region-side-inner'] = 'Region Side Inner';

$string['sitelogo'] = 'Site logo';
$string['sitename'] = 'Display site name along with small logo';
$string['sitenamedesc'] = 'If there is no small logo, the site name is always displayed in the navigation bar. If a small logo is set, it may be displayed with or without the site name.';
$string['smalllogo'] = 'Small logo';
$string['smalllogodesc'] = 'The small logo is displayed in the navigation bar. If there is a header logo for the front page and login page, the small logo is not displayed on these pages.';


$string['home'] = 'Home';
$string['dashboard'] = 'Dashboard';
$string['activecourse'] = 'Active Course';
$string['courses'] = 'Courses';
$string['resources'] = 'Resources';
$string['gradebook'] = 'Gradebook';
$string['faq'] = 'FAQs';
$string['glossary'] = 'Glossary';
$string['havenotcourses'] = 'You do not have any courses yet';
$string['settings'] = 'Settings';
$string['preferences'] = 'Preferences';
$string['profile'] = 'Profile';
$string['logout'] = 'Log Out';
$string['alerts'] = 'Alerts';
$string['messages'] = 'Messages';
$string['notes'] = 'Notes';
$string['open'] = 'Open';
$string['close'] = 'Close';
$string['courseprogress'] = 'Your Course Progress';
$string['courseprogress_mobile'] = 'Course Progress';
$string['actions'] = 'Actions';
$string['glossary_course_title'] = 'Page Glossary';
$string['glossary_course_description'] = 'Select Glossary for system Glossary';
$string['faq_course_title'] = 'Page FAQ';
$string['faq_course_description'] = 'Select FAQ for system FAQ';
$string['bookmarks'] = 'Bookmarks';
$string['schedule'] = 'Schedule';
$string['editname'] = 'Edit name';
$string['savedbookmarks'] = 'Saved bookmarks';
$string['addbookmarks'] = 'Add bookmark';
$string['name'] = 'Name:';
$string['savebookmark'] = 'Save bookmark';
$string['havenotbookmarks'] = 'You have not saved bookmarks';

// QUIZ
$string['attemptstate'] = 'Status';
$string['reviewattempt'] = 'Review attempt';
$string['attemptdate'] = 'Date/Time';
$string['statefinished'] = 'Complete';
$string['completed'] = 'Complete';
$string['quizreview'] = 'Quiz Review';
$string['quizcompletion'] = 'Quiz Completion';
$string['quizsubmittionreview'] = 'Quiz Submission Review';
$string['attention'] = 'Attention';
$string['quizsummaryattention'] = 'Once you submit, you will no longer be able to change your answers for this quiz.';
$string['returnquiz'] = 'Return to quiz';
$string['submitallandfinish'] = 'Submit Attempt';
$string['endtest'] = 'Finish Attempt';
$string['previous'] = 'Previous';
$string['next'] = 'Next';
$string['flaggedquestions'] = 'FLAGGED QUESTIONS';
$string['questionpoints'] = ' | Points: {$a}';

