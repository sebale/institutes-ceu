<?php

/**
 * This file contains main class for the course format Institutes
 *
 * @package   format_institutes
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot. '/course/format/lib.php');

define('FORMAT_SECTIONS_MODULES', 0);
define('FORMAT_SECTIONS_EXAMS', 1);
define('FORMAT_SECTIONS_GENERAL', 2);
define('FORMAT_SECTIONS_MODULE', 3);
define('FORMAT_SECTIONS_HEADING', 4);
define('FORMAT_SECTIONS_SEGMENT', 5);
define('FORMAT_SECTIONS_RESOURCES', 6);
define('FORMAT_SECTIONS_RESOURCESLINKS', 7);
define('FORMAT_SECTIONS_PAGEACTIVITIES', 8);
define('FORMAT_SECTIONS_ASSIGNMENTS', 9);

/**
 * Main class for the institutes course format
 *
 * @package    format_institutes
 * @copyright  2017 The Institutes
 */
class format_institutes extends format_base {

    public $_renderer = null;

    public function course_content_header() {
        global $CFG;

        require_once($CFG->dirroot. '/course/format/institutes/renderer.php');
        return new format_institutes_course_content_header;
    }

    public function course_content_footer() {
        global $CFG;

        require_once($CFG->dirroot. '/course/format/institutes/renderer.php');
        return new format_institutes_course_content_footer;
    }

    /**
     * Returns true if this course format uses sections
     *
     * @return bool
     */
    public function uses_sections() {
        return true;
    }

    /**
     * Returns the display name of the given section that the course prefers.
     *
     * Use section name is specified by user. Otherwise use default ("Topic #")
     *
     * @param int|stdClass $section Section object from database or just field section.section
     * @return string Display name that the course format prefers, e.g. "Topic 2"
     */
    public function get_section_name($section) {
        $section = $this->get_section($section);
        if ((string)$section->name !== '') {
            return format_string($section->name, true,
                    array('context' => context_course::instance($this->courseid)));
        } else {
            return $this->get_default_section_name($section);
        }
    }

    /**
     * Returns the default section name for the institutes course format.
     *
     * If the section number is 0, it will use the string with key = section0name from the course format's lang file.
     * If the section number is not 0, the base implementation of format_base::get_default_section_name which uses
     * the string with the key = 'sectionname' from the course format's lang file + the section number will be used.
     *
     * @param stdClass $section Section object from database or just field course_sections section
     * @return string The default value for the section name.
     */
    public function get_default_section_name($section) {
        if ($section->section == 0) {
            // Return the general section.
            return get_string('section0name', 'format_institutes');
        } else {
            // Use format_base::get_default_section_name implementation which
            // will display the section name in "Topic n" format.
            return parent::get_default_section_name($section);
        }
    }

    /**
     * The URL to use for the specified course (with section)
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = array()) {
        global $CFG;
        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', array('id' => $course->id));

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section - 1;
        } else {
            $sectionno = $section - 1;
        }
        if ($sectionno !== null) {
            if ($sr !== null) {
                if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
                    $sectionno = $sr;
                } else {
                    $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
                }
            } else {
                $usercoursedisplay = $course->coursedisplay;
            }
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                $url->param('section', $sectionno);
            } else {
                if (empty($CFG->linkcoursesections) && !empty($options['navigation'])) {
                    return null;
                }
                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    /**
     * Allows course format to execute code on moodle_page::set_course()
     *
     * format_flexsections processes additional attributes in the view course URL
     * to manipulate sections and redirect to course view page
     *
     * @param moodle_page $page instance of page calling set_course
     */
    public function page_set_course(moodle_page $page) {
        global $PAGE, $DB;
        if ($PAGE != $page) {
            return;
        }
        if ($this->courseid > 1){
            $context = context_course::instance($this->courseid);
            $coursenavblock = $DB->get_record('block_instances', array('parentcontextid'=>$context->id, 'blockname'=>'sb_coursenav'));
            if (isset($coursenavblock->id) and $coursenavblock->pagetypepattern != '*'){
                $coursenavblock->pagetypepattern = '*';
                $DB->update_record('block_instances', $coursenavblock);
            }
        }
        if ($this->on_course_view_page()) {
            $context = context_course::instance($this->courseid);

            if ($currentsectionnum = $this->get_viewed_section()) {
                navigation_node::override_active_url(new moodle_url('/course/view.php',
                        array('id' => $this->courseid,
                            'sectionid' => $this->get_section($currentsectionnum)->id)));
            }

            // if requested, move section
            $movesection = optional_param('movesection', null, PARAM_INT);
            $moveparent = optional_param('moveparent', null, PARAM_INT);
            $movebefore = optional_param('movebefore', null, PARAM_RAW);

            if ($movesection !== null && $moveparent !== null && has_capability('moodle/course:update', $context)) {
                $newsectionnum = $this->move_section($movesection, $moveparent, $movebefore);
                redirect(course_get_url($this->courseid, $newsectionnum));
            }
        }
    }

    /**
     * Returns the information about the ajax support in the given source format
     *
     * The returned object's property (boolean)capable indicates that
     * the course format supports Moodle course ajax features.
     *
     * @return stdClass
     */
    public function supports_ajax() {
        $ajaxsupport = new stdClass();
        $ajaxsupport->capable = true;
        return $ajaxsupport;
    }

    /**
     * Loads all of the course sections into the navigation
     *
     * @param global_navigation $navigation
     * @param navigation_node $node The course node within the navigation
     */
    public function extend_course_navigation($navigation, navigation_node $node) {
        global $PAGE;
        // if section is specified in course/view.php, make sure it is expanded in navigation
        if ($navigation->includesectionnum === false) {
            $selectedsection = optional_param('section', null, PARAM_INT);
            if ($selectedsection !== null && (!defined('AJAX_SCRIPT') || AJAX_SCRIPT == '0') &&
                    $PAGE->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
                $navigation->includesectionnum = $selectedsection;
            }
        }

        // check if there are callbacks to extend course navigation
        parent::extend_course_navigation($navigation, $node);

        // We want to remove the general section if it is empty.
        $modinfo = get_fast_modinfo($this->get_course());
        $sections = $modinfo->get_sections();
        if (!isset($sections[0])) {
            // The general section is empty to find the navigation node for it we need to get its ID.
            $section = $modinfo->get_section_info(0);
            $generalsection = $node->get($section->id, navigation_node::TYPE_SECTION);
            if ($generalsection) {
                // We found the node - now remove it.
                $generalsection->remove();
            }
        }
    }

    /**
     * Custom action after section has been moved in AJAX mode
     *
     * Used in course/rest.php
     *
     * @return array This will be passed in ajax respose
     */
    function ajax_section_move() {
        global $PAGE;
        $titles = array();
        $course = $this->get_course();
        $modinfo = get_fast_modinfo($course);
        $renderer = $this->get_renderer($PAGE);
        if ($renderer && ($sections = $modinfo->get_section_info_all())) {
            foreach ($sections as $number => $section) {
                $titles[$number] = $renderer->section_title($section, $course);
            }
        }
        return array('sectiontitles' => $titles, 'action' => 'move');
    }

    /**
     * Returns the list of blocks to be automatically added for the newly created course
     *
     * @return array of default blocks, must contain two keys BLOCK_POS_LEFT and BLOCK_POS_RIGHT
     *     each of values is an array of block names (for left and right side columns)
     */
    public function get_default_blocks() {
        return array(
            BLOCK_POS_LEFT => array('sb_coursesettings'),
            BLOCK_POS_RIGHT => array('sb_coursenav')
        );
    }

    public function get_sections_sequense(){
        global $DB;
        $course = $this->get_course();

        $sectionlist = array();
        $allsections = array();

        $course_sections = $DB->get_records('course_sections', array('course'=>$course->id));

        $format_sections = $DB->get_records_menu('course_format_sections', array('courseid'=>$course->id, 'format'=>'institutes'), 'sectionid', 'sectionid,parent');

        if (count($course_sections)){
            foreach ($course_sections as $section){
                if($section->section == 0) continue;
                if(!isset($format_sections[$section->id])) continue;

                $sectionlist[$format_sections[$section->id]][$section->id] = $section;
            }
        }

        return $sectionlist;
    }

    /**
     * Updates format options for a section
     *
     * Section id is expected in $data->id (or $data['id'])
     * If $data does not contain property with the option name, the option will not be updated
     *
     * @param stdClass|array $data return value from {@link moodleform::get_data()} or array with data
     * @return bool whether there were any changes to the options values
     */
    public function update_section_format_options($data) {
        global $DB, $CFG, $PAGE;
        $data = (array)$data;

        require_once($CFG->dirroot.'/course/lib.php');

        $course = $this->get_course();
        $section = $DB->get_record('course_sections', array('id'=>$data['id']));
        $format_renderer = $PAGE->get_renderer('format_institutes');

        $params = new stdClass();
        if ($data['parent'] > 0){
            $parent = $DB->get_record('course_format_sections', array('sectionid' => $data['parent'], 'courseid'=>$course->id, 'format'=>'institutes'));

            $params->level = $parent->level+1;
            $params->parentssequence = $data['parent'];
            if ($parent->parent > 0){
                $params->parentssequence = $parent->parentssequence.','.$data['parent'];
            }
            $params->parent = $data['parent'];
            $params->sectiontype = (isset($data['sectiontype'])) ? $data['sectiontype'] : 0;
            $params->state = (isset($data['state'])) ? $data['state'] : '';
            $params->coursetranscriptflag = (isset($data['coursetranscriptflag'])) ? $data['coursetranscriptflag'] : 0;
        } else {
            $params->parent = 0;
            $params->level = 0;
            $params->parentssequence = '';
            $params->sectiontype = (isset($data['sectiontype'])) ? $data['sectiontype'] : 0;
            $params->state = (isset($data['state'])) ? $data['state'] : '';
            $params->coursetranscriptflag = (isset($data['coursetranscriptflag'])) ? $data['coursetranscriptflag'] : 0;
        }

        $this->course_save_format_section($section, $params);
        $modinfo = get_fast_modinfo($course);
        course_get_format($course)->sort_root_sections($course, $modinfo, $format_renderer);

        rebuild_course_cache($course->id, true);

        return $this->update_format_options($data, $data['id']);
    }

    /**
     * Definitions of the additional options that this course format uses for course
     *
     * institutes format uses the following options:
     * - parent
     * - sectiontype
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function section_format_options($foreditform = false) {
        global $DB, $COURSE;

        $parent = optional_param('parent', 0, PARAM_INT);
        $id = optional_param('id', 0, PARAM_INT);
        $section = null;
        $rootparent = null;

        if ($id > 0){
            $section = $DB->get_record('course_format_sections', array('sectionid'=>$id, 'courseid'=>$COURSE->id, 'format'=>'institutes'));
            if (isset($section->parent)){
                $parent = $section->parent;
            }
            if (isset($section->parentssequence) and !empty($section->parentssequence)){
                $parentssequence = explode(',', $section->parentssequence);
                $rootparentid = reset($parentssequence);
                if ($rootparentid == $parent){
                   $rootparent = $parent;
                } else {
                    $rootparent = $DB->get_record('course_format_sections', array('sectionid'=>$rootparentid, 'courseid'=>$COURSE->id, 'format'=>'institutes'));
                }
            }
        }

        $fields = array();
        $fields['parent'] = array(
                'default' => $parent,
                 'type' => PARAM_INT,
                 'label' => 'Parent',
                 'element_type' => 'hidden',
             );

        $sectiontypes = array(0=> '', 4=>'Heading', 5 => 'Segment', 3 => 'Module', 9 => 'Assignments', 7 => 'Resources Links');

        if ($parent > 0){
            $sectionparent = $DB->get_record('course_format_sections', array('sectionid'=>$parent));
            if ((isset($rootparent->sectiontype) and $rootparent->sectiontype == FORMAT_SECTIONS_GENERAL) or (isset($sectionparent->sectiontype) and $sectionparent->sectiontype == FORMAT_SECTIONS_GENERAL)){
                $fields['sectiontype'] = array(
                     'default' => 0,
                     'type' => PARAM_INT,
                     'label' => 'Section Type',
                     'element_type' => 'hidden',
                 );
            } else {

                if ((isset($rootparent->sectiontype) and $rootparent->sectiontype == FORMAT_SECTIONS_RESOURCES) or (isset($sectionparent->sectiontype) and $sectionparent->sectiontype == FORMAT_SECTIONS_RESOURCES)){
                    $sectiontypes = array(4=>'Heading', 5 => 'Segment', 3 => 'Module', 9=> 'Assignments', 8 =>'Page Activities');
                } elseif (isset($sectionparent->sectiontype) and $sectionparent->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS){
                    $sectiontypes = array(0=> '', 4=>'Heading', 3 => 'Module', 7 => 'Resources Links');
                }

                $fields['sectiontype'] = array(
                     'default' => 0,
                     'type' => PARAM_INT,
                     'label' => 'Section Type',
                     'element_type' => 'select',
                     'element_attributes' => array($sectiontypes),
                 );
            }
        } else {
            $sectiontypes =array(2=>'General', 0 => 'Modules', 1 => 'Exams', 6 => 'Resources');
            $fields['sectiontype'] = array(
                 'default' => 0,
                 'type' => PARAM_INT,
                 'label' => 'Section Type',
                 'element_type' => 'select',
                 'element_attributes' => array($sectiontypes),
             );
        }

        $fields['coursetranscriptflag'] = array(
           'type' => PARAM_INT,
           'label' => 'Course Transcript Section',
           'element_type' => 'checkbox',
           'default' => 0,
         );
        $fields['state'] = array(
           'type' => PARAM_ALPHANUMEXT,
           'label' => 'State',
           'element_type' => 'select',
           'element_attributes' => array(format_institutes_get_state_list()),
         );

        return $fields;
    }

    public function course_format_options($foreditform = false) {
        static $courseformatoptions = false;
        if ($courseformatoptions === false) {
            $courseconfig = get_config('moodlecourse');
            $courseformatoptions = array(
                'numsections' => array(
                    'default' => 2,
                    'type' => PARAM_INT,
                ),
                'hiddensections' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'coursedisplay' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
            );
        }
        if ($foreditform && !isset($courseformatoptions['coursedisplay']['label'])) {
            $courseconfig = get_config('moodlecourse');
            $max = $courseconfig->maxsections;
            if (!isset($max) || !is_numeric($max)) {
                $max = 52;
            }
            $sectionmenu = array();
            for ($i = 0; $i <= $max; $i++) {
                $sectionmenu[$i] = "$i";
            }
            $courseformatoptionsedit = array(
                'numsections' => array(
                    'label' => new lang_string('rootsections', 'format_institutes'),
                    'element_type' => 'hidden',
                ),
                'hiddensections' => array(
                    'label' => new lang_string('hiddensections'),
                    'element_type' => 'hidden',
                ),
                'coursedisplay' => array(
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'hidden',
                ),
            );
            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }
        return $courseformatoptions;
    }

    /**
     * Adds format options elements to the course/section edit form.
     *
     * This function is called from {@link course_edit_form::definition_after_data()}.
     *
     * @param MoodleQuickForm $mform form the elements are added to.
     * @param bool $forsection 'true' if this is a section edit form, 'false' if this is course edit form.
     * @return array array of references to the added form elements.
     */
    public function create_edit_form_elements(&$mform, $forsection = false) {
        global $DB, $CFG;
        $elements = parent::create_edit_form_elements($mform, $forsection);
        $id = optional_param('id', 0, PARAM_INT);

        $course = $DB->get_record('course', array('id'=>$id));

        if (isset($course->id)){
            $course = get_course($id);
            $coursecontext = context_course::instance($course->id);
            $course_format_data = $this->get_course_format_data($course->id);
        } else {
            $course = new stdClass();
            $coursecontext = null;
            $course_format_data = null;
        }

        return $elements;
    }

    /**
     * Updates format options for a course
     *
     * In case if course format was changed to 'institutes', we try to copy options
     * 'coursedisplay', 'numsections' and 'hiddensections' from the previous format.
     * If previous course format did not have 'numsections' option, we populate it with the
     * current number of sections
     *
     * @param stdClass|array $data return value from {@link moodleform::get_data()} or array with data
     * @param stdClass $oldcourse if this function is called from {@link update_course()}
     *     this object contains information about the course before update
     * @return bool whether there were any changes to the options values
     */
    public function update_course_format_options($data, $oldcourse = null) {
        global $DB, $CFG;
        $data = (array)$data;

        if ($oldcourse !== null) {
            $oldcourse = (array)$oldcourse;
            $options = $this->course_format_options();
            foreach ($options as $key => $unused) {
                if (!array_key_exists($key, $data)) {
                    if (array_key_exists($key, $oldcourse)) {
                        $data[$key] = $oldcourse[$key];
                    } else if ($key === 'numsections') {
                        // If previous format does not have the field 'numsections'
                        // and $data['numsections'] is not set,
                        // we fill it with the maximum section number from the DB
                        $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                            WHERE course = ?', array($this->courseid));
                        if ($maxsection) {
                            // If there are no sections, or just default 0-section, 'numsections' will be set to default
                            $data['numsections'] = $maxsection;
                        }
                    }
                }
            }
        }

        $course = get_course($this->courseid);
        $coursecontext = context_course::instance($course->id);
        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$coursecontext, 'subdirs'=>0);

        $changed = $this->update_format_options($data);
        if ($changed && array_key_exists('numsections', $data)) {
            // If the numsections was decreased, try to completely delete the orphaned sections (unless they are not empty).
            $numsections = (int)$data['numsections'];
            $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                        WHERE course = ?', array($this->courseid));
            for ($sectionnum = $maxsection; $sectionnum > $numsections; $sectionnum--) {
                if (!$this->delete_section($sectionnum, false)) {
                    break;
                }
            }
        }

        return $changed;
    }

    /**
     * Whether this format allows to delete sections
     *
     * Do not call this function directly, instead use {@link course_can_delete_section()}
     *
     * @param int|stdClass|section_info $section
     * @return bool
     */
    public function can_delete_section($section) {
        return true;
    }


    /**
     * Prepares the templateable object to display section name
     *
     * @param \section_info|\stdClass $section
     * @param bool $linkifneeded
     * @param bool $editable
     * @param null|lang_string|string $edithint
     * @param null|lang_string|string $editlabel
     * @return \core\output\inplace_editable
     */
    public function inplace_editable_render_section_name($section, $linkifneeded = true,
                                                         $editable = null, $edithint = null, $editlabel = null) {
        if (empty($edithint)) {
            $edithint = new lang_string('editsectionname', 'format_institutes');
        }
        if (empty($editlabel)) {
            $title = get_section_name($section->course, $section);
            $editlabel = new lang_string('newsectionname', 'format_institutes', $title);
        }
        return parent::inplace_editable_render_section_name($section, $linkifneeded, $editable, $edithint, $editlabel);
    }

    public function get_course_format_data($courseid) {
        global $DB;
        $data = array();
        $course_data = $DB->get_records('course_format_options', array('courseid'=>$courseid, 'format'=>'institutes'));
        if (count($course_data)){
            foreach ($course_data as $item){
                $data[$item->name] = $item->value;
            }
        }

        return $data;
    }

    /**
     * Deletes a section
     *
     * Do not call this function directly, instead call {@link course_delete_section()}
     *
     * @param int|stdClass|section_info $section
     * @param bool $forcedeleteifnotempty if set to false section will not be deleted if it has modules in it.
     * @return bool whether section was deleted
     */
    public function delete_section($section, $forcedeleteifnotempty = false) {
        global $DB;
        if (!$this->uses_sections()) {
            // Not possible to delete section if sections are not used.
            return false;
        }
        if (!is_object($section)) {
            $section = $DB->get_record('course_sections', array('course' => $this->get_courseid(), 'section' => $section),
                'id,section,sequence,summary');
        }
        if (!$section || !$section->section) {
            // Not possible to delete 0-section.
            return false;
        }

        if (!$forcedeleteifnotempty && (!empty($section->sequence) || !empty($section->summary))) {
            return false;
        }

        $course = $this->get_course();

        $this->delete_child_sections($course, $section);

        // Remove the marker if it points to this section.
        if ($section->section == $course->marker) {
            course_set_marker($course->id, 0);
        }

        $lastsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                            WHERE course = ?', array($course->id));

        // Find out if we need to descrease the 'numsections' property later.
        $courseformathasnumsections = array_key_exists('numsections',
            $this->get_format_options());
        $decreasenumsections = $courseformathasnumsections && ($section->section <= $course->numsections);

        // Move the section to the end.
        move_section_to($course, $section->section, $lastsection, true);

        // Delete all modules from the section.
        foreach (preg_split('/,/', $section->sequence, -1, PREG_SPLIT_NO_EMPTY) as $cmid) {
            course_delete_module($cmid);
        }

        // Delete section and it's format options.
        $DB->delete_records('course_format_sections', array('sectionid' => $section->id, 'courseid'=>$course->id, 'format'=>'institutes'));
        $DB->delete_records('course_format_options', array('sectionid' => $section->id, 'courseid'=>$course->id, 'format'=>'institutes'));
        $DB->delete_records('course_sections', array('id' => $section->id));

        $course = $this->get_course();
        $this->update_format_options(array('id' => $course->id, 'numsections' => $course->numsections - 1));
        $this->sort_root_sections($course);

        return true;
    }

    public function delete_child_sections($course, $section) {
        global $DB, $CFG, $PAGE;
        require_once($CFG->dirroot.'/course/lib.php');

        $format_renderer = $PAGE->get_renderer('format_institutes');
        $modinfo = get_fast_modinfo($course);
        $sections_to_delete = array();

        $sections_to_delete = $format_renderer->get_all_section_childs($course, $modinfo, $section->section, $sections_to_delete, true);

        $numsections = $course->numsections;
        if (count($sections_to_delete)){
            foreach ($sections_to_delete as $thissection){
                // Remove the marker if it points to this section.
                if ($thissection->section == $course->marker) {
                    course_set_marker($course->id, 0);
                }

                $currentsection = $DB->get_record('course_sections', array('id'=>$thissection->id));

                $lastsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                                    WHERE course = ?', array($course->id));

                if (!$currentsection){
                    return false;
                }

                // Move the section to the end.
                move_section_to($course, $currentsection->section, $lastsection, true);

                // Delete all modules from the section.
                foreach (preg_split('/,/', $currentsection->sequence, -1, PREG_SPLIT_NO_EMPTY) as $cmid) {
                    course_delete_module($cmid);
                }

                // Delete section and it's format options.
                $DB->delete_records('course_format_sections', array('sectionid' => $currentsection->id, 'courseid'=>$course->id, 'format'=>'institutes'));
                $DB->delete_records('course_format_options', array('sectionid' => $currentsection->id, 'courseid'=>$course->id, 'format'=>'institutes'));
                $DB->delete_records('course_sections', array('id' => $thissection->id));
                $numsections = $numsections-1;
                $this->update_format_options(array('id' => $course->id, 'numsections' => $numsections));
                rebuild_course_cache($course->id, true);
            }
        }

        return true;
    }

    function process_sections_sequese(&$sectionlist, $course, $modinfo = null, $format_renderer = null, $sections_sequense = null, $sectionid, &$i = 0){

        if (count($sections_sequense[$sectionid]['childs'])){
            foreach($sections_sequense[$sectionid]['childs'] as $section){
                $sectionlist[$section->id] = ++$i;

                if (isset($sections_sequense[$section->id]['childs'])){
                    $this->process_sections_sequese($sectionlist, $course, $modinfo, $format_renderer, $sections_sequense, $section->id, $i);
                }
            }
        }
    }

    function sort_root_sections($course, $modinfo = null, $format_renderer = null){
        global $DB, $CFG, $PAGE;
        require_once($CFG->dirroot.'/course/lib.php');

        if (!$format_renderer){
            $format_renderer = $PAGE->get_renderer('format_institutes');
        }
        if (!$modinfo){
            $modinfo = get_fast_modinfo($course);
        }

        $sections_sequense = $format_renderer->get_sections_sequense($course, $modinfo, 0);
        $sectionlist = array();

        // ROOT LEVEL
        if (isset($sections_sequense[0]['childs'])){
           $this->process_sections_sequese($sectionlist, $course, $modinfo, $format_renderer, $sections_sequense, 0);
        }

        if (count($sectionlist)){
            $transaction = $DB->start_delegated_transaction();
            foreach ($sectionlist as $id => $position) {
                $DB->set_field('course_sections', 'section', -$position, array('id' => $id));
            }
            foreach ($sectionlist as $id => $position) {
                $DB->set_field('course_sections', 'section', $position, array('id' => $id));
                $DB->set_field('course_format_sections', 'section', $position, array('sectionid' => $id, 'courseid'=>$course->id, 'format'=>'institutes'));
            }
            $transaction->allow_commit();
        }

        rebuild_course_cache($course->id, true);
    }

    function course_create_format_sections_if_missing(){
        global $DB;
        $course = $this->get_course();

        $course_sections = $DB->get_records('course_sections', array('course'=>$course->id));
        $format_sections = $DB->get_records_menu('course_format_sections', array('courseid'=>$course->id, 'format'=>'institutes'), 'sectionid', 'sectionid,id');

        if (count($course_sections)){
            foreach ($course_sections as $sections){
                if (!isset($format_sections[$sections->id])){
                    $new_format_section = new stdClass();
                    $new_format_section->courseid = $course->id;
                    $new_format_section->format = 'institutes';
                    $new_format_section->sectionid = $sections->id;
                    $new_format_section->section = $sections->section;
                    $new_format_section->parent = 0;
                    $new_format_section->level = 0;
                    $new_format_section->timecreated = time();
                    $new_format_section->timemodified = time();
                    $new_format_section->sectiontype = 0;

                    $DB->insert_record('course_format_sections', $new_format_section);
                }
            }
        }
    }

    function course_save_format_section($section, $params = array()){
        global $DB;
        $course = $this->get_course();
        $format_section = $DB->get_record('course_format_sections', array('courseid'=>$course->id, 'format'=>'institutes', 'sectionid'=>$section->id));

        if ($format_section){
            $format_section->section = $section->section;
            $format_section->parent = $params->parent;
            $format_section->level = $params->level;
            $format_section->parentssequence = $params->parentssequence;
            $format_section->timemodified = time();
            $format_section->sectiontype = (isset($params->sectiontype)) ? $params->sectiontype : 0;

            $DB->update_record('course_format_sections', $format_section);
        } else {
            $format_section = new stdClass();
            $format_section->courseid = $course->id;
            $format_section->format = 'institutes';
            $format_section->sectionid = $section->id;
            $format_section->section = $section->section;
            $format_section->parent = $params->parent;
            $format_section->level = $params->level;
            $format_section->parentssequence = $params->parentssequence;
            $format_section->timecreated = time();
            $format_section->timemodified = time();
            $format_section->sectiontype = (isset($params->sectiontype)) ? $params->sectiontype : 0;

            $DB->insert_record('course_format_sections', $format_section);
        }
    }

    function course_create_format_section($index = 0, $data = array(), $parent = null){
        global $DB, $CFG;

        require_once($CFG->dirroot.'/course/lib.php');

        $course = $this->get_course();

        update_course((object)array('id' => $course->id, 'numsections' => $index));

        $section = new stdClass();
        $section->course = $course->id;
        $section->section = $index;
        $section->name = (!empty($data->name)) ? $data->name : get_string('sectionname', 'format_institutes').' '.$index;
        $section->visible = 1;
        $section->summary = '';
        $section->summaryformat = 1;
        $section->sequence = '';
        $section->id = $DB->insert_record('course_sections', $section);

        $params = new stdClass();
        $params->parent = (isset($parent->sectionid)) ? $parent->sectionid : 0;
        $params->sectionid = $section->id;
        $params->section = $index;
        $params->timemodified = time();
        if (isset($parent->sectionid)){
            $params->level = $parent->level+1;
            $params->parentssequence = $parent->sectionid;
            if (isset($parent->parentssequence) and !empty($parent->parentssequence)){
                $params->parentssequence = $parent->parentssequence.','.$parent->sectionid;
            }
            $params->sectiontype = (isset($data->sectiontype)) ? $data->sectiontype : 0;
            $params->coursetranscriptflag = (isset($data->coursetranscriptflag)) ? $data->coursetranscriptflag : 0;
            $params->state = (isset($data->state)) ? $data->state : '';
        } else {
            $params->parentssequence = '';
            $params->level = 0;
            $params->sectiontype = 0;
            $params->coursetranscriptflag = 0;
            $params->state = '';
        }

        $format_options = array();
        $format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$section->id, 'name'=>'parent', 'value'=>$params->parent);
        $format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$section->id, 'name'=>'sectiontype', 'value'=>$params->sectiontype);
        $format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$section->id, 'name'=>'coursetranscriptflag', 'value'=>$params->coursetranscriptflag);
        $format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$section->id, 'name'=>'state', 'value'=>$params->state);

        foreach ($format_options as $foption){
            $DB->insert_record('course_format_options', $foption);
        }

        $this->course_save_format_section($section, $params);

        return $params;
    }



    /**
     * Returns true if we are on /course/view.php page
     *
     * @return bool
     */
    public function on_course_view_page() {
        global $PAGE;
        return ($PAGE->has_set_url() &&
                $PAGE->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)
                );
    }

    /**
     * If we are on course/view.php page return the 'section' attribute from query
     *
     * @return int
     */
    public function get_viewed_section() {
        global $PAGE;
        if ($this->on_course_view_page()) {
            return $PAGE->url->get_param('section');
        }
        return 0;
    }

    /**
     * If in section moving mode returns section number, otherwise returns null
     *
     * @return null|int
     */
    public function is_moving_section() {
        global $PAGE;
        if ($this->on_course_view_page() && $PAGE->user_is_editing()) {
            return optional_param('moving', null, PARAM_INT);
        }
        return null;
    }

    /**
     * Returns control 'Move here' for particular parent section
     *
     * @param int|section_info $parent
     * @param int|section_info $before
     * @return array of controls (0 or 1 element)
     */
    public function get_edit_control_movehere($parent, $before, $courseformat) {
        global $OUTPUT;
        $movingsection = $this->is_moving_section();
        if (!$movingsection || !$this->can_move_section_to($movingsection, $parent, $before, $courseformat)) {
            return null;
        }

        $parentnum = $this->get_section_number($parent);

        $movelink = course_get_url($this->courseid);
        $movelink->params(array('movesection' => $movingsection, 'moveparent' => $parentnum));

        if ($before) {
            $beforenum = $this->get_section_number($before);
            $movelink->params(array('movebefore' => $beforenum));
        }

        // Increase number of sections.
        $strmovesection = strip_tags(get_string('movefull', '', "'".$this->get_section_name($movingsection)."'"));
        $icon = $OUTPUT->pix_icon('movehere', $strmovesection);
        $control = html_writer::link($movelink, $icon, array('class' => 'movehere-sections'));

        return $control;
    }

    /**
     * Returns the section relative number regardless whether argument is an object or an int
     *
     * @param int|section_info $section
     * @return int
     */
    protected function get_section_number($section) {
        if ($section === null || $section === '') {
            return null;
        } else if (is_object($section)) {
            return $section->section;
        } else {
            return (int)$section;
        }
    }

    /**
     * Check if we can move the section to this position
     *
     * not allow to insert section as it's own subsection
     * not allow to insert section directly before or after itself (it would not change anything)
     *
     * @param int|section_info $section
     * @param int|section_info $parent
     * @param null|section_info|int $before null if in the end of subsections list
     */
    public function can_move_section_to($section, $parent, $before = null, $courseformat) {

        if (!has_capability('moodle/course:update', context_course::instance($this->courseid))){
            return false;
        }

        $section = $this->get_section($section);
        $parent = $this->get_section($parent);
        if ($section === null || $parent === null ||
                !has_capability('moodle/course:update', context_course::instance($this->courseid))) {
            return false;
        }

        if ($section){
            $section = $courseformat->process_section_data($section);
        }
        if ($parent){
            $parent = $courseformat->process_section_data($parent);
        }

        // check that $parent is not subsection of $section
        if ($section->section == $parent->section || $this->section_has_parent($parent, $section->id, $courseformat)) {
            return false;
        }

        if ($before) {
            if (is_string($before)) {
                $before = (int)$before;
            }
            $before = $this->get_section($before);
            if ($before){
                $before = $courseformat->process_section_data($before);
                if ($before->section == $section->section){
                    return false;
                } elseif (!$before->parent and !$section->parent and $before->section-1 == $section->section){
                    return false;
                }
            }
            // check that it's a subsection of $parent
            if (isset($parent->section) and $parent->section > 0 and (!$before || $before->parent !== $parent->id)){
                return false;
            }
        }

        if ($section->parent == $parent->id) {
            // section's parent is not being changed
            // do not insert section directly before or after itself
            if ($before && $before->section == $section->section) {
                return false;
            }
            $subsections = array();
            $lastsibling = null;
            foreach ($courseformat->_sections as $sibling) {
                if ($sibling->parent == $parent->id) {
                    if ($before && $before->section == $sibling->section) {
                        if ($lastsibling && $lastsibling->section == $section->section) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                    $lastsibling = $sibling;
                }
            }
            if ($lastsibling && !$before && $lastsibling->section == $section->section) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if given section has another section among it's parents
     *
     * @param int|section_info $section child section
     * @param int $parentid parent section id
     * @return boolean
     */
    protected function section_has_parent($section, $parentid, $courseformat) {
        if (!$section) {
            return false;
        }
        $section = $this->get_section($section);
        if (!$section->section) {
            return false;
        }

        $section = $courseformat->process_section_data($section);
        if ($section->parent == $parentid) {
            return true;
        } else if ($section->parent == 0) {
            return false;
        } else if ($section->parent >= $section->section) {
            // some error
            return false;
        } else {
            return $this->section_has_parent($section->parent, $parentid);
        }
    }


    /**
     * Moves section to the specified position
     *
     * @param int|section_info $section
     * @param int|section_info $parent
     * @param null|int|section_info $before
     * @return int new section number
     */
    protected function move_section($section, $parent, $before = null) {
        global $DB, $PAGE;

        $course = $this->get_course();
        $modinfo = get_fast_modinfo($course);
        $format_renderer = $PAGE->get_renderer('format_institutes');
        $this->_renderer = $format_renderer;

        $section = $this->get_section($section);
        if (isset($section->id)){
            $section = $this->_renderer->process_section_data($section);
        }

        $parent = $this->get_section($parent);
        if (isset($parent->id)){
            $parent = $this->_renderer->process_section_data($parent);
        }

        $newsectionnumber = $section->section;

        if (!$this->can_move_section_to($section->section, $parent->section, $before, $this->_renderer)) {
            die($newsectionnumber);
            return $newsectionnumber;
        }

        if ($section->visible != $parent->visible && $section->parent != $parent->id) {
            // section is changing parent and new parent has different visibility than the section
            if ($section->visible) {
                // visible section is moved under hidden parent
                $updatesectionvisible = 0;
                $updatesectionvisibleold = 1;
            } else {
                // hidden section is moved under visible parent
                if ($section->visibleold) {
                    $updatesectionvisible = 1;
                    $updatesectionvisibleold = 1;
                }
            }
        }

        // find the changes in the sections numbering
        $origorder = array();
        foreach ($this->get_sections() as $subsection) {
            $origorder[$subsection->id] = $subsection->section;
        }
        $neworder = array();

        foreach ($this->get_sections() as $num => $rootsection) {
            $rootsection = $this->_renderer->process_section_data($rootsection);
            if (!$rootsection->parent){
                $this->reorder_sections($neworder, $rootsection->section, $section->section, $parent->section, $before);
            }
        }

        if (count($origorder) != count($neworder)) {
            die('Error in sections hierarchy'); // TODO
        }

        $changes = array();
        foreach ($origorder as $id => $num) {
            if ($num == $section->section) {
                $newsectionnumber = $neworder[$id];
            }
            if ($num != $neworder[$id]) {
                $changes[$id] = array('old' => $num, 'new' => $neworder[$id]);
                if ($num && $this->get_course()->marker == $num) {
                    $changemarker = $neworder[$id];
                }
            }
        }

        if (empty($changes) and $parent->id == $section->parent) {
            return $newsectionnumber;
        }

        // Update all in database in one transaction
        $transaction = $DB->start_delegated_transaction();
        // Update sections numbers in 2 steps to avoid breaking database uniqueness constraint
        foreach ($changes as $id => $change) {
            $DB->set_field('course_sections', 'section', -$change['new'], array('id' => $id));
        }
        foreach ($changes as $id => $change) {
            $DB->set_field('course_sections', 'section', $change['new'], array('id' => $id));
        }
        // change parents of their subsections
        $currentsection = $DB->get_record('course_format_options', array('courseid'=>$course->id, 'sectionid'=>$section->id, 'name'=>'parent'));
        if (isset($currentsection)){
            $currentsection->value = (isset($parent->id) and $parent->section > 0) ? $parent->id : 0;
            $DB->update_record('course_format_options', $currentsection);
        }

        $this->update_corse_sections($section, $newsectionnumber, $parent, $changes);

        $transaction->allow_commit();
        rebuild_course_cache($this->courseid, true);
        if (isset($changemarker)) {
            course_set_marker($this->courseid, $changemarker);
        }
        if (isset($updatesectionvisible)) {
            $this->set_section_visible($newsectionnumber, $updatesectionvisible, $updatesectionvisibleold);
        }

        return $newsectionnumber;
    }

    public function update_corse_sections($section, $newsectionnumber = 0, $parent, $changes){
        global $DB;

        $parentid = (isset($parent->id) and $parent->section > 0) ? $parent->id : 0;
        $level = (isset($parent->level) and $parent->section > 0) ? $parent->level+1 : 0;
        $parentssequence = (isset($parent->id) and $parent->section > 0) ? ((!empty($parent->parentssequence)) ? $parent->parentssequence.','.$parent->id : $parent->id) : '';

        $DB->set_field('course_format_sections', 'section', $newsectionnumber, array('sectionid' => $section->id,  'format'=>'institutes'));
        $DB->set_field('course_format_sections', 'parent', $parentid, array('sectionid' => $section->id,  'format'=>'institutes'));
        $DB->set_field('course_format_sections', 'level', $level, array('sectionid' => $section->id,  'format'=>'institutes'));
        $DB->set_field('course_format_sections', 'parentssequence', $parentssequence, array('sectionid' => $section->id,  'format'=>'institutes'));

        $children = $this->get_subsections($section->section);
        if (count($children)){
            $parentsection = $section;
            $parentsection->section = (isset($changes[$section->id]['new'])) ? $changes[$section->id]['new'] : $newsectionnumber;
            $parentsection->parent = $parentid;
            $parentsection->level = $level;
            $parentsection->parentssequence = $parentssequence;

            foreach ($children as $subsection) {
                $subsection = $this->_renderer->_sections[$subsection->id];
                $newsectionid = $parentsection->section + 1;
                $this->update_corse_sections($subsection, $newsectionid, $parentsection, $changes);
            }
        }

    }


    /**
     * Function recursively reorders the sections while moving one section to the new position
     *
     * If $movedsectionnum is not specified, function just populates the array for each (sub)section
     * If $movedsectionnum is specified, we ignore it on the present location but add it
     * under $movetoparentnum before $movebeforenum
     *
     * @param array $neworder the result or re-ordering, array (sectionid => sectionnumber)
     * @param int|section_info $cursection
     * @param int|section_info $movedsectionnum
     * @param int|section_info $movetoparentnum
     * @param int|section_info $movebeforenum
     */
    protected function reorder_sections(&$neworder, $cursection, $movedsectionnum = null, $movetoparentnum = null, $movebeforenum = null, $rootbeforenum = null) {
        // normalise arguments
        $cursection = $this->get_section($cursection);
        $movetoparentnum = $this->get_section_number($movetoparentnum);
        $movebeforenum = $this->get_section_number($movebeforenum);
        $movedsectionnum = $this->get_section_number($movedsectionnum);
        if ($movedsectionnum === null) {
            $movebeforenum = $movetoparentnum = null;
        }
        $movedsection = $this->get_section($movedsectionnum);

        if ($movebeforenum && !$movetoparentnum && $cursection->section == $movebeforenum ) {
            //$neworder[$movedsection->id] = $movebeforenum-1;
            $this->reorder_sections($neworder, $movedsectionnum, null, null, null, $movebeforenum);
        }

        // ignore section being moved
        if ($movedsectionnum !== null && $movedsectionnum == $cursection->section) {
            return;
        }

        // add current section to $neworder
        if ($rootbeforenum > 0) {
            $neworder[$cursection->id] = ($rootbeforenum > $cursection->section) ? count($neworder) : $rootbeforenum;
        } else {
            $neworder[$cursection->id] = count($neworder);
        }

        // loop through subsections and reorder them (insert $movedsectionnum if necessary)
        foreach ($this->get_subsections($cursection->section) as $subsection) {
            if ($movebeforenum && $subsection->section == $movebeforenum) {
                $this->reorder_sections($neworder, $movedsectionnum);
            }
            if ($movetoparentnum !== 0) {
                $this->reorder_sections($neworder, $subsection, $movedsectionnum, $movetoparentnum, $movebeforenum);
            } else {
                $this->reorder_sections($neworder, $subsection, $movedsectionnum, $movetoparentnum, $movebeforenum);
            }
        }
        if (!$movebeforenum && $movetoparentnum !== 0 && $movetoparentnum == $cursection->section) {
            $this->reorder_sections($neworder, $movedsectionnum);
        }

        // move to the end
        if (!$movebeforenum && $movetoparentnum === 0) {
            $childs = array();
            $childs = $this->_renderer->get_all_section_childs($this->get_course(), get_fast_modinfo($this->get_course()), $movedsectionnum, $childs, true);
            $countchilds = (count($childs)) ? count($childs) : 0;

            if ($this->get_course()->numsections == count($neworder) + $countchilds){
                $this->reorder_sections($neworder, $movedsectionnum);
            }
        }
    }

    /**
     * Returns the list of direct subsections of the specified section
     *
     * @param int|section_info $section
     * @return array
     */
    public function get_subsections($section) {
        $section = $this->get_section($section);
        $subsections = array();
        foreach ($this->get_sections() as $num => $subsection) {
            $subsection = $this->_renderer->process_section_data($subsection);
            if ($subsection->parent == $section->id && $num != $section->section) {
                $subsections[$num] = $subsection;
            }
        }
        return $subsections;
    }

}


/**
 * Implements callback inplace_editable() allowing to edit values in-place
 *
 * @param string $itemtype
 * @param int $itemid
 * @param mixed $newvalue
 * @return \core\output\inplace_editable
 */
function format_institutes_inplace_editable($itemtype, $itemid, $newvalue) {
    global $DB, $CFG;
    require_once($CFG->dirroot . '/course/lib.php');
    if ($itemtype === 'sectionname' || $itemtype === 'sectionnamenl') {
        $section = $DB->get_record_sql(
            'SELECT s.* FROM {course_sections} s JOIN {course} c ON s.course = c.id WHERE s.id = ? AND c.format = ?',
            array($itemid, 'institutes'), MUST_EXIST);
        return course_get_format($section->course)->inplace_editable_update_section_name($section, $itemtype, $newvalue);
    }
}

function format_institutes_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_COURSE) {
        return false;
    }
    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'format_institutes', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

function format_institutes_get_modules_list($courseid = 0, $modname = '') {
    global $DB;

    if (empty($modname)) return array();

    $list = array('' => get_string('selectoption', 'format_institutes', get_string('pluginname', 'mod_'.$modname)));

    if($records = $DB->get_records_sql("SELECT cm.id, i.name FROM {course_modules} cm LEFT JOIN {".$modname."} as i ON cm.instance = i.id LEFT JOIN {modules} m ON m.id = cm.module LEFT JOIN {course_sections} cs ON cs.id = cm.section WHERE cm.course = :courseid AND m.name = :modname AND cs.section = 0", array('courseid'=>$courseid, 'modname'=>$modname))) {
        foreach ($records as $record) {
            $list[$record->id] = $record->name;
        }
    }

    return $list;
}

function format_institutes_get_course_hiddenresources($course) {
    global $PAGE, $DB;

    $resources = $DB->get_records_sql_menu("SELECT cmid, coursestate FROM {course_format_resources} WHERE courseid = :courseid AND type = :type AND coursestate = 0", array('courseid'=>$course->id, 'type'=>'module'));

    return $resources;
}

function format_institutes_get_course_cmresources($course){
    global $PAGE, $DB;
    $resources = array();

    $allresources = $DB->get_records('resource', array('course'=>$course->id));
    if (count($allresources)){
        foreach ($allresources as $resource){
            $resources[$resource->id] = $resource;
        }
    }
    return $resources;
}

function format_institutes_noteseditor_options($courseid = 0) {
    global $CFG;

    $options = array(
        'maxfiles' => EDITOR_UNLIMITED_FILES,
        'maxbytes'=>$CFG->maxbytes,
        'trusttext'=>false,
        'noclean'=>true,
        'subdirs' => 0,
        'accepted_types' => '*'
    );
    if (!empty($courseid)) {
        $options['context'] = context_course::instance($courseid);
    }
    return $options;
}

function format_institutes_get_course_notifications($course) {
    global $DB;

    $notifications = $DB->get_records_sql("SELECT n.*   FROM {course_format_notes} n
                                                        WHERE n.courseid = :courseid
                                                            AND n.status > 0
                                                            AND n.timestart < :timestart
                                                            AND (n.timeend = 0 OR n.timeend > :timeend)
                                                        ORDER BY n.sortorder ASC",
                                         array('courseid'=>$course->id, 'timestart'=>time(), 'timeend'=>time()));

    return $notifications;
}

function format_institutes_get_state_list() {
    global $DB;
    $staterecords = $DB->get_records_menu('state_ce_req', null, '', 'distinct state, full_state');
    $statearr = array(''=>'') + $staterecords;
    return $statearr;
}

function format_institutes_get_course_states($course){
    global $USER, $CFG;
    require_once("$CFG->dirroot/local/tiutilities/locallib.php");

    $user_enrolment_data = get_user_enrolment_data($course->id, $USER->id);
    $states = array();

    if (!empty($user_enrolment_data->states)) {
        $states = explode(',', $user_enrolment_data->states);
    }

    return $states;
}


function format_institutes_get_course_requirements($course, $states = array()){
    global $USER, $CFG;

    require_once("$CFG->dirroot/admin/tool/statecerequirements/statereqslib.php");
    $requirements = get_state_requirements($states, $course->id);

    $requirements['transcriptshown'] = (isset($requirements['transcriptshown'])) ? $requirements['transcriptshown'] : 0;
    $requirements['issecurepdf'] = (isset($requirements['issecurepdf'])) ? $requirements['issecurepdf'] : 0;

    return $requirements;
}

function format_institutes_get_course_sections($course) {
    global $PAGE, $DB;

    $sections = array();
    $allsections = $DB->get_records_sql(
        "SELECT s.*, fs.parent, fs.level, fs.parentssequence, fs.sectiontype, cft.value as coursetranscriptflag, cfs.value as state
                 FROM {course_sections} s
            LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course AND fs.format = :format1
            LEFT JOIN {course_format_options} cft ON cft.courseid = s.course AND cft.sectionid = s.id AND cft.format = :format2 AND cft.name = :coursetranscriptflag
            LEFT JOIN {course_format_options} cfs ON cfs.courseid = s.course AND cfs.sectionid = s.id AND cfs.format = :format3 AND cfs.name = :state
                WHERE s.course = :course
                ORDER BY s.section",
        array('format1'=>'institutes', 'format2'=>'institutes', 'format3'=>'institutes', 'course'=>$course->id, 'coursetranscriptflag'=>'coursetranscriptflag', 'state'=>'state'));

    if (count($allsections)){
        foreach($allsections as $section){
            $sections[$section->id] = $section;
        }
    }

    return $sections;
}

function format_institutes_get_sections_parent($course, $section, $type = 0, $root = false) {
    global $PAGE, $DB;

    if (!empty($section->parentssequence)){

        $order = ($root) ? 'ASC' : 'DESC';

        $sections = $DB->get_records_sql(
            "SELECT s.*, fs.parent, fs.level, fs.parentssequence, fs.sectiontype, cft.value as coursetranscriptflag, cfs.value as state
                     FROM {course_format_sections} fs
                LEFT JOIN {course_sections} s ON fs.sectionid = s.id AND fs.courseid = s.course AND fs.format = :format1
                LEFT JOIN {course_format_options} cft ON cft.courseid = s.course AND cft.sectionid = s.id AND cft.format = :format2 AND cft.name = :coursetranscriptflag
                LEFT JOIN {course_format_options} cfs ON cfs.courseid = s.course AND cfs.sectionid = s.id AND cfs.format = :format3 AND cfs.name = :state
                    WHERE s.id IN ($section->parentssequence) AND s.course = :course
                 ORDER BY section $order", array('format1'=>'institutes', 'format2'=>'institutes', 'format3'=>'institutes', 'course'=>$course->id, 'coursetranscriptflag'=>'coursetranscriptflag', 'state'=>'state'));

        if (count($sections)){
            foreach ($sections as $section){
                if ($type == 0){
                    break;
                } elseif ($type == $section->sectiontype){
                    break;
                }
            }
        }

    }

    return $section;
}

function format_institutes_process_copy_section($course, $sections_sequense, $format_renderer, $currentsection, $parent, $index) {
    if (isset($sections_sequense[$currentsection->id]['childs']) and count($sections_sequense[$currentsection->id]['childs'])){
        foreach($sections_sequense[$currentsection->id]['childs'] as $section){

            $index += 1;
            $section = $format_renderer->process_section_data($section);
            $newparent = course_get_format($course)->course_create_format_section($index, $section, $parent);

            $index = format_institutes_process_copy_section($course, $sections_sequense, $format_renderer, $section, $newparent, $index);
        }
    }

    return $index;
}




