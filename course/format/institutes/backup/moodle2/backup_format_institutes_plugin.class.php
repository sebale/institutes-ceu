<?php

/**
 * Defines backup_format_institutes_plugin class
 *
 * @package     format_institutes
 * @category    backup
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Provides the steps to perform one complete backup of the format instance
 */
class backup_format_institutes_plugin extends backup_format_plugin {

    protected function define_course_plugin_structure() {

        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, '/course/format', 'institutes');

        // Create plugin container element with standard name
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // Add wrapper to plugin
        $plugin->add_child($pluginwrapper);

        // course notes //
        $notes = new backup_nested_element('notes');
        $note = new backup_nested_element('note', array('id'),
                                            array('courseid',
                                                  'notetext',
                                                  'color',
                                                  'timestart',
                                                  'timeend',
                                                  'status',
                                                  'sortorder'));
        $pluginwrapper->add_child($notes);
        $notes->add_child($note);
        $note->set_source_table('course_format_notes', array('courseid' => backup::VAR_COURSEID));

        // annotate files //
        $note->annotate_files('format_institutes', 'notetext', 'id');

        return $plugin;
    }

    /**
     * Returns the format information to attach to section element
     */
    protected function define_section_plugin_structure() {
        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, $this->get_format_condition(), 'institutes');
        // Create one standard named plugin element (the visible container).
        // The sectionid and courseid not required as populated on restore.
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());
        // Connect the visible container ASAP.
        $plugin->add_child($pluginwrapper);

        // course_format_sections //
        $formatsections = new backup_nested_element('formatsections');
        $formatsection = new backup_nested_element('formatsection', array('id'),
                                            array('courseid',
                                                  'format',
                                                  'sectionid',
                                                  'section',
                                                  'parent',
                                                  'level',
                                                  'parentssequence',
                                                  'imageid',
                                                  'timecreated',
                                                  'timemodified',
                                                  'sectiontype'
                                                  ));

        $pluginwrapper->add_child($formatsections);
        $formatsections->add_child($formatsection);
        $formatsection->set_source_table('course_format_sections', array('courseid' => backup::VAR_COURSEID, 'sectionid' => backup::VAR_SECTIONID));

        return $plugin;
    }

    /**
     * Returns the format information to attach to section element
     */
    protected function define_module_plugin_structure() {
        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, $this->get_format_condition(), 'institutes');
        // Create one standard named plugin element (the visible container).
        // The sectionid and courseid not required as populated on restore.
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());
        // Connect the visible container ASAP.
        $plugin->add_child($pluginwrapper);

        // course_format_settings //
        $formatsettings = new backup_nested_element('formatsettings');

        $formatsetting = new backup_nested_element('formatsetting', array('id'),
                                            array('courseid',
                                                  'type',
                                                  'name',
                                                  'value'));

        $pluginwrapper->add_child($formatsettings);
        $formatsettings->add_child($formatsetting);

        $formatsetting->set_source_table('course_format_settings', array('value'=>backup::VAR_MODID, 'courseid' => backup::VAR_COURSEID));

        return $plugin;
    }
}
