<?php

/**
 * Defines restore_format_institutes_plugin class
 *
 * @package     format_institutes
 * @category    backup
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot . '/course/format/institutes/lib.php');

/**
 * resource restore task that provides all the settings and steps to perform one
 * complete restore of the activity
 */
class restore_format_institutes_plugin extends restore_format_plugin {

    /**
     * Returns the paths to be handled by the plugin at course level
     */
    protected function define_course_plugin_structure() {
        $paths = array();

        // Add own format stuff.
        $elename = 'institutes'; // This defines the postfix of 'process_*' below.

        /*
         * This is defines the nested tag within 'plugin_format_grid_course' to allow '/course/plugin_format_grid_course' in
         * the path therefore as a path structure representing the levels in section.xml in the backup file.
         */
        $elepath = $this->get_pathfor('/');
        $paths[] = new restore_path_element($elename, $elepath);

        $paths[] = new restore_path_element('note', $this->get_pathfor('/notes/note'));

        return $paths;
    }

    /**
     * Process the 'plugin_format_institutes_course' element within the 'course' element in the 'course.xml' file in the '/course'
     * folder of the zipped backup 'mbz' file.
     */
    public function process_institutes($data) {
        global $DB;
        $data = (object) $data;
        /* We only process this information if the course we are restoring to
          has 'institutes' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'institutes') {
            return;
        }

    }

    public function process_note($data) {
        global $DB;
        $data = (object) $data;

        /* We only process this information if the course we are restoring to
          has 'institutes' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'institutes') {
            return;
        }

        $oldid = $data->id;
        $data->courseid = $this->task->get_courseid();
        $newitemid = $DB->insert_record('course_format_notes', $data);
        if (!$newitemid) {
            throw new moodle_exception('invalidrecordid', 'format_institutes', '',
            'Could not set notes');
        }

        $this->set_mapping('note', $oldid, $newitemid, true, null);
    }

   /**
     * Returns the paths to be handled by the plugin at section level
     */
    protected function define_section_plugin_structure() {
        $paths = array();
        // Add own format stuff.
        $elename = 'institutessection'; // This defines the postfix of 'process_*' below.
        /* This is defines the nested tag within 'plugin_format_institutes_section' to allow '/section/plugin_format_institutes_section' in
         * the path therefore as a path structure representing the levels in section.xml in the backup file.
         */
        $elepath = $this->get_pathfor('/formatsections/formatsection');
        $paths[] = new restore_path_element($elename, $elepath);

        return $paths; // And we return the interesting paths.
    }
    /**
     * Process the 'plugin_format_institutes_section' element within the 'section' element in the 'section.xml' file in the
     * '/sections/section_sectionid' folder of the zipped backup 'mbz' file.
     */
    public function process_institutessection($data) {
        global $DB, $backup_sections;
        $data = (object) $data;

        /* We only process this information if the course we are restoring to
           has 'institutes' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'institutes') {
            return;
        }

        $this->set_mapping('backup_sections', $data->sectionid, $this->task->get_sectionid());
        $data->courseid = $this->task->get_courseid();
        $data->sectionid = $this->task->get_sectionid();

        if (!$DB->record_exists('course_format_sections', array('courseid' => $data->courseid, 'sectionid' => $data->sectionid))) {
            $data->timecreated = time();
            $data->timemodified = time();
            if (!$DB->insert_record('course_format_sections', $data, true)) {
                throw new moodle_exception('invalidrecordid', 'format_institutes', '',
                'Could not insert format institutes sections.');
            }
        }
        // No need to annotate anything here.
    }

    /**
     * Returns the paths to be handled by the plugin at section level
     */
    protected function define_module_plugin_structure() {
        $paths = array();

        $elepath = $this->get_pathfor('/formatsettings/formatsetting');
        $paths[] = new restore_path_element('institutessetting', $elepath);

        return $paths;
    }

    public function process_institutessetting($data) {
        global $DB;
        $data = (object) $data;

        if (empty($data->value)){
            return;
        }

        $data->courseid = $this->task->get_courseid();
        $data->value = $this->task->get_moduleid();

        if (!$DB->insert_record('course_format_settings', $data, true)) {
            throw new moodle_exception('invalidrecordid', 'format_institutes', '',
            'Could not insert format institutes sections.');
        }
    }

    protected function after_restore_course() {
        global $DB;

        // process format sections
        $sections = $DB->get_records_sql("SELECT * FROM {course_format_sections} WHERE courseid = :courseid AND format = :format ORDER BY section", array('courseid'=>$this->task->get_courseid(), 'format'=>'institutes'));
        if (count($sections)){
            foreach ($sections as $section){
                if ($section->parent == 0) continue;

                $section->parent = $this->get_mappingid('backup_sections', $section->parent);

                $parentssequence = explode(',', $section->parentssequence);
                $newsequence = array();
                if (count($parentssequence)){
                    foreach($parentssequence as $parent){
                        $newsequence[] = $this->get_mappingid('backup_sections', $parent);
                    }
                    $section->parentssequence = implode(',', $newsequence);
                }

                $DB->update_record('course_format_sections', $section);

                $course_section_options = $DB->get_record('course_format_options', array('courseid'=>$this->task->get_courseid(), 'format'=>'institutes', 'sectionid'=>$section->sectionid, 'name'=>'parent'));
                if ($course_section_options){
                    $course_section_options->value = $section->parent;
                    $DB->update_record('course_format_options', $course_section_options);
                }
            }
        }

        $this->add_related_files('format_institutes', 'notetext', 'note');
    }

}
