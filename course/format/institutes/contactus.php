<?php

/**
 * institutes version file.
 *
 * @package    format_institutes
 * @copyright  2017 The Institutes
 */


require('../../../config.php');
require_once('lib.php');

$systemcontext   = context_system::instance();

$id             = required_param('id', PARAM_INT); // Option id.

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/institutes/contactus.php', $pageparams);

$settings = $PAGE->theme->settings;
// prepare course and format
$title = (!empty($settings->contactustitle)) ? $settings->contactustitle : get_string('contactus', 'theme_institutes');

$PAGE->navbar->add($title, new moodle_url('/course/format/institutes/contactus.php', $pageparams));

$PAGE->set_title($title);
$PAGE->set_heading($title);
$USER->editing = 0;
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(course_get_url($course));
    $PAGE->set_button($buttons);
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title, '1', 'contactus-title');

echo html_writer::start_tag('div', array('class' => 'ceu-contactus'));
echo $settings->contacttext;
echo html_writer::end_tag('div');


echo $OUTPUT->footer();
