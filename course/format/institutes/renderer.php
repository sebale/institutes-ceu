<?php

/**
 * Renderer for outputting the institutes course format.
 *
 * @package format_institutes
 * @copyright  2017 The Institutes
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');

class format_institutes_course_content_header implements renderable {}
class format_institutes_course_content_footer implements renderable {}


/**
 * Basic renderer for institutes format.
 *
 * @copyright  2017 The Institutes
 */
class format_institutes_renderer extends format_section_renderer_base {

    public $_sections = array();
    public $_states = array();
    public $_requirements = array();
    public $_canviewhiddensections = false;
    public $_root_parent = null;
    public $_section_types = array('0'=>'modules', '1'=>'exams', '2'=>'general', '3'=>'modules', '4'=>'heading', '5'=>'segment', '6'=>'resources', '7'=>'resourceslinks', '8'=>'pageactivities', '9'=>'assignments');
    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_institutes_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');

        $course = $page->course;
        $course = course_get_format($course)->get_course();

        // process course format sections
        $this->_sections = format_institutes_get_course_sections($course);

        // process instututes requirements
        $this->_canviewhiddensections = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id));
        $this->_states = format_institutes_get_course_states($course);
        $this->_requirements = format_institutes_get_course_requirements($course, $this->_states);

    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list($sectiontype = 0) {
        return html_writer::start_tag('ul', array('class' => 'institutes'.(($sectiontype == FORMAT_SECTIONS_EXAMS) ? ' exams-view' : ' modules-view')));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('topicoutline');
    }

    /**
     * Generate the section title, wraps it in a link to the section page if page is to be displayed on a separate page
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title($section, $course, $status = '') {
        //return $this->render(course_get_format($course)->inplace_editable_render_section_name($section));
        $title = get_section_name($course, $section);
        $url = new moodle_url('/course/view.php',
                                array('id' => $course->id,
                                      'section' => $section->section));

        return $title;
    }

    /**
     * Generate the section title to be displayed on the section page, without a link
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @return string HTML to output.
     */
    public function section_title_without_link($section, $course) {
        //return $this->render(course_get_format($course)->inplace_editable_render_section_name($section, false));
        return get_section_name($course, $section);
    }

    public function get_course_formatted_summary($course, $options = array()) {
        global $CFG;
        require_once($CFG->libdir. '/filelib.php');
        if (empty($course->summary)) {
            return '';
        }
        $options = (array)$options;
        $context = context_course::instance($course->id);
        if (!isset($options['context'])) {
            // TODO see MDL-38521
            // option 1 (current), page context - no code required
            // option 2, system context
            // $options['context'] = context_system::instance();
            // option 3, course context:
            // $options['context'] = $context;
            // option 4, course category context:
            // $options['context'] = $context->get_parent_context();
        }
        $summary = file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', null);
        $summary = format_text($summary, $course->summaryformat, $options, $course->id);

        return $summary;
    }


    public function print_mainlevel_section_page($course, $sections, $mods, $modnames, $modnamesused) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);

        // course notifications
        echo $this->course_notifications($course);

        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // display course summary
        $course_summary = $this->get_course_formatted_summary($course,
                          array('overflowdiv' => true, 'noclean' => false, 'para' => false));

        if (!empty($course_summary)){
            echo html_writer::start_tag('div', array('class' => 'courseformat-summary'));
                echo $course_summary;
            echo html_writer::end_tag('div');
        }

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        echo html_writer::start_tag('div', array('class' => 'sections-containter'));

        // Now the list of sections..
        echo $this->start_section_list();

        $j = 1;
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            $thissection = $this->process_section_data($thissection);

            if (!$this->can_view_section($thissection)){
                continue;
            }

            if(intval($thissection->parent) > 0 or $section == 0 or $thissection->sectiontype == FORMAT_SECTIONS_RESOURCES){
                continue;
            }
            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }
            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            echo $this->section_header($thissection, $course, false, 0);
            echo html_writer::tag('div', $this->format_summary_text($thissection), array('class' => 'section-summary'));

            if (!$PAGE->user_is_editing() and $thissection->sectiontype == FORMAT_SECTIONS_GENERAL){
                echo $this->print_instructions_sections($course, $thissection);
            }

            echo $this->section_footer();
            $j++;
        }

        echo $this->end_section_list();

        echo html_writer::end_tag('div');
    }

    public function print_instructions_sections($course, $thissection) {
        global $PAGE, $DB;
        $output = '';

        $modinfo = get_fast_modinfo($course);
        $sections = $this->get_all_section_childs($course, $modinfo, $thissection->section, $sections = array());
        $instructions = array();

        if (count($sections)){

            foreach ($sections as $section){
                if (!$this->can_view_section($section)){
                    continue;
                }
                if (isset($modinfo->sections[$section->section])){
                    foreach ($modinfo->sections[$section->section] as $modnumber) {
                        $mod = $modinfo->cms[$modnumber];

                        if ($mod->modname == 'resource'){
                            $instructions[] = $mod;
                        }
                    }
                }
            }
        }

        if (count($instructions)){
            $fs = get_file_storage();

            $output .= html_writer::start_tag('div', array('class' => 'course-instruction-box'));
            $output .= html_writer::start_tag('ul');
                foreach ($instructions as $mod){
                    $cmcontext = context_module::instance($mod->id);
                    $resource = $DB->get_record('resource', array('id'=>$mod->instance), '*', MUST_EXIST);
                    $files = $fs->get_area_files($cmcontext->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
                    $file = reset($files); unset($files);
                    $path = '/'.$cmcontext->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
                    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, true);

                    $output .= html_writer::start_tag('li');
                        $output .= html_writer::link($fullurl, $mod->get_formatted_name(), array('target'=>'_blank'));
                    $output .= html_writer::end_tag('li');
                }
            $output .=  html_writer::end_tag('ul');
            $output .=  html_writer::end_tag('div');
        }

        return $output;
    }

    public function print_innerlevel_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $parentsection = 0; $childsections = array(); $currentchildsections = array();

        $currentsection = $modinfo->get_section_info($displaysection);
        $currentsection = $this->process_section_data($currentsection);
        $root_parent = $this->get_section_root_parent($course, $modinfo, $currentsection);

        if ($currentsection->parent > 0 and isset($this->_sections[$currentsection->parent]->section)){
            $parentsection = $modinfo->get_section_info($this->_sections[$currentsection->parent]->section);
        } else {
            $parentsection = $currentsection;
        }

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            $thissection = $this->process_section_data($thissection);

            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            } elseif ($thissection->parent == $currentsection->id){
                $currentchildsections[$section] = $thissection;
            }
        }

        echo $this->start_section_list($currentsection->sectiontype);

        if ($parentsection){

            if ($currentsection->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS){
                echo html_writer::start_tag('div', array('class' => 'inner-sectionname'));

                if (isset($this->_root_parent->sectiontype) and $this->_root_parent->sectiontype == FORMAT_SECTIONS_RESOURCES){

                    // show heading
                    $parentsectiontype = format_institutes_get_sections_parent($course, $currentsection, 4);
                    echo html_writer::tag('h3', ((isset($parentsectiontype->id)) ? get_section_name($course, $parentsectiontype) : get_section_name($course, $currentsection)), array('class' => 'sectionname'));
                    echo $this->section_progress($course, $currentsection);

                    if (isset($parentsection->id)){
                        echo html_writer::tag('h4', get_section_name($course, $currentsection), array('class' => 'sectionname-level-2'));
                    }
                } else {
                    echo html_writer::tag('h3', get_section_name($course, $currentsection), array('class' => 'sectionname'));
                    echo $this->section_progress($course, $currentsection);
                }
                echo html_writer::end_tag('div');
            } else {
                echo $this->section_header($parentsection, $course, false, $parentsection->section);
                echo $this->section_footer();
                echo $this->section_progress($course, $parentsection);
            }

            if($parentsection->level == 0){
                    if (count($childsections)){
                        echo $this->print_sections_anchors_menu($course, $modinfo, $childsections);
                    }
                    echo html_writer::start_tag('li', array('class' => 'section main clearfix section-level-1 topic-view sectiontype-heading mainlevel-activitylist'));
                        echo html_writer::start_tag('div', array('class'=>'content'));
                            echo html_writer::start_tag('div', array('class'=>'segment-section-modules-box activities-box'));
                                echo $this->courserenderer->course_section_cm_list($course, $parentsection);
                            echo html_writer::end_tag('div');
                        echo html_writer::end_tag('div');
                    echo html_writer::end_tag('li');
                }

            if (count($childsections)){

                foreach ($childsections as $section){
                    if (!$this->can_view_section($section)){
                        continue;
                    }

                    if ($currentsection->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS and $section->id != $currentsection->id){
                        continue;
                    }

                    if ($section->sectiontype == FORMAT_SECTIONS_SEGMENT){
                        echo $this->print_segment_section($section, $modinfo, $course, $currentsection);
                    } else if (($section->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS or $section->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES) and $section->id != $currentsection->id){
                        echo $this->print_segment_inner_sections($course, $modinfo, $section, array($section), $currentsection);
                    } else {
                        // print inner parent
                        if ($currentsection->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS and $currentsection->sectiontype != FORMAT_SECTIONS_PAGEACTIVITIES){
                            echo $this->section_header($section, $course, false, $currentsection->section);
                        }

                        if (($this->get_section_activities($course, $modinfo, $currentsection->section) or $this->get_section_activities($course, $modinfo, $section->section)) and $section->sectiontype != FORMAT_SECTIONS_MODULE){
                            echo html_writer::start_tag('div', array('class' => 'segment-section-modules-box activities-box'));
                            echo $this->courserenderer->course_section_cm_list($course, $section, 0, array('displaylabel'=>true, 'displayaudio'=>((isset($this->_root_parent->sectiontype) and $this->_root_parent->sectiontype == FORMAT_SECTIONS_RESOURCES) ? true : false)));
                            echo html_writer::end_tag('div');
                        }

                        $innersections = $this->get_section_childs($course, $modinfo, $section->section);
                        if (count($innersections)){
                            echo $this->print_inner_sections($course, $modinfo, $innersections, $currentsection);
                        }

                        // print inner parent
                        if ($currentsection->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS and $currentsection->sectiontype != FORMAT_SECTIONS_PAGEACTIVITIES){
                            echo $this->section_footer();
                        }
                    }
                }
            }
        }

        echo $this->end_section_list();

    }


    // print activities sections
    public function print_activities_section_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $parentsection = 0; $childsections = array();

        $currentsection = $modinfo->get_section_info($displaysection);
        $currentsection = $this->process_section_data($currentsection);

        if ($currentsection->parent > 0 and isset($this->_sections[$currentsection->parent]->section)){
            $parentsection = $modinfo->get_section_info($this->_sections[$currentsection->parent]->section);
        } else {
            $parentsection = $currentsection;
        }

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            $thissection = $this->process_section_data($thissection);

            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }

        echo $this->start_section_list($currentsection->sectiontype);

        if ($parentsection){

            echo html_writer::tag('h3', get_string('activities', 'format_institutes'), array('class' => 'activities-section-title'));
            echo html_writer::tag('div', get_section_name($course, $currentsection), array('class' => 'activities-section-name'));

            if (count($childsections)){

                foreach ($childsections as $section){
                    if (!$this->can_view_section($section)){
                        continue;
                    }
                    if ($section->id != $currentsection->id){
                        continue;
                    }


                    if (($this->get_section_activities($course, $modinfo, $currentsection->section) or $this->get_section_activities($course, $modinfo, $section->section)) and $section->sectiontype != FORMAT_SECTIONS_MODULE){
                        echo html_writer::start_tag('div', array('class' => 'segment-section-modules-box activities-box'));
                        echo $this->courserenderer->course_section_cm_activitieslist($course, $section, 0, array('displaylabel'=>true));
                        echo html_writer::end_tag('div');
                    }

                }
            }

        }

        echo $this->end_section_list();

    }

    public function print_segment_section($section, $modinfo, $course, $currentsection) {
        global $PAGE;
        $output = '';

        $output .= $this->section_header($section, $course, false, $currentsection->section);

            if ($this->get_section_activities($course, $modinfo, $section->section)){
                $output .= html_writer::start_tag('div', array('class' => 'segment-section-modules-box'));
                $output .= $this->courserenderer->course_section_cm_list($course, $section, 0, array('displaylabel'=>true,  'displayaudio'=>((isset($this->_root_parent->sectiontype) and $this->_root_parent->sectiontype == FORMAT_SECTIONS_RESOURCES) ? true : false)));
                $output .= html_writer::end_tag('div');
            }

            $innersections = $this->get_section_childs($course, $modinfo, $section->section);
            if (count($innersections)){
                $output .= $this->print_segment_inner_sections($course, $modinfo, $section, $innersections, $currentsection);
            }
        $output .= $this->section_footer();

        return $output;
    }

    public function print_segment_inner_sections($course, $modinfo, $section, $innersections, $currentsection) {
        global $PAGE;
        $output = '';

        if (count($innersections)){

            $output .= html_writer::start_tag('ul', array('class' => 'segment-sections-box'));
            foreach ($innersections as $isection){

                if ($isection->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS and $isection->sectiontype != FORMAT_SECTIONS_PAGEACTIVITIES){
                    $output .= $this->section_header($isection, $course, false, $currentsection->section);
                } else {

                    $output .= html_writer::start_tag('li', array('class' => 'segment-sections-item clearfix'));

                    $sectionurl = (isset($this->_root_parent->sectiontype) and $this->_root_parent->sectiontype == FORMAT_SECTIONS_RESOURCES) ? '/course/format/institutes/resources.php' : '/course/view.php';

                    // section name
                    $output .= html_writer::start_tag('div', array('class' => 'segment-sections-link'));
                        $url = new moodle_url($sectionurl,
                            array('id' => $course->id,
                                  'section' => $isection->section));
                        $output .= html_writer::link($url, get_section_name($course, $isection));
                    $output .= html_writer::end_tag('div');

                    // section button
                    $output .= html_writer::start_tag('div', array('class' => 'segment-sections-btnlink'));
                        $url = new moodle_url($sectionurl, array('id' => $course->id, 'section' => $isection->section));
                        $output .= html_writer::link($url, get_string('view', 'format_institutes'), array('class'=>'btn'));
                    $output .= html_writer::end_tag('div');

                    // section completion
                    $output .= $this->print_section_completion($course, $isection);

                    $output .= html_writer::end_tag('li');

                    if ($isection->sectiontype == FORMAT_SECTIONS_SEGMENT){
                        $innersections = $this->get_section_childs($course, $modinfo, $isection->section);
                        $output .= print_segment_inner_sections($course, $modinfo, $isection, $innersections, $currentsection);
                    }
                }
            }
            $output .= html_writer::end_tag('ul');
        }


        return $output;
    }

    public function print_sections_anchors_menu($course, $modinfo, $sections) {
        global $PAGE;
        $output = '';

        $printsections = array();
        foreach ($sections as $section){
            if (!$this->can_view_section($section) or ($section->sectiontype != FORMAT_SECTIONS_HEADING and $section->sectiontype != FORMAT_SECTIONS_ASSIGNMENTS)){
                continue;
            }
            $printsections[] = $section;
        }

        if (count($printsections)){
            $output .= html_writer::start_tag('ul', array('class' => 'sections-anchors-list'));
            foreach ($printsections as $section){
                $output .= html_writer::start_tag('li');
                    $output .= html_writer::link('#section-'.$section->section, get_section_name($course, $section));
                $output .= html_writer::end_tag('li');
            }
            $output .= html_writer::end_tag('ul');
        }

        return $output;
    }

    public function section_progress($course, $section) {
        global $USER;
        $output = '';

        $context = context_course::instance($course->id);

        //if(is_enrolled($context, $USER)) {
            $progress = $this->get_section_completion($course, $section->section);

            $output .= html_writer::start_tag('div', array('class' => 'section-progress-box '.$progress['status']));
                $output .= html_writer::tag('label', $progress['progress'].'% '.get_string('complete', 'format_institutes'));

                $output .= html_writer::start_tag('div', array('class' => 'section-progress'));
                    $output .= html_writer::tag('div', '', array('class'=>'section-progress-percentage', 'style'=>'width:'.$progress['progress'].'%;'));
                $output .= html_writer::end_tag('div');
            $output .= html_writer::end_tag('div');
        //}

        return $output;
    }


    public function print_section_completion($course, $section) {
        $output = '';

        $completionclass = ' notstarted';
        $completiontext = $completiontitle = get_string('notstarted', 'format_institutes');

        $progress = $this->get_section_completion($course, $section->section);

        if ($progress['status'] == 'inprogress'){
            $completionclass = ' inprogress';
            $completiontext = $completiontitle = get_string('inprogress', 'format_institutes');
        } elseif ($progress['status'] == 'completed') {
            $completionclass = ' completed';
            $completiontext = $completiontitle = get_string('completed', 'format_institutes');
        }

        $output .= html_writer::start_tag('div', array('class'=>'clearfix completion-actions-box auto'.$completionclass));
            $output .= html_writer::tag('span', '', array('class'=>'completion-actions-icon', 'title'=>$completiontitle));
            $output .= html_writer::tag('span', $completiontext, array('class' => 'completion-actions-text'));
        $output .= html_writer::end_tag('div');

        return $output;
    }

    public function get_section_activities($course, $modinfo, $displaysection) {

        $parentsection = $modinfo->get_section_info($displaysection);
        $activities = 0;

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section != $displaysection){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            $activities = (isset($modinfo->sections[$section]) and !empty($modinfo->sections[$section])) ? count($modinfo->sections[$section]) : 0;
        }

        return $activities;
    }

    public function get_section_childs($course, $modinfo, $displaysection, $withrequirements = false) {

        $parentsection = $modinfo->get_section_info($displaysection);
        $childsections = array();

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }
            $thissection = $this->process_section_data($thissection);
            if ($withrequirements){
                if (!$this->can_view_section($thissection)){
                    continue;
                }
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }

        return $childsections;
    }

    public function get_all_section_childs($course, $modinfo, $displaysection, $sections = array(), $manage = false) {

        $parentsection = $modinfo->get_section_info($displaysection);
        $childsections = array();

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if (!$manage){
                if($section == 0){
                    continue;
                }

                // Show the section if the user is permitted to access it, OR if it's not available
                // but there is some available info text which explains the reason & should display.
                $showsection = $thissection->uservisible ||
                        ($thissection->visible && !$thissection->available &&
                        !empty($thissection->availableinfo));
                if (!$showsection) {
                    continue;
                }
            }

            $thissection = $this->process_section_data($thissection);

            if ($thissection->parent == $parentsection->id){
                $childsections[$section] = $thissection;
            }
        }
        if (count($childsections)){
            foreach ($childsections as $child){
                $sections[$child->section] = $child;
                $sections = $this->get_all_section_childs($course, $modinfo, $child->section, $sections);
            }
        }

        return $sections;
    }

    public function get_section_completion($course, $sectionid) {
        global $CFG, $DB, $PAGE;

        $progress = array('status'=>'notstarted', 'progress'=>0);

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $currentsection = $modinfo->get_section_info($sectionid);
        $completioninfo = new completion_info($course);

        $context = context_course::instance($course->id);
        $states = format_institutes_get_course_states($course);
        $requirements = format_institutes_get_course_requirements($course, $states);
        $canviewhiddensections = has_capability('moodle/course:viewhiddensections', $context);

        $sections = array($currentsection->section=>$currentsection) + $this->get_all_section_childs($course, $modinfo, $currentsection->section, array());

        $modules = 0; $completed = 0; $viewed = 0;

        if (count($sections)){
            foreach ($sections as $section){
                if ($section->section == 0) continue;

                $section = $this->process_section_data($section);

                if (!isset($modinfo->sections[$section->section])) continue;

                // check user states
                if (isset($section->state) && !empty($section->state) && !in_array($section->state, $states) && !$canviewhiddensections) {
                    continue;
                }

                foreach ($modinfo->sections[$section->section] as $modnumber) {

                    $mod = $modinfo->cms[$modnumber];
                    if (!$mod->uservisible) continue;

                    // modules
                    $completion = $completioninfo->is_enabled($mod);
                    if ($completion == COMPLETION_TRACKING_NONE) {
                        continue;
                    }
                    $modules++;

                    $completiondata = $completioninfo->get_data($mod, true);
                    // viewed
                    if ($completiondata->viewed){
                        $viewed++;
                    }

                    // completed
                    if ($completiondata->completionstate == COMPLETION_COMPLETE OR $completiondata->completionstate == COMPLETION_COMPLETE_PASS){
                        $completed++;
                    }

                }

            }
        }

        if ($modules > 0){
            if ($viewed > 0 or $completed > 0) $progress['status'] = 'inprogress';
            if ($completed > 0){
                if ($modules == $completed){
                    $progress['status'] = 'completed';
                    $progress['progress'] = 100;
                } else {
                    $progress['progress'] = round(($completed/$modules) * 100);
                }
            }
        }


        return $progress;
    }

    public function print_inner_sections($course, $modinfo, $sections, $currentsection){
        global $CFG, $OUTPUT;
        $o = '';

        if (count($sections)){
            $o .= html_writer::start_tag('ul', array('class' => 'inner-sections'));
                $i = 1;
                foreach ($sections as $section){
                    if (!$this->can_view_section($section)){
                        continue;
                    }
                    if ($section->sectiontype == FORMAT_SECTIONS_SEGMENT){
                        $o .= $this->print_segment_section($section, $modinfo, $course, $currentsection);
                    } elseif ($section->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS or $section->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES){
                        $o .= $this->print_segment_inner_sections($course, $modinfo, $section, array($section), $currentsection);
                    } else {
                        $o .= $this->section_header($section, $course, false, $section->section, 1);

                        $innersections = $this->get_section_childs($course, $modinfo, $section->section);
                        if (count($innersections)){
                            $o .= $this->print_inner_sections($course, $modinfo, $innersections, $currentsection);
                        }
                    }
                }
            $o .= html_writer::end_tag('ul');
        }

        return $o;
    }


    /**
     * Output the html for a multiple section page with topic view
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections (argument not used)
     * @param array $mods (argument not used)
     * @param array $modnames (argument not used)
     * @param array $modnamesused (argument not used)
     */
    public function print_editing_sections_page($course, $sections, $mods, $modnames, $modnamesused, $displaysection) {
        global $PAGE;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $context = context_course::instance($course->id);
        // Title with completion help icon.
        $completioninfo = new completion_info($course);
        echo $completioninfo->display_help_icon();
        echo $this->output->heading($this->page_title(), 2, 'accesshide');

        // Copy activity clipboard..
        echo $this->course_activity_clipboard($course, 0);

        $movingsection = course_get_format($course)->is_moving_section();
        if ($movingsection){
            // reset section moving
            echo html_writer::start_tag('div', array('class' => 'mdl-right reset-section-moving'));
            $cancelmovingurl = course_get_url($course->id);
            $straddsection = get_string('cancelmoving', 'format_institutes');
            echo html_writer::link($cancelmovingurl, $straddsection, array('class' => 'btn', 'style'=>'margin-bottom:15px;'));
            echo html_writer::end_tag('div');
        }

        $root_sections = array();
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {

            $thissection = $this->process_section_data($thissection);

            if ($section > $course->numsections) {
                // activities inside this section are 'orphaned', this section will be printed as 'stealth' below
                continue;
            }

            // if current section is transcript section
            $can_view_section = $this->can_view_section($thissection);
            if ($can_view_section > 1){
                $thissection->visible = 0;
            } elseif (!$can_view_section){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                // If the hiddensections option is set to 'show hidden sections in collapsed
                // form', then display the hidden section message - UNLESS the section is
                // hidden by the availability system, which is set to hide the reason.
                if (!$course->hiddensections && $thissection->available) {
                    echo $this->section_hidden($section, $course->id);
                }

                continue;
            }

            if (!$thissection->parent){
                $root_sections[$thissection->section] = $thissection;
            }
        }

        if (count($root_sections)){
            $i = 1;
            foreach ($root_sections as $rsection){
                $this->display_section($course, $rsection->section, 0, $i++, count($root_sections));
            }
        }

        // Increase number of sections.
        echo html_writer::start_tag('div', array('id' => 'changenumsections', 'class' => 'mdl-right'));
        $straddsection = get_string('createnewsection', 'format_institutes');
        $url = new moodle_url('/course/format/institutes/addsection.php',
            array('courseid' => $course->id,
                  'parent' => 0));
        echo html_writer::link($url, $straddsection, array('class' => 'increase-sections btn', 'style'=>'margin-top:15px;'));
        echo html_writer::end_tag('div');

    }

    /**
     * Display section and all its activities and subsections (called recursively)
     *
     * @param int|stdClass $course
     * @param int|section_info $section
     * @param int $sr section to return to (for building links)
     * @param int $level nested level on the page (in case of 0 also displays additional start/end html code)
     */
    public function display_section($course, $section, $level = 0, $root = 0, $scount = 0) {
        global $PAGE;
        $course = course_get_format($course)->get_course();
        $section = course_get_format($course)->get_section($section);
        $context = context_course::instance($course->id);

        $section = $this->process_section_data($section);
        $sectionnum = $section->section;

        $contentvisible = true;
        if (!$section->uservisible) {
            if ($section->visible && !$section->available && $section->availableinfo) {
                // Still display section but without content.
                $contentvisible = false;
            } else {
                return '';
            }
        }

        $movingsection = course_get_format($course)->is_moving_section();
        if ($level === 0) {
            echo html_writer::start_tag('ul', array('class' => 'section-level-'.$level));
            if ($section->section) {
                $this->display_insert_section_here($course, $section->parent, $section->section);
            }
        }
        echo html_writer::start_tag('li',
                array('class' => "section main".
                    ($movingsection === $sectionnum ? ' ismoving' : '').
                    (course_get_format($course)->is_section_current($section) ? ' current' : '').
                    (($section->coursetranscriptflag) ? ' section-transcript' : '').
                    (($section->visible && $contentvisible) ? '' : ' hidden'),
                    'id' => 'section-'.$sectionnum));

        $rightcontent = $this->section_right_content($section, $course, false);
        echo html_writer::tag('div', $rightcontent, array('class' => 'right side'));

        // display section content
        echo html_writer::start_tag('div', array('class' => 'content clearfix'));
        // display section name and expanded/collapsed control
        if ($sectionnum && ($title = $this->section_title($section, $course, ($level == 0) || !$contentvisible))) {
            echo html_writer::tag('h3', $title, array('class' => 'sectionname'));
        }

        echo $this->section_availability_message($section,
            has_capability('moodle/course:viewhiddensections', $context));

        // display section description (if needed)
        if ($contentvisible && ($summary = $this->format_summary_text($section))) {
            echo html_writer::tag('div', $summary, array('class' => 'summary'));
        } else {
            echo html_writer::tag('div', '', array('class' => 'summary nosummary'));
        }
        // display section contents (activities and subsections)
        if ($contentvisible) {
            // display resources and activities
            echo $this->courserenderer->course_section_cm_list($course, $section);

            // a little hack to allow use drag&drop for moving activities if the section is empty
            if (empty(get_fast_modinfo($course)->sections[$sectionnum])) {
                echo "<ul class=\"section img-text\">\n</ul>\n";
            }
            echo $this->courserenderer->course_section_add_cm_control($course, $sectionnum);

            // display subsections
            $modinfo = get_fast_modinfo($course);
            $children = $this->get_section_childs($course, $modinfo, $section->section);

            if ((!empty($children) || $movingsection) /*and $section->section > 0*/) {
                echo html_writer::start_tag('ul', array('class' => 'section-level-'.($level+1)));
                foreach ($children as $chsection) {
                    $this->display_insert_section_here($course, $section, $chsection->section);
                    $this->display_section($course, $chsection->section, $level+1);
                }
                if ($section->section) {
                    $this->display_insert_section_here($course, $section, null);
                }
                echo html_writer::end_tag('ul');
            }
        }
        echo html_writer::end_tag('div');
        echo html_writer::end_tag('li');
        if ($level === 0){
            if ($section->section and $root == $scount) {
                $this->display_insert_section_here($course, $section->parent, null);
            }
            echo html_writer::end_tag('ul');
        }
    }

    public function course_notifications($course) {
        $output = '';
        $notifications = format_institutes_get_course_notifications($course);
        $context = context_course::instance($course->id);

        if (count($notifications)){
            foreach ($notifications as $note){
                $output .= html_writer::start_tag('div', array('class' => 'course-notifications '.$note->color, 'id'=>'note_'.$note->id));
                $output .= html_writer::tag('i', '', array('class' => 'ion-alert course-notifications-alert'));

                $notetext = file_rewrite_pluginfile_urls($note->notetext, 'pluginfile.php', $context->id, 'format_intitutes_ceu', 'notetext', $note->id);

                $options = new stdClass();
                $options->noclean = true;
                $options->overflowdiv = true;
                $content = format_text($notetext, 1, $options);

                $output .=  html_writer::tag('div', $content, array('class' => 'course-notifications-content'));
                $output .=  html_writer::tag('i', '', array('class' => 'ion-close course-notifications-close', 'title'=>get_string('close', 'format_institutes'), 'onclick'=>'jQuery("#note_'.$note->id.'").remove();'));
                $output .=  html_writer::end_tag('div');
            }
        }

        return $output;
    }

    public function course_start_buttons($course) {
        $output = '';
        $course_completion = $this->get_course_completion($course);

        if ($course_completion->status == 'completed'){
            return $output;
        }

        $modinfo = get_fast_modinfo($course);
        $url = '';


        $course_resources = format_institutes_get_course_hiddenresources($course);
        $states = format_institutes_get_course_states($course);
        $requirements = format_institutes_get_course_requirements($course, $states);

        $coursecontext = context_course::instance($course->id);
        $canviewhiddensections = has_capability('moodle/course:viewhiddensections', $coursecontext);

        if ($course_completion->status == 'notyetstarted' or $course_completion->status == 'pending'){

            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if (!$thissection->visible or $thissection->section <= 1) continue;

                $parentsection = $this->get_parent_section($course, $thissection->id);
                if (isset($parentsection->section) and $parentsection->section == 1) continue;

                if (isset($thissection->state) && !empty($thissection->state) && !in_array($thissection->state, $states) && !$canviewhiddensections) {
                    continue;
                }

                $url = new moodle_url('/course/view.php',
                                array('id' => $course->id,
                                      'section' => $section));
                $title = get_string('get_started', 'format_institutes');
                break;
            }

        } else {

            $completioninfo = new completion_info($course);
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if (!$thissection->visible or $thissection->section <= 1) continue;
                if (!empty($url)) break;

                $parentsection = $this->get_parent_section($course, $thissection->id);
                if (isset($parentsection->section) and $parentsection->section == 1) continue;

                if (isset($thissection->state) && !empty($thissection->state) && !in_array($thissection->state, $states) && !$canviewhiddensections) {
                    continue;
                }

                if (isset($modinfo->sections[$thissection->section])) {
                    foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                        if (!empty($url)) break;

                        $mod = $modinfo->cms[$modnumber];

                        if (!$this->has_nav_mod($mod) or $section < 1){
                            continue;
                        }

                        $completion = $completioninfo->is_enabled($mod);
                        if ($completion == COMPLETION_TRACKING_NONE) {
                            continue;
                        }
                        $completiondata = $completioninfo->get_data($mod, true);

                        // completed
                        if ($completiondata->completionstate == COMPLETION_COMPLETE OR $completiondata->completionstate == COMPLETION_COMPLETE_PASS){
                            continue;
                        } else {
                            $url = new moodle_url('/mod/'.$mod->modname.'/view.php',
                                        array('id' => $mod->id));
                            $title = get_string('continue', 'format_institutes');
                            break;
                        }

                    }
                }
            }

        }
        if (!empty($url)){
            $output .= html_writer::start_tag('div', array('class' => 'course-startbutton-box'));
                $output .= html_writer::link($url, $title, array('class'=>'btn'));
            $output .=  html_writer::end_tag('div');
        }

        return $output;
    }

    public function get_course_completion($course) {
        global $CFG, $PAGE, $DB, $USER, $OUTPUT;

        require_once("{$CFG->libdir}/completionlib.php");

        $result = new stdClass();
        $result->completion = 0;
        $result->status = 'notyetstarted';

        // Get course completion data.
        $info = new completion_info($course);

        // Load criteria to display.
        $completions = $info->get_completions($USER->id);

        if ($info->is_tracked_user($USER->id) and $course->startdate <= time()) {

            // For aggregating activity completion.
            $activities = array();
            $activities_complete = 0;

            // For aggregating course prerequisites.
            $prerequisites = array();
            $prerequisites_complete = 0;

            // Flag to set if current completion data is inconsistent with what is stored in the database.
            $pending_update = false;

            // Loop through course criteria.
            foreach ($completions as $completion) {
                $criteria = $completion->get_criteria();
                $complete = $completion->is_complete();

                if (!$pending_update && $criteria->is_pending($completion)) {
                    $pending_update = true;
                }

                // Activities are a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    $activities[$criteria->moduleinstance] = $complete;

                    if ($complete) {
                        $activities_complete++;
                    }

                    continue;
                }

                // Prerequisites are also a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    $prerequisites[$criteria->courseinstance] = $complete;

                    if ($complete) {
                        $prerequisites_complete++;
                    }

                    continue;
                }
            }

            $itemsCompleted  = $activities_complete + $prerequisites_complete;
            $itemsCount      = count($activities) + count($prerequisites);

            // Aggregate completion.
            if ($itemsCount > 0) {
                $result->completion = round(($itemsCompleted / $itemsCount) * 100);
            }

            // Is course complete?
            $coursecomplete = $info->is_course_complete($USER->id);

            // Load course completion.
            $params = array(
                'userid' => $USER->id,
                'course' => $course->id
            );
            $ccompletion = new completion_completion($params);

            // Has this user completed any criteria?
            $criteriacomplete = $info->count_course_user_data($USER->id);

            if ($pending_update) {
                $status = 'pending';
            } else if ($coursecomplete) {
                $status = 'completed';
                $result->completion = 100;
            } else if ($criteriacomplete || $ccompletion->timestarted) {
                $status = 'inprogress';
            } else {
                $status = 'notyetstarted';
            }

            $result->status = $status;
        }

        return $result;
    }

    public function course_instructions($course) {
        $output = '';

        $instructions = format_institutes_get_course_instructions($course);
        $context = context_course::instance($course->id);

        if (count($instructions)){
            $output .= html_writer::start_tag('div', array('class' => 'course-instruction-box'));
            $output .= html_writer::tag('div', get_string('instructionsdescription', 'format_institutes'), array('class' => 'course-instruction-info'));

            $output .= html_writer::start_tag('ul');
                foreach ($instructions as $instruction){
                    $output .= html_writer::start_tag('li');
                        $output .= html_writer::link("javascript:void(0)", $instruction->title, array('onclick'=>'instructionsPopupOpen('.$instruction->id.')'));
                        // popup box
                        $output .= html_writer::start_tag('div', array('id'=>'instruction_'.$instruction->id, 'class'=>'instructions-popup-box'));
                            $output .= html_writer::start_tag('div', array('class' => 'course-notifications-title'));
                                $output .=  html_writer::tag('i', '', array('class' => 'ion-close course-notifications-close', 'title'=>get_string('close', 'format_institutes'), 'onclick'=>'instructionsPopupClose();'));
                            $output .= $instruction->title;
                            $output .= html_writer::end_tag('div');
                            $output .= html_writer::tag('div', $instruction->message, array('class' => 'course-notifications-message'));
                            if ($instruction->attention){
                                $output .= html_writer::start_tag('div', array('class' => 'course-notifications-attention'));
                                    $output .= html_writer::tag('span', '', array('class' => 'icon-alert'));
                                    $output .= $instruction->attention;
                                $output .= html_writer::end_tag('div');
                            }
                            $output .= html_writer::start_tag('div', array('id'=>'instruction_'.$instruction->id, 'class'=>'instructions-popup-buttons'));
                                $output .= html_writer::link("javascript:void(0)", get_string('cancel'), array('onclick'=>'instructionsPopupClose()', 'class'=>'btn btn-cancel'));

                                $fs = get_file_storage();
                                $files = $fs->get_area_files($context->id, 'format_institutes', 'instructionfile', $instruction->id);
                                if (count($files) > 0) {
                                    foreach ($files as $file){
                                        if ($file->get_filename() == '.') continue;

                                        $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $file->get_filename());

                                        $output .= html_writer::link($url, get_string('viewpdf', 'format_institutes'), array('onclick'=>'instructionsPopupClose()', 'class'=>'btn', 'target'=>'_blank'));
                                    }
                                }

                            $output .=  html_writer::end_tag('div');
                        $output .=  html_writer::end_tag('div');
                    $output .= html_writer::end_tag('li');
                }
            $output .=  html_writer::end_tag('ul');
            $output .=  html_writer::end_tag('div');
        }

        return $output;
    }

    protected function section_header($section, $course, $onsectionpage, $sectionreturn=null, $isactivities = 0) {
        global $PAGE, $CFG, $DB;

        $o = '';
        $currenttext = ''; $sectionstyle = '';
        $section = $this->process_section_data($section);
        $generalsection = (!$PAGE->user_is_editing() and $section->sectiontype == FORMAT_SECTIONS_GENERAL) ? true : false;
        $sectionstyle .= ($section->sectiontype == FORMAT_SECTIONS_SEGMENT) ? ' open' : '';
        $sectionstyle .= ($generalsection) ? ' general-section' : '';

        if ($section->section > 0) {
            // Only in the non-general sections.
            if (!$section->visible) {
                $sectionstyle .= ' hidden';
            } else if (course_get_format($course)->is_section_current($section)) {
                $sectionstyle .= ' current';
            }
        }

        $slevel = (isset($this->_sections[$section->id]->level)) ? ' section-level-'.intval($this->_sections[$section->id]->level) : '';

        $params = array(
            'id' => 'section-'.$section->section,
            'class' => 'section main clearfix'.$sectionstyle.$slevel.' topic-view sectiontype-'.$this->_section_types[$section->sectiontype].((($section->coursetranscriptflag) ? ' section-transcript' : '')), 'role'=>'region',
            'aria-label'=> get_section_name($course, $section)
        );

        if ($section->section == $sectionreturn and $section->section > 0){
            $params['class'] .= ' current';
        }
        if (!$PAGE->user_is_editing() and !$generalsection){
            $section->progress = $this->get_section_completion($course, $section->section);
            $params['class'] .= ' '.$section->progress['status'];

            if (($section->level == 0 and $section->section != $sectionreturn)){
                $params['onclick'] = "location='".$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section->section."'";
            } elseif (($section->level == 0 and $section->section == $sectionreturn)){
                $params['class'] .= ' main-section';
            }
        }

        $o.= html_writer::start_tag('li', $params);

        // Create a span that contains the section title to be used to create the keyboard section move menu.
        $o .= html_writer::tag('span', get_section_name($course, $section), array('class' => 'hidden sectionname'));

        if (!$PAGE->user_is_editing()){
            $leftcontent = $this->section_left_content($section, $course, $onsectionpage);
            $o.= html_writer::tag('div', $leftcontent, array('class' => 'left side'));
        }

        $rightcontent = $this->section_right_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $rightcontent, array('class' => 'right side'));
        $o.= html_writer::start_tag('div', array('class' => 'content clearfix'));

        // When not on a section page, we display the section titles except the general section if null
        $hasnamenotsecpg = (!$onsectionpage && ($section->section != 0 || !is_null($section->name)));

        // When on a section page, we only display the general section title, if title is not the default one
        $hasnamesecpg = ($onsectionpage && ($section->section == 0 && !is_null($section->name)));

        $classes = ' accesshide';
        if ($hasnamenotsecpg || $hasnamesecpg) {
            $classes = '';
        }

        // segment section
        if (!$PAGE->user_is_editing() and $section->sectiontype == FORMAT_SECTIONS_SEGMENT){

            $o .= html_writer::start_tag('div', array('class' => 'segment-title'.(($sectionreturn == $section->section) ? ' segment-heading' : '')));
                $o .= html_writer::start_tag('div', array('class' => 'segment-toggler'));
                    $o .= html_writer::tag('span', '+', array('class'=>'show'));
                    $o .= html_writer::tag('span', '-', array('class'=>'hide'));
                $o .= html_writer::end_tag('div');
                $o .= get_section_name($course, $section);
            $o .= html_writer::end_tag('div');

        } elseif (!$PAGE->user_is_editing() and ($section->sectiontype == FORMAT_SECTIONS_MODULE or $section->sectiontype == FORMAT_SECTIONS_RESOURCESLINKS)){

            // section name
            $o .= html_writer::start_tag('div', array('class' => 'section-name-box'.((($section->coursetranscriptflag) ? ' section-transcript' : ''))));

            $o .= html_writer::tag('div', get_section_name($course, $section), array('class' => 'smodule-title'));

            $o .= html_writer::start_tag('div', array('class' => 'summary'));
            $o .= $this->format_summary_text($section);

            $o .= $this->courserenderer->course_section_cm_list_labels($course, $section, 0);

            $o .= html_writer::end_tag('div');

            $o .= html_writer::end_tag('div');

            $o .= html_writer::start_tag('div', array('class' => 'section-modules-box'));
            $o .= $this->courserenderer->course_section_cm_list($course, $section, 0, array('displayaudio'=>((isset($this->_root_parent->sectiontype) and $this->_root_parent->sectiontype == FORMAT_SECTIONS_RESOURCES) ? true : false)));
            $o .= html_writer::end_tag('div');

            $o .= html_writer::start_tag('div', array('class' => 'section-audio-box'));
            $o .= $this->course_section_audio($course, $section, 0);
            $o .= html_writer::end_tag('div');

        } else {
            if (!$PAGE->user_is_editing() and $section->level == 0) {
                $title = html_writer::tag('span', get_section_name($course, $section), array('class' => 'title'));
                $o .= $this->output->heading($title, 3);
            } elseif (!$PAGE->user_is_editing() and $section->level > 0) {
                $sectionname = html_writer::tag('span', get_section_name($course, $section));
                $o .= $this->output->heading($sectionname, 3, 'sectionname' . $classes);
            } elseif ($PAGE->user_is_editing() and $section->level == 2) {
                $sectionname = html_writer::tag('span', get_section_name($course, $section));
                $o .= $this->output->heading($sectionname, 3, 'sectionname' . $classes);
            } else {
                $sectionname = html_writer::tag('span', $this->section_title($section, $course, ''));
                $o .= $this->output->heading($sectionname, 3, 'sectionname' . $classes);
            }

            $o .= html_writer::start_tag('div', array('class' => 'summary'));
            $o .= $this->format_summary_text($section);
            $o .= html_writer::end_tag('div');

            $context = context_course::instance($course->id);
            $o .= $this->section_availability_message($section,
                    has_capability('moodle/course:viewhiddensections', $context));

            if (($section->level == 0 and !$sectionreturn) and !$PAGE->user_is_editing() and !$generalsection){
                $o .= $this->section_completion_status($course, $section);
            }
        }

        return $o;
    }

    public function course_section_audio($course, $section){
        global $USER, $CFG, $DB;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = array(); $resources = array();
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                if ($mod->modname != 'resource'){
                    continue;
                }

                $resources[$modnumber] = $mod;
            }
        }

        if (count($resources) > 0){
            foreach ($resources as $cm_resource){

                $resource = $DB->get_record('resource', array('id'=>$cm_resource->instance), '*', MUST_EXIST);
                $context = context_module::instance($cm_resource->id);
                $fs = get_file_storage();
                $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
                if (count($files) > 0) {
                    $file = reset($files);
                    unset($files);

                    if (stristr($file->get_mimetype(), 'audio')){
                        $path = '/'.$context->id.'/mod_resource/content/'.$resource->revision.$file->get_filepath().$file->get_filename();
                        $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, 1);

                        $output .= html_writer::link($fullurl, html_writer::tag('span', '', array('class' => 'audio-icon')).get_string('downloadaudio', 'format_institutes'), array('class'=>'course-section-audio', 'title'=>get_string('downloadaudio', 'format_institutes')));
                    }
                }
            }
        }

        return $output;
    }

    protected function get_sectionid($section) {
        $sectionid = 0;

        $level_sections = array();
        if (count($this->_sections)){
            foreach ($this->_sections as $ssection){
                $level_sections[$ssection->level][$ssection->section] = $ssection;
            }
        }

        if (count($level_sections[$section->level])){
            $i = 0;
            foreach ($level_sections[$section->level] as $ls){
                if ($ls->id == $section->id) {
                    $sectionid = $i;
                    break;
                }
                $i++;
            }
        }

        return $sectionid;
    }

    protected function section_completion_status($course, $section) {
        $o = '';

        $o .= html_writer::start_tag('div', array('class'=>'section-completion-status'));
            if ($section->level == 0 and $section->progress['status'] == 'notstarted'){
                $o .= html_writer::tag('label', get_string('notstarted', 'format_institutes'));
            } else {
                $o .= html_writer::tag('label', get_string('completed', 'format_institutes').':');
                $o .= $section->progress['progress'].'%';
            }
        $o .= html_writer::end_tag('div');

        return $o;
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false) {
        global $PAGE;

        if (!$PAGE->user_is_editing() or $section->section == 0) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $isstealth = $section->section > $course->numsections;

        $movingsection = course_get_format($course)->is_moving_section();
        $sr = course_get_format($course)->get_viewed_section();


        $controls = array();

        //if (!isset($this->_sections[$section->id]->level) or (isset($this->_sections[$section->id]->level) and $this->_sections[$section->id]->level != 3)){
            $editeurl = new moodle_url('/course/format/institutes/addsection.php', array("courseid"=>$course->id, "parent"=>$section->id));

            $controls['addsection'] = array('url' => $editeurl, "icon" => 'i/manual_item',
                                                   'name' => get_string('addsection', 'format_institutes'),
                                                   'pixattr' => array('class' => '', 'alt' => get_string('addsection', 'format_institutes')),
                                                   'attr' => array('class' => 'editing_addsection', 'title' => get_string('addsection', 'format_institutes')));
        //}


        if (!$isstealth && $section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marked',
                                               'name' => $highlightoff,
                                               'pixattr' => array('class' => '', 'alt' => $markedthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markedthistopic));




            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marker',
                                               'name' => $highlight,
                                               'pixattr' => array('class' => '', 'alt' => $markthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markthistopic));
            }
        }

        if (!isset($this->_sections[$section->id]->level) or (isset($this->_sections[$section->id]->level) and $this->_sections[$section->id]->level == 0)){
            $editeurl = new moodle_url('/course/format/institutes/copysection.php', array("courseid"=>$course->id, "sectionid"=>$section->id));

            $controls['copysection'] = array('url' => $editeurl, "icon" => 't/copy',
                                                   'name' => get_string('copysection', 'format_institutes'),
                                                   'pixattr' => array('class' => '', 'alt' => get_string('copysection', 'format_institutes')),
                                                   'attr' => array('class' => 'editing_copysection', 'title' => get_string('copysection', 'format_institutes')));
        }

        // Move section control
        if (!$movingsection && has_capability('moodle/course:update', $coursecontext) && $section->section != $sr) {
            $strmovesection = new lang_string('move');
            $moveurl = course_get_url($course, $section->section, array('sr' => $sr));
            $moveurl->params(array('moving' => $section->section, 'sesskey' => sesskey()));

            $controls['move'] = array('url' => $moveurl, "icon" => 't/move',
                                       'name' => $strmovesection,
                                       'pixattr' => array('class' => '', 'alt' => $strmovesection),
                                       'attr' => array('class' => 'editing_movesection', 'title' => $strmovesection));
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        if (array_key_exists("moveup", $parentcontrols)) {
            unset($parentcontrols['moveup']);
        }
        if (array_key_exists("movedown", $parentcontrols)) {
            unset($parentcontrols['movedown']);
        }

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }


    /**
     * Displays the target div for moving section (in 'moving' mode only)
     *
     * @param int|stdClass $courseorid current course
     * @param int|section_info $parent new parent section
     * @param null|int|section_info $before number of section before which we want to insert (or null if in the end)
     */
    protected function display_insert_section_here($courseorid, $parent, $before = null) {
        if ($control = course_get_format($courseorid)->get_edit_control_movehere($parent, $before, $this)) {
            echo $control;
        }
    }

    public function course_print_coursefile($course)
    {
        global $PAGE, $CFG;

        require_once($CFG->dirroot . '/filter/mediaplugin/filter.php');
        $filterplugin = new filter_mediaplugin(null, array());

        $context = context_course::instance($course->id);
        $contentimages = '';
        if ($course->infotext) {
            $contentimages .= html_writer::tag('div', $course->infotext, array('class' => 'course-file-box embed'));
        } else {
            $fs = get_file_storage();
            $image_files = array();
            $imgfiles = $fs->get_area_files($context->id, 'format_institutes', 'thumbnail', $course->id);
            $j = 1;
            foreach ($imgfiles as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                $itemid = $course->id . "_" . $j++;
                if ($filename == '.' or !$filetype) continue;

                if (stripos($filetype, 'video') !== FALSE) {
                    $url = moodle_url::make_pluginfile_url($context->id, 'format_institutes', 'thumbnail', $course->id, '/', $filename);
                    $contentimages .= $filterplugin->filter('<div class="course-info-playerbox" id="' . $itemid . '"><a href="' . $url . '">' . $filename . '</a></div>');
                } else if (stripos($filetype, 'image') !== FALSE) {
                    $url = moodle_url::make_pluginfile_url($context->id, 'format_institutes', 'thumbnail', $course->id, '/', $filename);
                    $contentimages .= html_writer::empty_tag('img', array('src' => $url->out()));
                }
            }

            if (!empty($contentimages)) {
                $contentimages = html_writer::tag('div', $contentimages, array('class' => 'course-file-box'));
            }
        }

        return $contentimages;

    }

    public function get_parent_section($course, $sectionid){
        global $PAGE, $USER, $DB;
        $section = null;

        if (isset($this->_sections[$sectionid]) and $this->_sections[$sectionid]->level > 0 and $this->_sections[$sectionid]->parent){
            $parentid = $this->_sections[$sectionid]->parent;
            if (isset($this->_sections[$parentid])){
                $section = $this->_sections[$parentid];
            }
        } elseif (isset($this->_sections[$sectionid]) and $this->_sections[$sectionid]->level == 0){
            $section = $this->_sections[$sectionid];
        }

        return $section;
    }

    public function get_section_number($course, $sectionid){
        global $PAGE, $USER, $DB;
        $snumber = 1; $sections = array();

        if (isset($this->_sections[$sectionid]) and $this->_sections[$sectionid]->parent > 0){
            foreach ($this->_sections as $section){
                if ($section->parent == $this->_sections[$sectionid]->parent){
                    $sections[$section->id] = $snumber++;
                }
            }
        }

        return (isset($sections[$sectionid])) ? $sections[$sectionid] : $snumber;
    }

    protected function render_format_institutes_course_content_header(format_institutes_course_content_header $a) {
        global $PAGE, $USER, $DB;

        $o = '';
        $currentsection = null;
        $course = $PAGE->course;

        if ($PAGE->user_is_editing() or !isset($PAGE->cm->id)) return $o;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();

        $menu_settings = $DB->get_records_menu('course_format_settings',array('courseid'=>$course->id, 'type'=>'menu'),'name','name,value');

        if (isset($menu_settings['glossary']) and $menu_settings['glossary'] == $PAGE->cm->id) {
            return $o;
        } elseif (isset($menu_settings['faq']) and $menu_settings['faq'] == $PAGE->cm->id){
            return $o;
        }

        $section = $this->_sections[$this->page->cm->section];

        if (isset($section->id)){
            $o .= html_writer::tag('h2', get_section_name($course, $section), array('class' => 'section-title'));
        }

        return $o;
    }

    protected function render_format_institutes_course_content_footer(format_institutes_course_content_footer $a) {
        global $PAGE, $USER, $DB, $CFG;

        $o = '';
        $status = 'notstarted'; $type = '';
        $course = $PAGE->course;

        if ($PAGE->user_is_editing()) return $o;
        if ($this->page->course->id == 1) return $o;

        $modinfo = get_fast_modinfo($course);
        $course = course_get_format($course)->get_course();
        $completioninfo = new completion_info($course);
        $displaysection = optional_param('section', 0, PARAM_INT);

        if (isset($PAGE->cm->id)){
            $mod = $modinfo->cms[$PAGE->cm->id];

            $displaysection = (isset($this->_sections[$mod->section])) ? $this->_sections[$mod->section] : 0;
            $parentsection = $this->get_parent_section($course, $displaysection->id);
            if (isset($displaysection->id)){
                $rootparent = $this->get_section_root_parent($course, $modinfo, $displaysection);
            }

            $activitynavlinks = $this->get_activity_nav_links($course, $mod);
            if ($parentsection->sectiontype != FORMAT_SECTIONS_RESOURCES){
                $sectionnavlinks = $this->get_nav_links($course, $modinfo->get_section_info_all(), $displaysection->section);
            }

            $o .= html_writer::start_tag('div', array('class' => 'course-content-footer-navbuttons segment-sections-navigation clearfix'));

                if (isset($rootparent->sectiontype) and $rootparent->sectiontype == FORMAT_SECTIONS_RESOURCES){
                    $o .= html_writer::start_tag('div', array('class' => 'segment-back-buttons'));

                    // back to resources
                    $backtext = get_string('backtoresources', 'format_institutes');
                    $backlink = html_writer::link(new moodle_url('/course/format/institutes/resources.php', array('id' => $course->id)), $backtext);
                    $o .= html_writer::tag('span', $backlink, array('class' => 'btn btn-success prev-button'));

                    // back to assignments
                    $assignmentsection = $this->get_section_root_by_type($course, $modinfo, 0, true);
                    if (isset($assignmentsection->section)){
                        $backtext = get_string('backtoassignments', 'format_institutes');
                        $backlink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id, 'section'=>$assignmentsection->section)), $backtext);
                        $o .= html_writer::tag('span', $backlink, array('class' => 'btn btn-success prev-button'));
                    }

                    $o .= html_writer::end_tag('div');
                }


                if (!empty($activitynavlinks['previous'])){
                    $o .= html_writer::tag('span', $activitynavlinks['previous'], array('class' => 'btn prev-button'));
                } elseif (isset($sectionnavlinks['previous']) and !empty($sectionnavlinks['previous'])){
                    $o .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'btn prev-button'));
                }
                if (!empty($activitynavlinks['next'])){
                    $o .= html_writer::tag('span', $activitynavlinks['next'], array('class' => 'btn next-button'));
                } elseif (isset($sectionnavlinks['next']) and !empty($sectionnavlinks['next'])){
                    $o .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'btn next-button'));
                }
            $o .= html_writer::end_tag('div');

        } elseif ($displaysection > 0) {

            $currentsection = $modinfo->get_section_info($displaysection);
            $segmentsection = $this->get_section_parent_by_type($course, $modinfo, $currentsection, 5);
            $rootparent = $this->get_section_root_parent($course, $modinfo, $currentsection);

            if (isset($segmentsection->id) or $currentsection->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS or $currentsection->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES){

                if (!isset($segmentsection->id)){
                    $segmentsection = new stdClass();
                    $segmentsection->id = 0;
                }

                $sectionnavlinks = $this->get_subsections_nav_links($course, $modinfo->get_section_info_all(), $currentsection, $segmentsection);

                $o .= html_writer::start_tag('div', array('class' => 'course-content-footer-navbuttons segment-sections-navigation clearfix'));

                    if ($rootparent->sectiontype == FORMAT_SECTIONS_RESOURCES){
                        $o .= html_writer::start_tag('div', array('class' => 'segment-back-buttons'));

                        // back to resources
                        $backtext = get_string('backtoresources', 'format_institutes');
                        $backlink = html_writer::link(new moodle_url('/course/format/institutes/resources.php', array('id' => $course->id)), $backtext);
                        $o .= html_writer::tag('span', $backlink, array('class' => 'btn btn-success prev-button'));

                        // back to assignments
                        $assignmentsection = $this->get_section_root_by_type($course, $modinfo, 0, true);
                        if (isset($assignmentsection->section)){
                            $backtext = get_string('backtoassignments', 'format_institutes');
                            $backlink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id, 'section'=>$assignmentsection->section)), $backtext);
                            $o .= html_writer::tag('span', $backlink, array('class' => 'btn btn-success prev-button'));
                        }

                        $o .= html_writer::end_tag('div');
                    }

                    if (!empty($sectionnavlinks['previous'])){
                        $o .= html_writer::tag('span', $sectionnavlinks['previous'], array('class' => 'btn prev-button'));
                    }
                    if (!empty($sectionnavlinks['next'])){
                        $o .= html_writer::tag('span', $sectionnavlinks['next'], array('class' => 'btn next-button'));
                    }
                $o .= html_writer::end_tag('div');
            }
        } elseif (strstr($PAGE->url, '/course/format/institutes/resources.php')){

            // back to assignments
            $assignmentsection = $this->get_section_root_by_type($course, $modinfo, 0, true);
            if (isset($assignmentsection->section)){
                $o .= html_writer::start_tag('div', array('class' => 'course-content-footer-navbuttons segment-sections-navigation clearfix'));
                $o .= html_writer::start_tag('div', array('class' => 'segment-back-buttons resources-buttons'));
                $backtext = get_string('backtoassignments', 'format_institutes');
                $backlink = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id, 'section'=>$assignmentsection->section)), $backtext);
                $o .= html_writer::tag('span', $backlink, array('class' => 'btn btn-success prev-button'));
                $o .= html_writer::end_tag('div');
                $o .= html_writer::end_tag('div');
            }
        }

        return $o;
    }



    /**
     * Generate next/previous section links for naviation
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections The course_sections entries from the DB
     * @param int $sectionno The section number in the coruse which is being dsiplayed
     * @return array associative array with previous and next section link
     */
    protected function get_nav_links($course, $sections, $sectionno) {
        // FIXME: This is really evil and should by using the navigation API.
        $course = course_get_format($course)->get_course();
        $modinfo = get_fast_modinfo($course);

        $currentsection = $modinfo->get_section_info($sectionno);
        $parentsection = $this->get_section_root_parent($course, $modinfo, $currentsection);

        $canviewhidden = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id))
            or !$course->hiddensections;

        $links = array('previous' => '', 'next' => '');
        $back = $sectionno - 1;
        if ($back > 1){
            while ($back > 0 and empty($links['previous'])) {
                $sections[$back] = $this->process_section_data($sections[$back]);

                if (($canviewhidden || $sections[$back]->uservisible) and $sections[$back]->level == 0 and $sections[$back]->sectiontype != FORMAT_SECTIONS_GENERAL and $sections[$back]->sectiontype != FORMAT_SECTIONS_RESOURCES and $this->can_view_section($sections[$back]) and $parentsection->id != $sections[$back]->id) {
                    $params = array();
                    if (!$sections[$back]->visible) {
                        $params = array('class' => 'dimmed_text');
                    }
                    $previouslink = html_writer::tag('span', $this->output->larrow(), array('class' => 'larrow'));
                    $previouslink .= get_string('previous');
                    $links['previous'] = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id,
                                          'section' => $back)), $previouslink, $params);
                }
                $back--;
            }
        }

        $forward = $sectionno + 1;
        while ($forward <= $course->numsections and empty($links['next'])) {
            $sections[$forward] = $this->process_section_data($sections[$forward]);

            if (($canviewhidden || $sections[$forward]->uservisible) and $sections[$forward]->level == 0 and $sections[$forward]->sectiontype != FORMAT_SECTIONS_GENERAL and $sections[$forward]->sectiontype != FORMAT_SECTIONS_RESOURCES and $this->can_view_section($sections[$forward])) {
                $params = array();
                if (!$sections[$forward]->visible) {
                    $params = array('class' => 'dimmed_text');
                }
                $nextlink = get_string('next');
                $nextlink .= html_writer::tag('span', $this->output->rarrow(), array('class' => 'rarrow'));
                $links['next'] = html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id,
                                      'section' => $forward)), $nextlink, $params);
            }
            $forward++;
        }

        return $links;
    }



    /**
     * Generate next/previous section links for naviation
     *
     * @param stdClass $course The course entry from DB
     * @param array $sections The course_sections entries from the DB
     * @param int $sectionno The section number in the coruse which is being dsiplayed
     * @return array associative array with previous and next section link
     */
    protected function get_subsections_nav_links($course, $sections, $currentsection, $segmentsection) {
        // FIXME: This is really evil and should by using the navigation API.
        $course = course_get_format($course)->get_course();
        $modinfo = get_fast_modinfo($course);

        $parentsection = $this->get_section_root_parent($course, $modinfo, $currentsection);
        $sectionurl = (isset($parentsection->sectiontype) and $parentsection->sectiontype == FORMAT_SECTIONS_RESOURCES) ? '/course/format/institutes/resources.php' : '/course/view.php';

        $canviewhidden = has_capability('moodle/course:viewhiddensections', context_course::instance($course->id))
            or !$course->hiddensections;

        $links = array('previous' => '', 'next' => '');
        $back = $currentsection->section - 1;

        if ($back > 1){
            $parent = 0;
            while ($back > 0 and empty($links['previous'])) {
                $sections[$back] = $this->process_section_data($sections[$back]);
                $sectionroot = $this->get_section_root_parent($course, $modinfo, $sections[$back]);

                if (isset($this->_sections[$sections[$back]->parent])){
                    $parent = $this->_sections[$sections[$back]->parent];
                }

                if (($canviewhidden || $sections[$back]->uservisible) and ($sections[$back]->parent == $segmentsection->id or $sections[$back]->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS or $sections[$back]->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES or (isset($parent->sectiontype) and $parent->sectiontype == FORMAT_SECTIONS_SEGMENT)) and $this->can_view_section($sections[$back]) and $sectionroot->id == $parentsection->id) {
                    $params = array();
                    if (!$sections[$back]->visible) {
                        $params = array('class' => 'dimmed_text');
                    }
                    $previouslink = html_writer::tag('span', $this->output->larrow(), array('class' => 'larrow'));
                    $previouslink .= get_string('previous');
                    $links['previous'] = html_writer::link(new moodle_url($sectionurl, array('id' => $course->id,
                                          'section' => $back)), $previouslink, $params);
                }
                $back--;
            }
        }

        $forward = $currentsection->section + 1;
        $parent = 0;
        while ($forward <= $course->numsections and empty($links['next'])) {
            $sections[$forward] = $this->process_section_data($sections[$forward]);
            $sectionroot = $this->get_section_root_parent($course, $modinfo, $sections[$forward]);

            if (isset($this->_sections[$sections[$forward]->parent])){
                $parent = $this->_sections[$sections[$forward]->parent];
            }

            if (($canviewhidden || $sections[$forward]->uservisible) and ($sections[$forward]->parent == $segmentsection->id or $sections[$forward]->sectiontype == FORMAT_SECTIONS_ASSIGNMENTS or $sections[$forward]->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES or (isset($parent->sectiontype) and $parent->sectiontype == FORMAT_SECTIONS_SEGMENT)) and $this->can_view_section($sections[$forward]) and $sectionroot->id == $parentsection->id) {
                $params = array();
                if (!$sections[$forward]->visible) {
                    $params = array('class' => 'dimmed_text');
                }
                $nextlink = get_string('next');
                $nextlink .= html_writer::tag('span', $this->output->rarrow(), array('class' => 'rarrow'));
                $links['next'] = html_writer::link(new moodle_url($sectionurl, array('id' => $course->id,
                                      'section' => $forward)), $nextlink, $params);
            }
            $forward++;
        }

        return $links;
    }

    protected function has_nav_mod($mod) {
        if ((!$mod->uservisible || !$mod->has_view()) && !has_capability('moodle/course:viewhiddenactivities', $mod->context)) {
            return false;
        }

        if($mod->modname == 'label') {
            return false;
        }

        if(!$mod->url) {
            return false;
        }

        return true;
    }

    protected function get_activity_nav_links($course, $cm) {
        $course = course_get_format($course)->get_course();
        $modinfo = get_fast_modinfo($course);

        $links = array(
            'previous' => '',
            'next' => ''
        );

        $result = array(
            'firstmodules' => false,
            'prev_mod' => 0,
            'next_mod' => 0,
            'break_prev' => false,
            'break_next' => false
        );

        $currentsection = $this->_sections[$cm->section];
        $parentsection = $this->get_section_root_parent($course, $modinfo, $currentsection);

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            $thissection = $this->process_section_data($thissection);
            if (!$thissection->visible or $section == 0 or $thissection->sectiontype == FORMAT_SECTIONS_GENERAL or !$this->can_view_section($thissection)) continue;

            if (!$this->can_view_section($thissection)) {
                continue;
            }

            $thisparentsection = $this->get_section_root_parent($course, $modinfo, $thissection);

            if ($parentsection->id != $thisparentsection->id){
                continue;
            }
            if ($parentsection->sectiontype == FORMAT_SECTIONS_RESOURCES and $thisparentsection->sectiontype != FORMAT_SECTIONS_RESOURCES){
                continue;
            } elseif ($parentsection->sectiontype != FORMAT_SECTIONS_RESOURCES and $thisparentsection->sectiontype == FORMAT_SECTIONS_RESOURCES){
                continue;
            }

            if (isset($modinfo->sections[$thissection->section])) {
                foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];

                    if(!$this->has_nav_mod($mod) or $section == 0) {
                        continue;
                    }

                    // check if it is audio file and ignore
                    if ($mod->modname == 'resource'){
                        $context = context_module::instance($mod->id);
                        $fs = get_file_storage();
                        $files = $fs->get_area_files($context->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
                        if (count($files) > 0) {
                            $file = reset($files);
                            unset($files);
                            if (stristr($file->get_mimetype(), 'audio') and $thisparentsection->sectiontype != FORMAT_SECTIONS_RESOURCES){
                                continue;
                            }
                        }

                        // check if current section is transcript section and show/hide secured trascript file
                        if ($thissection->coursetranscriptflag){
                            $issecurepdf = (strpos($file->get_filename(), '_s.pdf') !== false);
                            if ($issecurepdf and !$this->_requirements['issecurepdf']){
                                continue;
                            }
                        }
                    }

                    if ($cm->id == $mod->id) {
                        if (!$result['break_prev']) {
                            $result['firstmodules'] = true;
                        }
                        $result['break_prev'] = true;
                        $result['break_next'] = true;
                    } else {
                        if (!$result['break_prev']) {
                            $result['prev_mod'] = $mod;
                        }
                        if ($result['break_next']) {
                            $result['next_mod'] = $mod;
                            break;
                        }
                    }
                }
            }

            if (($result['prev_mod'] || $result['firstmodules']) && $result['next_mod']){
                break;
            }
        }

        if($result['prev_mod']) {
            $onclick = htmlspecialchars_decode($result['prev_mod']->onclick, ENT_QUOTES);
            $links['previous'] = html_writer::link($result['prev_mod']->url, get_string('previous'), [
                'onclick' => $onclick
            ]);
        }

        if($result['next_mod']) {
            $onclick = htmlspecialchars_decode($result['next_mod']->onclick, ENT_QUOTES);
            $links['next'] = html_writer::link($result['next_mod']->url, get_string('next'), [
                'onclick' => $onclick
            ]);
        }
        return $links;
    }

    public function get_sections_sequense($course, $modinfo, $rootsection = 0) {

        if ($rootsection > 0){
            $parentsection = $modinfo->get_section_info($rootsection);
        }

        $sectionlist = array();

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if($section == 0){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                continue;
            }

            $thissection = $this->process_section_data($thissection);

            $sectionlist[$thissection->parent]['childs'][$thissection->id] = $thissection;
        }

        return $sectionlist;
    }

    public function get_root_sections($course, $modinfo, $type = 0){

        $sectionlist = array();

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            $thissection = $this->process_section_data($thissection);
            if($section == 0){
                continue;
            }

            if (!$this->can_view_section($thissection)){
                continue;
            }

            // Show the section if the user is permitted to access it, OR if it's not available
            // but there is some available info text which explains the reason & should display.
            $showsection = $thissection->uservisible ||
                    ($thissection->visible && !$thissection->available &&
                    !empty($thissection->availableinfo));
            if (!$showsection) {
                continue;
            }

            if ($thissection->level == 0){
                if ($type > 0 and $thissection->sectiontype == $type){
                    $sectionlist[$thissection->id] = $thissection;
                } elseif ($type == 0){
                    $sectionlist[$thissection->id] = $thissection;
                }
            }
        }

        return $sectionlist;
    }

    public function get_section_root_parent($course, $modinfo, $section){

        $section = $this->process_section_data($section);

        if (!empty($section->parentssequence)){
            $parentssequence = explode(',', $section->parentssequence);
            $parent = reset($parentssequence);

            // Now the list of sections..
            foreach ($modinfo->get_section_info_all() as $s => $thissection) {
                $thissection = $this->process_section_data($thissection);
                if($s == 0){
                    continue;
                }

                if ($thissection->level == 0 and $thissection->id == $parent){
                    $section = $thissection;
                }
            }
        }

        $this->_root_parent = $section;
        return $section;
    }

    public function get_section_parent_by_type($course, $modinfo, $section, $sectiontype = 0){

        $section = $this->process_section_data($section);
        $parentsection = null;

        if (!empty($section->parentssequence)){
            $parentssequence = explode(',', $section->parentssequence);

            // Now the list of sections..
            foreach ($modinfo->get_section_info_all() as $s => $thissection) {
                $thissection = $this->process_section_data($thissection);
                if($s == 0){
                    continue;
                }

                if ($thissection->sectiontype == $sectiontype and in_array($thissection->id, $parentssequence)){
                    $parentsection = $thissection;
                    break;
                }
            }
        }

        return $parentsection;
    }

    public function get_section_root_by_type($course, $modinfo, $sectiontype = 0, $root = false){

        $section = null;

        // Now the list of sections..
        foreach ($modinfo->get_section_info_all() as $s => $thissection) {
            $thissection = $this->process_section_data($thissection);
            if($s == 0){
                continue;
            }
            if (($root and $thissection->level == 0) or !$root){
                if ($thissection->sectiontype == $sectiontype){
                    $section = $thissection;
                    break;
                }
            }
        }
        return $section;
    }

    public function process_section_data($thissection){

        $thissection->parent = (isset($this->_sections[$thissection->id]->parent)) ? $this->_sections[$thissection->id]->parent : 0;
        $thissection->level = (isset($this->_sections[$thissection->id]->level)) ? $this->_sections[$thissection->id]->level : 0;
        $thissection->parentssequence = (isset($this->_sections[$thissection->id]->parentssequence)) ? $this->_sections[$thissection->id]->parentssequence : '';
        $thissection->sectiontype = (isset($this->_sections[$thissection->id]->sectiontype)) ? $this->_sections[$thissection->id]->sectiontype : 0;
        $thissection->coursetranscriptflag = (isset($this->_sections[$thissection->id]->coursetranscriptflag)) ? $this->_sections[$thissection->id]->coursetranscriptflag : 0;
        $thissection->state = (isset($this->_sections[$thissection->id]->state)) ? $this->_sections[$thissection->id]->state : '';

        return $thissection;
    }


    // process institutes requirements for any section
    public function can_view_section($thissection){

        $can_view = 1;

        // if current section is transcript section
        if ($thissection->coursetranscriptflag and !$this->_requirements['transcriptshown']){
            $can_view = 0;
        }

        // check user states
        if (isset($thissection->state) && !empty($thissection->state) && !in_array($thissection->state, $this->_states)){
            $can_view = 0;
        }

        // display for user who has permissions to see hidden sections
        if (!$can_view && $this->_canviewhiddensections) {
            $can_view = 2;
        }

        return $can_view;
    }

}
