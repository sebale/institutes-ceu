<?php

/**
 * Upgrade code containing changes to the plugin data table.
 *
 * @package    format_institutes
 * @copyright  2017 The Institutes
 */

$observers = array(

    array(
        'eventname' => 'core\event\course_deleted',
        'callback'  => 'format_institutes_observer::institutes_course_deleted',
    ),
    array(
        'eventname' => 'core\event\course_module_deleted',
        'callback'  => 'format_institutes_observer::institutes_course_module_deleted',
    ),
);
