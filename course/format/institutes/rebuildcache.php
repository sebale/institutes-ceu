<?php

/**
 *
 * @package institutes
 * @copyright  2017 The Institutes
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/course/lib.php');

$courseid = required_param('courseid', PARAM_INT);
$parent = optional_param('parent', 0, PARAM_INT);
$sr = optional_param('sr', 0, PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$courseformatoptions = course_get_format($course)->get_format_options();

$PAGE->set_url('/course/format/institutes/rebuildcache.php', array('courseid' => $courseid));

require_login($course);
require_capability('moodle/course:update', context_course::instance($course->id));

$format_renderer = $PAGE->get_renderer('format_institutes');

$modinfo = get_fast_modinfo($course);
course_get_format($course)->sort_root_sections($course, $modinfo, $format_renderer);

rebuild_course_cache($course->id, true);

if ($parent > 0){
    $url = new moodle_url('/course/editsection.php', array("id"=>$id, "parent"=>$parent));
} else {
    $url = new moodle_url('/course/editsection.php', array("id"=>$id));
}

redirect($url);
