<?php

/**
 * Version details
 *
 * @package    format
 * @subpackage institutes
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 20170304000;        // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires  = 2016051900;        // Requires this Moodle version.
$plugin->component = 'format_institutes';    // Full name of the plugin (used for diagnostics).
