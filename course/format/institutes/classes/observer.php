<?php

defined('MOODLE_INTERNAL') || die();

/**
 * Event handler for category enrolment plugin.
 *
 * We try to keep everything in sync via listening to events,
 * it may fail sometimes, so we always do a full sync in cron too.
 */
class format_institutes_observer {

   /**
     * Triggered when 'course_deleted' event is triggered.
     *
     * @param core\event\core\event\course_deleted $event
     */
    public static function institutes_course_deleted(core\event\course_deleted $event) {
        global $DB;

        $courseid = $event->objectid;

        $DB->delete_records('course_format_sections', array('courseid'=>$courseid));
        $DB->delete_records('course_format_settings', array('courseid'=>$courseid));
        $DB->delete_records('course_format_notes', array('courseid'=>$courseid));
    }

    /**
     * Triggered when 'course_module_completion_updated' event is triggered.
     *
     * @param \core\event\course_module_completion_updated $event
     */
    public static function institutes_course_module_deleted(core\event\course_module_deleted $event) {
        global $DB;

        $modid = $event->objectid;
        $cmid = $event->contextinstanceid;

        $DB->delete_records('course_format_settings', array('value'=>$cmid, 'type'=>'menu'));
    }

}
