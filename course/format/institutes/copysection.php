<?php

/**
 *
 * @package core_course
  * @copyright  2017 The Institutes
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/course/lib.php');

$courseid = required_param('courseid', PARAM_INT);
$sectionid = required_param('sectionid', PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$section = $DB->get_record('course_format_sections', array('sectionid' => $sectionid, 'courseid'=>$course->id, 'format'=>'institutes'));
$courseformatoptions = course_get_format($course)->get_format_options();
$index = $courseformatoptions['numsections'] + 1;
$PAGE->set_url('/course/format/institutes/copysection.php', array('courseid' => $courseid));

require_login($course);
require_capability('moodle/course:update', context_course::instance($course->id));

if ($sectionid > 0){
    $currentsection = $DB->get_record('course_sections', array('id'=>$sectionid));
    $format_section = $DB->get_record_sql(
        "SELECT s.*, fs.parent, fs.level, fs.parentssequence, fs.sectiontype, cft.value as coursetranscriptflag, cfs.value as state
                 FROM {course_sections} s
            LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course AND fs.format = :format1
            LEFT JOIN {course_format_options} cft ON cft.courseid = s.course AND cft.sectionid = s.id AND cft.format = :format2 AND cft.name = :coursetranscriptflag
            LEFT JOIN {course_format_options} cfs ON cfs.courseid = s.course AND cfs.sectionid = s.id AND cfs.format = :format3 AND cfs.name = :state
                WHERE s.course = :course AND s.id = :sectionid
                ORDER BY s.section",
        array('sectionid'=>$sectionid, 'format1'=>'institutes', 'format2'=>'institutes', 'format3'=>'institutes', 'course'=>$course->id, 'coursetranscriptflag'=>'coursetranscriptflag', 'state'=>'state'));
}

$format_renderer = $PAGE->get_renderer('format_institutes');
$modinfo = get_fast_modinfo($course);
$sections_sequense = $format_renderer->get_sections_sequense($course, $modinfo, $currentsection->id);

// create root section
update_course((object)array('id' => $course->id, 'numsections' => $index));

$newsection = new stdClass();
$newsection->course = $courseid;
$newsection->section = $index;
$newsection->name = (!empty($currentsection->name)) ? $currentsection->name : get_string('sectionname', 'format_institutes').' '.$index;
$newsection->visible = 1;
$newsection->summary = '';
$newsection->summaryformat = 1;
$newsection->sequence = '';
$newsection->id = $DB->insert_record('course_sections', $newsection);

$params = new stdClass();
$params->parent = 0;
$params->parentssequence = '';
$params->level = 0;
$params->timemodified = time();
$params->sectiontype = (isset($format_section->sectiontype)) ? $format_section->sectiontype : 0;
$params->coursetranscriptflag = (isset($format_section->coursetranscriptflag)) ? $format_section->coursetranscriptflag : 0;
$params->state = (isset($format_section->state)) ? $format_section->state : '';

$format_options = array();
$format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$newsection->id, 'name'=>'parent', 'value'=>'0');
$format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$newsection->id, 'name'=>'sectiontype', 'value'=>$params->sectiontype);
$format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$newsection->id, 'name'=>'coursetranscriptflag', 'value'=>$params->coursetranscriptflag);
$format_options[] = array('courseid'=>$course->id, 'format'=>$course->format, 'sectionid'=>$newsection->id, 'name'=>'state', 'value'=>$params->state);

foreach ($format_options as $foption){
    $DB->insert_record('course_format_options', $foption);
}

course_get_format($course)->course_save_format_section($section, $params);

 // ROOT LEVEL
if (isset($sections_sequense[$currentsection->id]['childs'])){

    if (isset($sections_sequense[$currentsection->id]['childs']) and count($sections_sequense[$currentsection->id]['childs'])){
        foreach($sections_sequense[$currentsection->id]['childs'] as $section){

            $index += 1;
            $params->sectionid = $newsection->id;
            $params->section = $newsection->section;
            $section = $format_renderer->process_section_data($section);
            $parent = course_get_format($course)->course_create_format_section($index, $section, $params);

            $index = format_institutes_process_copy_section($course, $sections_sequense, $format_renderer, $section, $parent, $index);
        }
    }
}

$modinfo = get_fast_modinfo($course);
course_get_format($course)->sort_root_sections($course, $modinfo, $format_renderer);
rebuild_course_cache($courseid, true);

redirect(new moodle_url('/course/view.php', array("id"=>$course->id)));
