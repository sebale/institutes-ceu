<?php

/**
 * institutes version file.
 *
 * @package    format_institutes
 * @copyright  2017 The Institutes
 */


require('../../../config.php');
require_once('lib.php');

$systemcontext   = context_system::instance();

$id             = required_param('id', PARAM_INT); // Option id.
$displaysection = optional_param('section', 0, PARAM_INT);

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

// prepare course and format
$format = course_get_format($course);
$course = $format->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));
$format->course_create_format_sections_if_missing();
$modinfo = get_fast_modinfo($course);

$fs = get_file_storage();

$title = get_string('resources', 'format_institutes');

$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/institutes/resources.php', $pageparams);
if (!$displaysection){
    $PAGE->navbar->add($title, new moodle_url('/course/format/institutes/resources.php', $pageparams));
}

$PAGE->set_title($title);
$PAGE->set_heading($title);
$USER->editing = 0;
if ($PAGE->user_allowed_editing()) {
    $buttons = $OUTPUT->edit_button(course_get_url($course));
    $PAGE->set_button($buttons);
}

$renderer = $PAGE->get_renderer('format_institutes');
$resources_sections = $renderer->get_root_sections($course, $modinfo, 6);

echo $OUTPUT->header();
echo html_writer::start_tag('div', array('class' => 'ceu-resources course-content'));

echo html_writer::start_tag('div', array('class' => 'course-level-3'.(($displaysection > 0) ? ' inner-page' : ' main-page')));

if ($displaysection > 0){
    $currentsection = $DB->get_record('course_sections', array('course'=>$course->id, 'section'=>$displaysection));
    $currentsection = $renderer->process_section_data($currentsection);

    if ($currentsection->sectiontype == FORMAT_SECTIONS_PAGEACTIVITIES){
        echo html_writer::start_tag('div', array('class' => 'course-level-activities'));
            $renderer->print_activities_section_page($course, null, null, null, null, $displaysection);
        echo html_writer::end_tag('div');
    } else {
        $renderer->print_innerlevel_section_page($course, null, null, null, null, $displaysection);
    }

} elseif(count($resources_sections)) {
    foreach ($resources_sections as $section){
        $renderer->print_innerlevel_section_page($course, null, null, null, null, $section->section);
    }
}

echo html_writer::end_tag('div');

echo html_writer::end_tag('div');


echo $OUTPUT->footer();
