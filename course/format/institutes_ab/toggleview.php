<?php

/**
 *
 * @package format_institutes_ab
 * @copyright  2017 The Institutes
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/course/lib.php');

$id      = required_param('id', PARAM_INT);
$view    = optional_param('view', 'institutes', PARAM_RAW);

set_user_preference('courseview_'.$id, $view);

redirect(new moodle_url('/course/view.php', array('id'=>$id)));


