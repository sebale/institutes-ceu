<?php

/**
 * This script allows the number of sections in a course to be increased
 * or decreased, redirecting to the course page.
 *
 * @package core_course
 * @copyright  2017 The Institutes
 */

require_once(dirname(__FILE__).'/../../../config.php');
require_once($CFG->dirroot.'/course/lib.php');

$courseid = required_param('courseid', PARAM_INT);
$parentid = optional_param('parent', 0, PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$parent = $DB->get_record('course_format_sections', array('sectionid' => $parentid, 'courseid'=>$course->id, 'format'=>'institutes_ab'));
$courseformatoptions = course_get_format($course)->get_format_options();
$index = $courseformatoptions['numsections'] + 1;
$PAGE->set_url('/course/format/institutes_ab/addsection.php', array('courseid' => $courseid));

require_login($course);
require_capability('moodle/course:update', context_course::instance($course->id));

$format_renderer = $PAGE->get_renderer('format_institutes_ab');

update_course((object)array('id' => $course->id, 'numsections' => $index));

$section = new stdClass();
$section->course = $courseid;
$section->section = $index;
$section->name = get_string('sectionname', 'format_institutes_ab').' '.$index;
$section->visible = 1;
$section->summary = '';
$section->summaryformat = 1;
$section->sequence = '';

$section->id = $DB->insert_record('course_sections', $section);

$params = new stdClass();
$params->parent = $parentid;
if ($params->parent > 0){
    $params->level = $parent->level+1;
    $params->parentssequence = $parentid;
    if ($parent->parent > 0){
        $params->parentssequence = $parent->parentssequence.','.$parentid;
    }
} else {
    $params->parentssequence = '';
    $params->level = 0;
}
course_get_format($course)->course_save_format_section($section, $params);

if (isset($parent->sectionid)){
    $modinfo = get_fast_modinfo($course);
    course_get_format($course)->sort_root_sections($course, $modinfo, $format_renderer);
}
rebuild_course_cache($courseid, true);

if (isset($parent->sectionid)){
    $url = new moodle_url('/course/editsection.php', array("id"=>$section->id, "sr"=>$parent->section, "parent"=>$parent->sectionid));
} else {
    $url = new moodle_url('/course/editsection.php', array("id"=>$section->id));
}

redirect($url);
