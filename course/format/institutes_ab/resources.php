<?php

/**
 * institutes_ab version file.
 *
 * @package    format_institutes_ab
 * @author     institutes_ab
 * @copyright  2017 The Institutes
 */


require('../../../config.php');
require_once('lib.php');

$systemcontext   = context_system::instance();

$id = required_param('id', PARAM_INT); // Option id.

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

$categories = $DB->get_records_sql("SELECT * FROM {course_format_resources} WHERE courseid = :courseid AND type = :type ORDER BY sortorder ASC", array('courseid'=>$course->id, 'type'=>'category'));
$resources = format_institutes_ab_get_course_resources($course);
$course_resources = format_institutes_ab_get_course_cmresources($course);

$isediting = ($PAGE->user_is_editing() and has_capability('format/institutes_ab:manageresources', $systemcontext)) ? true : false;
$fs = get_file_storage();

$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/institutes_ab/resources.php', $pageparams);

$title = get_string('resources', 'format_institutes_ab');

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class' => 'resources-box'.(($isediting) ? ' editing' : '')));

    if ($isediting){
        echo html_writer::start_tag('div', array('class'=>'action-buttons'));
        echo html_writer::link(new moodle_url('/course/format/institutes_ab/editcategory.php', array('id'=>$course->id)), get_string('createcategory', 'format_institutes_ab'), array('title' => get_string('createcategory', 'format_institutes_ab'),  'class'=>'btn btn-create'));
        echo html_writer::link(new moodle_url('/course/format/institutes_ab/resourcessettings.php', array('id'=>$course->id)), get_string('resourcessettings', 'format_institutes_ab'), array('title' => get_string('resourcessettings', 'format_institutes_ab'),  'class'=>'btn btn-create'));
        echo html_writer::end_tag('div');
    }

    echo html_writer::start_tag('ul', array('class'=>'sorting'));

if (isset($resources[0]) and count($resources[0])){
    foreach ($resources[0] as $resource){
        $str_download = get_string('download', 'format_institutes_ab');

        $cmcontext = context_module::instance($resource['mod']->id);
        $files = $fs->get_area_files($cmcontext->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
        $file = reset($files); unset($files); $instanceid = $resource['mod']->instance;
        if ($file and isset($course_resources[$instanceid])){
            $path = '/'.$cmcontext->id.'/mod_resource/content/'.$course_resources[$instanceid]->revision.$file->get_filepath().$file->get_filename();
            $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, true);
            if (stristr($file->get_mimetype(), 'video')){
                $str_download = get_string('watchvideo', 'format_institutes_ab');
                $fullurl = $resource['mod']->url;
            }
        }

        echo html_writer::start_tag('li', array('class'=>'clearfix', 'data-id'=>$resource['resource']->id));
            echo html_writer::tag('div', $resource['mod']->get_formatted_name(), array('class'=>'resource-name'));
            echo html_writer::start_tag('div', array('class'=>'resource-link'));
                echo html_writer::link((isset($fullurl)) ? $fullurl : $resource['mod']->url, $str_download, array('class'=>'btn'));
            echo html_writer::end_tag('div');
            if ($isediting){
                echo html_writer::start_tag('div', array('class'=>'actions'));
                    echo html_writer::tag('span', html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/dragdrop'), 'alt' => get_string('move'))), array('class'=>'resource-move'));
                echo html_writer::end_tag('div');
            }
        echo html_writer::end_tag('li');
    }
}

if (count($categories)){
    $i = 1;
    foreach ($categories as $category){
        echo html_writer::start_tag('li', array('class'=>'category clearfix', 'data-id'=>$category->id));
            echo html_writer::tag('div', $category->name, array('class'=>'category-name'));
            if ($isediting){
                $buttons = array();
                echo html_writer::start_tag('div', array('class'=>'actions'));
                    // edit
                    $actionurl = new moodle_url('/course/format/institutes_ab/editcategory.php', array('id'=>$course->id, 'cid'=>$category->id));
                    $actionimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit')));
                    $buttons[] = html_writer::link($actionurl, $actionimg, array('title' => get_string('edit')));

                    // delete
                    $actionurl = new moodle_url('/course/format/institutes_ab/editcategory.php', array('id'=>$course->id, 'cid'=>$category->id, 'action'=>'delete'));
                    $actionimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete')));
                    $buttons[] = html_writer::link($actionurl, $actionimg, array('title' => get_string('delete')));

                    if ($i > 1){
                        // moveup
                        $actionurl = new moodle_url('/course/format/institutes_ab/editcategory.php', array('id'=>$course->id, 'cid'=>$category->id, 'action'=>'moveup'));
                        $actionimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/up'), 'alt' => get_string('moveup')));
                        $buttons[] = html_writer::link($actionurl, $actionimg, array('title' => get_string('moveup')));
                    }

                    // movedown
                    if ($i < count($categories)){
                        $actionurl = new moodle_url('/course/format/institutes_ab/editcategory.php', array('id'=>$course->id, 'cid'=>$category->id, 'action'=>'movedown'));
                        $actionimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/down'), 'alt' => get_string('movedown')));
                        $buttons[] = html_writer::link($actionurl, $actionimg, array('title' => get_string('movedown')));
                    }

                    echo implode(' ', $buttons);

                echo html_writer::end_tag('div');
            }
        echo html_writer::end_tag('li');
        $i++;

        if (isset($resources[$category->id]) and count($resources[$category->id])){
            foreach ($resources[$category->id] as $resource){
                $str_download = get_string('download', 'format_institutes_ab');

                $cmcontext = context_module::instance($resource['mod']->id);
                $files = $fs->get_area_files($cmcontext->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
                $file = reset($files); unset($files); $instanceid = $resource['mod']->instance;
                if ($file and isset($course_resources[$instanceid])){
                    $path = '/'.$cmcontext->id.'/mod_resource/content/'.$course_resources[$instanceid]->revision.$file->get_filepath().$file->get_filename();
                    $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, true);
                    if (stristr($file->get_mimetype(), 'video')){
                        $str_download = get_string('watchvideo', 'format_institutes_ab');
                        $fullurl = $resource['mod']->url;
                    }
                }

                echo html_writer::start_tag('li', array('class'=>'clearfix', 'data-id'=>$resource['resource']->id));
                    echo html_writer::tag('div', $resource['mod']->get_formatted_name(), array('class'=>'resource-name'));
                    echo html_writer::start_tag('div', array('class'=>'resource-link'));
                        echo html_writer::link((isset($fullurl)) ? $fullurl : $resource['mod']->url, $str_download, array('class'=>'btn'));
                    echo html_writer::end_tag('div');
                    if ($isediting){
                        echo html_writer::start_tag('div', array('class'=>'actions'));
                            echo html_writer::tag('span', html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/dragdrop'), 'alt' => get_string('move'))), array('class'=>'resource-move'));
                        echo html_writer::end_tag('div');
                    }
                echo html_writer::end_tag('li');
            }
        }
    }
}

    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div');

?>
<?php if ($isediting) : ?>
<script src="<?php echo $CFG->wwwroot; ?>/course/format/institutes_ab/assets/js/jquery-sortable.js" type="text/javascript"></script>
<script>
    jQuery('.sorting').sortable({
        containerSelector: 'ul',
        itemSelector: 'li',
        handle: '.resource-move',
        horizontal: false,
        placeholder: '<li class=\"placeholder\"></li>',
        onDrag: function (item, group, _super) {
            item.addClass('active');
            jQuery('.sorting li').addClass('not-active');
        },
        onDrop: function  (item, container, _super) {
            jQuery('.sorting li').removeClass('not-active');
            item.removeClass('active');
            newIndex = item.index(); var new_eid = 0;
            var eid = item.attr('data-id');
            if (newIndex > 0){
                var row = jQuery('.sorting li').eq(newIndex-1);
                new_eid = parseInt(row.attr('data-id'));
            }
            if (new_eid || newIndex == 0){
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $CFG->wwwroot; ?>/course/format/institutes_ab/ajax.php?action=move_resource&id=<?php echo $course->id; ?>&eid='+eid+'&moveafter='+new_eid
                });
            }
            _super(item)
        },
        onCancel: function (item, container, _super, event) {
            jQuery('.sorting li').removeClass('not-active');
            item.removeClass('active');
            _super(item)
        },
    });
</script>
<?php endif; ?>
<?php

echo $OUTPUT->footer();
