<?php

/**
 * institutes_ab version file.
 *
 * @package    format_institutes_ab_ceu
 * @author     institutes_ab
 * @copyright  2017 The Institutes
 */


require('../../../config.php');
require_once('lib.php');

$systemcontext   = context_system::instance();

$id = required_param('id', PARAM_INT); // Option id.

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

$custonmresources = format_institutes_get_course_customresources($course);
$categories = $DB->get_records_sql("SELECT * FROM {course_format_resources} WHERE courseid = :courseid AND type = :type ORDER BY sortorder ASC", array('courseid'=>$course->id, 'type'=>'category'));

$resources = format_institutes_ab_get_course_resources($course);
$course_resources = format_institutes_ab_get_course_cmresources($course);
$sections = format_institutes_ab_get_course_sections($course);

$isediting = ($PAGE->user_is_editing() and has_capability('format/institutes_ab:manageresources', $systemcontext)) ? true : false;
$fs = get_file_storage();
$ceustates = format_institutes_ab_get_state_list();
$states = format_institutes_ab_get_course_states($course);
$requirements = format_institutes_ab_get_course_requirements($course, $states);
$canviewhiddensections = has_capability('moodle/course:viewhiddensections', $context);

$title = get_string('resources', 'format_institutes_ab');

$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/institutes_ab/courseresources.php', $pageparams);
$PAGE->navbar->add($title, new moodle_url('/course/format/institutes_ab/courseresources.php', $pageparams));

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title, 2, 'resources-heading');

echo html_writer::start_tag('div', array('class' => 'resources-box ceu-resources'));

    echo html_writer::start_tag('ul');

    // course resources
    if (isset($resources[0]) and count($resources[0])){
        foreach ($resources[0] as $resource){

            $section = (isset($sections[$resource['mod']->section])) ? $sections[$resource['mod']->section] : null;
            if (isset($section->id)){
                // if current section is transcript section
                if ($section->coursetranscriptflag and !$requirements['transcriptshown'] and !$canviewhiddensections){
                    continue;
                }
                // check user states
                if (isset($section->state) && !empty($section->state) && !in_array($section->state, $states) && !$canviewhiddensections) {
                    continue;
                }
            }

            $str_download = get_string('download', 'format_institutes');

            $cmcontext = context_module::instance($resource['mod']->id);
            $files = $fs->get_area_files($cmcontext->id, 'mod_resource', 'content', 0, 'sortorder DESC, id ASC', false);
            $file = reset($files); unset($files); $instanceid = $resource['mod']->instance;
            if ($file and isset($course_resources[$instanceid])){
                $path = '/'.$cmcontext->id.'/mod_resource/content/'.$course_resources[$instanceid]->revision.$file->get_filepath().$file->get_filename();
                $fullurl = moodle_url::make_file_url('/pluginfile.php', $path, true);
                if (stristr($file->get_mimetype(), 'video')){
                    $str_download = get_string('watchvideo', 'format_institutes');
                    $fullurl = $resource['mod']->url;
                }

                if (isset($section->coursetranscriptflag) and $section->coursetranscriptflag){
                    $issecurepdf = (strpos($file->get_filename(), '_s.pdf') !== false);
                    if ($issecurepdf and !$requirements['issecurepdf']){
                        continue;
                    }
                }
            }

            echo html_writer::start_tag('li', array('class'=>'clearfix', 'data-id'=>$resource['resource']->id));

                echo html_writer::tag('h3', $resource['mod']->get_formatted_name(), array('class'=>'resource-title'));
                echo html_writer::start_tag('div', array('class'=>'resource-description'));

                    if (!empty($course_resources[$instanceid]->intro)) {
                        $gotintro = trim(strip_tags($course_resources[$instanceid]->intro));
                        if ($gotintro) {
                            echo format_module_intro('resource', $course_resources[$instanceid], $resource['mod']->id);
                        }
                    }
                echo html_writer::end_tag('div');
                echo html_writer::start_tag('div', array('class'=>'resource-link'));
                    echo html_writer::link((isset($fullurl)) ? $fullurl : $resource['mod']->url, $str_download, array('class'=>'btn'));
                echo html_writer::end_tag('div');
            echo html_writer::end_tag('li');
        }
    }

    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div');


echo $OUTPUT->footer();
