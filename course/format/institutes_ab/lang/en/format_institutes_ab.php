<?php

/**
 * Strings for component 'format_institutes_ab'
 *
 * @package   format_institutes_ab
 * @copyright  2017 The Institutes
 */

$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['editsectionname'] = 'Edit topic name';
$string['deletesection'] = 'Delete topic';
$string['newsectionname'] = 'New name for topic {$a}';
$string['sectionname'] = 'Topic';
$string['pluginname'] = 'Institutes AB format';
$string['section0name'] = 'General';
$string['page-course-view-institutes-ab'] = 'Any course main page in institutes AB format';
$string['page-course-view-institutes-ab-x'] = 'Any course page in institutes AB format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';
$string['rootsections'] = 'Root sections';
$string['thumbnailfile'] = 'Course image/video file';
$string['completed'] = 'Completed';
$string['schedule'] = 'Schedule';
$string['schedulefile'] = 'Schedule File';
$string['infotext'] = 'Embed Video';
$string['pickup'] = 'Resume Course';
$string['schedule'] = 'Schedule';
$string['gradebook'] = 'Gradebook';
$string['activity'] = 'Activity';
$string['requiredasset'] = 'Required asset';
$string['status'] = 'Status';
$string['view'] = 'View';
$string['take'] = 'Take';
$string['viewmodule'] = 'View Module';
$string['viewwebsite'] = 'View Website';
$string['viewlink'] = 'View Link';
$string['notstarted'] = 'Not Started';
$string['completed'] = 'Completed';
$string['notcompleted'] = 'Not Completed';
$string['inprogress'] = 'In Progress';
$string['markcomplete'] = 'Mark Complete';
$string['markincomplete'] = 'Mark Incomplete';
$string['fail'] = 'Fail';
$string['createnewsection'] = 'Create new section';
$string['addsection'] = 'Add section';
$string['coursemenusettings'] = 'Course menu settings';
$string['glossary'] = 'Glossary';
$string['faq'] = 'FAQ';
$string['save'] = 'Save';
$string['selectoption'] = ' -- Select {$a} -- ';
$string['resources'] = 'Resources';
$string['name'] = 'Name';
$string['section'] = 'Section';
$string['type'] = 'Type';
$string['actions'] = 'Actions';
$string['resourcessettings'] = 'Resources Settings';
$string['nothingtodisplay'] = 'Nothing to display';
$string['editcategory'] = 'Edit category';
$string['createcategory'] = 'Create category';
$string['deletecategory'] = 'Delete category';
$string['confirmcategorydelete'] = 'Are you sure want to delete category <b>{$a}</b>?';
$string['download'] = 'Download';
$string['watchvideo'] = 'Watch video';
$string['displayonresources'] = 'Display on Resources Page';
$string['displayoncourse'] = 'Display on Course Page';
$string['institutes_ab:settings'] = 'Manage Settings';
$string['institutes_ab:manageresources'] = 'Manage course resources';
$string['institutes_ab:manageinstructions'] = 'Manage course instructions';
$string['institutes_ab:managenotes'] = 'Manage course notes';
