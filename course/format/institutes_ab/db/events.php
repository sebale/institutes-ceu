<?php

/**
 * Upgrade code containing changes to the plugin data table.
 *
 * @package    format_institutes_ab
 * @author     institutes_ab
 * @copyright  2017 The Institutes
 */

$observers = array(

    array(
        'eventname' => 'core\event\course_deleted',
        'callback'  => 'format_institutes_ab_observer::institutes_ab_course_deleted',
    ),
    array(
        'eventname' => 'core\event\course_module_deleted',
        'callback'  => 'format_institutes_ab_observer::institutes_ab_course_module_deleted',
    ),
);
