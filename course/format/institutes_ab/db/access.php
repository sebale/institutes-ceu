<?php

/**
 * institutes_ab version file.
 *
 * @package    format_institutes_ab
 * @author     institutes_ab
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'format/institutes_ab:settings' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
    'format/institutes_ab:manageresources' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),
);
