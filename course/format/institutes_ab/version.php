<?php
/**
 * Version details
 *
 * @package    format
 * @subpackage institutes_ab
 * @copyright  2017 The Institutes
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 20161114010;        // The current plugin version (Date: YYYYMMDDXX).
$plugin->requires  = 2016051900;        // Requires this Moodle version.
$plugin->component = 'format_institutes_ab';    // Full name of the plugin (used for diagnostics).
